//
//  Dialog.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 16/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Dialog : NSObject

+(void) showUnVailableStore;
+(void) showSuccessAddCart;

+(void) showConnectionErrorOnView:(UIView*) view;
+(void) showToast:(NSString*) title onView:(UIView*) view;

+(void) showDialog:(NSString*) title;

@end
