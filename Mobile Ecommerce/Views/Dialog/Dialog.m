//
//  Dialog.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 16/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "Dialog.h"
#import "MBProgressHUD.h"
#import "SIAlertView.h"
#import "StoreConfig.h"

#define OFFSET 170.f

@implementation Dialog

+(void) showConnectionErrorOnView:(UIView*) view{
    [self showToast:@"Erro na conexão" onView:view];
}

+(void) showSuccessAddCart{
    [self showDialog:@"Produto adicionado ao carrinho com sucesso"];
}

+(void) showUnVailableStore{
    
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Atenção" andMessage:@"Loja indisponível"];
    
    [alertView addButtonWithTitle:@"OK"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              [alert dismissAnimated:YES];
                          }];
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    
    [alertView show];
    
}

+(void) showToast:(NSString*) title onView:(UIView*) view{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = title;
    hud.margin = 10.f;
    hud.yOffset = OFFSET;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:3];
}

+(void) showDialog:(NSString*) message{

    if(message == nil){
        message = @"";
    }
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

@end
