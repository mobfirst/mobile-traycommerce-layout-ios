//
//  SettingCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryCell.h"

@interface SettingCell : CategoryCell

@property(nonatomic,strong) void (^block) (SettingCell* cell);
-(void) setupView;

@end
