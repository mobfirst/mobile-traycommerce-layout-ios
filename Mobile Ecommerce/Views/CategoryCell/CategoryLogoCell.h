//
//  CategoryLogoCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 24/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryLogoCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *storeLoge;

@end
