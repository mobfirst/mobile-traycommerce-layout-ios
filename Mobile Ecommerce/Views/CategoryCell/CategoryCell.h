//
//  CategoryCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 09/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *categoryImage;
@property (strong, nonatomic) IBOutlet UILabel *categoryName;
@property (strong, nonatomic) IBOutlet UIView *viewFooter;
@end
