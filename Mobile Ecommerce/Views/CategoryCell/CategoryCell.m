//
//  CategoryCell.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 09/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "CategoryCell.h"

@implementation CategoryCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
