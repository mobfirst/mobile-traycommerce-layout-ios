//
//  StoreContactViewCell.h
//  Mia Inko
//
//  Created by Diego P Navarro on 28/02/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreContactViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *title;

@property (strong,nonatomic) NSIndexPath* indexPath;
@property(nonatomic,strong) void (^block) (NSIndexPath* indexPath);

-(void) setupCell;


@end
