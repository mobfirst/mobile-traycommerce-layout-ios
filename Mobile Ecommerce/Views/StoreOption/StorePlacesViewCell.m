//
//  StorePlacesViewCell.m
//  Mia Inko
//
//  Created by Diego P Navarro on 28/02/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "StorePlacesViewCell.h"

@implementation StorePlacesViewCell



-(void) setupCell{
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.contentView addGestureRecognizer:tapGestureRecognizer];
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer{
    if(self.block){
        self.block(self.indexPath);
    }
}

@end
