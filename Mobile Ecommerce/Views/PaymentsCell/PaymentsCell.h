//
//  PaymentsCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 29/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentsCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *title;

@end
