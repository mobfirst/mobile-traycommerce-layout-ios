//
//  ProductCategoryBannerCell.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 26/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "ProductCategoryBannerCell.h"
#import "StoreConfig.h"
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

@interface ProductCategoryBannerCell(){
    NSArray * images;
}

@end

@implementation ProductCategoryBannerCell

-(void) setupCell {
    
    images = [StoreConfig getStore].banners;
    
    self.pageControl.currentPageIndicatorTintColor = [StoreConfig getStoreColor];
    self.pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    

    CGRect frameScrollView = self.scrollView.frame;
    frameScrollView.size.width = [[UIScreen mainScreen] bounds].size.width;
    self.scrollView.frame = frameScrollView;
    
    [self addBanners];
}


-(void) addBanners{
    
    for(int count = 0;count < [images count];count ++){
        CGRect frame;
        frame.origin.x = self.scrollView.frame.size.width * count;
        frame.origin.y = 0;
        frame.size = self.scrollView.frame.size;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        imageView.backgroundColor = [UIColor whiteColor];
        [imageView sd_setImageWithURL:[NSURL URLWithString:[images objectAtIndex:count]]];
        [self.scrollView addSubview:imageView];
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * [images count], self.scrollView.frame.size.height);
    self.pageControl.numberOfPages = [images count];

}

#pragma mark - UIScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender{
    
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
}

@end
