//
//  ProductCategorySubCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryClickControl.h"
#import "ProductCategory.h"


@interface ProductCategorySubCell : UITableViewCell

@property(strong,nonatomic) ProductCategory* category;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewCategory;
@property (strong, nonatomic) IBOutlet UILabel *label;

-(void) setupView;
@end
