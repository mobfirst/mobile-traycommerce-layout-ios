//
//  ProductCategoryCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 26/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+Extras.h"
#import "ProductCategory.h"

@interface ProductCategoryCell : UITableViewCell

@property (strong,nonatomic) ProductCategory* category;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *visualEffectView;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewCategory;
@property (strong, nonatomic) IBOutlet UILabel *categoryName;
@property (strong, nonatomic) IBOutlet UIView *viewButton;

@property (nonatomic, strong) void(^responseBlock)(ProductCategoryCell*);


-(void) setupCellWithSelectedBlock:(void (^)(ProductCategoryCell*))block;

@end
