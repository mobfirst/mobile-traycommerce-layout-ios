//
//  ProductCategoryCell.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 26/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "ProductCategoryCell.h"
#import "StoreConfig.h"
#import "ServiceApiMapper.h"
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

@implementation ProductCategoryCell


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void) setupCellWithSelectedBlock:(void (^)(ProductCategoryCell*))block{

    [self getCategoryImage];
    
    self.responseBlock = block;
    
    self.categoryName.text = [self.category.name uppercaseString];
    self.viewButton.backgroundColor = [StoreConfig getStoreColor];
    
    self.viewButton.layer.cornerRadius = self.viewButton.frame.size.width/2;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.contentView addGestureRecognizer:tapGestureRecognizer];
    
}
 - (void) handleTapFrom: (UITapGestureRecognizer *)recognizer{
     if(self.responseBlock){
         self.responseBlock(self);
     }
}

-(void) getCategoryImage{
    
    [[ServiceApiMapper Api] getCategoryImage:self.category.id withBlock:^(NSObject * object, NSError * error) {
        
        if(!error && object){
            [self.imageViewCategory sd_setImageWithURL: [NSURL URLWithString:(NSString*)object]];
        }
    }];
}
@end
