//
//  ProductCategorySubCell.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "ProductCategorySubCell.h"
#import "ServiceApiMapper.h"
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

@implementation ProductCategorySubCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void) setupView{
    
    [self getCategoryImage];
    
    UIImage * image = [UIImage imageNamed:@"no_image.jpg"];
    self.imageViewCategory .image = image;
   
    self.imageViewCategory.layer.masksToBounds = YES;
    self.imageViewCategory.layer.cornerRadius = self.imageViewCategory.frame.size.width/2;
    self.label.text = [self.category.name uppercaseString];
}

-(void) getCategoryImage{
    [[ServiceApiMapper Api] getCategoryImage:self.category.id withBlock:^(NSObject * object, NSError * error) {
        if(!error && object){
            [self.imageViewCategory sd_setImageWithURL: [NSURL URLWithString:(NSString*)object]];
        }
    }];
}


@end
