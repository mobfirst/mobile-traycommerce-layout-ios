//
//  InstagramCell.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 20/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "InstagramCell.h"
#import "UIColor+HexColors.h"
#import "Utils.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define MARGEN 8.0f
#define IMAGE_SIZE 250.0f

@implementation InstagramCell

-(void) setupView:(InstagramObjects*) instagramObject{
    
    self.instagramPost = instagramObject;
    
    self.imagePost.image = nil;
    [self.imagePost sd_setImageWithURL:[NSURL URLWithString:instagramObject.image]];
    
    self.imageUser.image = nil;
    [self.imageUser sd_setImageWithURL:[NSURL URLWithString:instagramObject.userimage]];

    self.labelUser.text = [NSString stringWithFormat:@"@%@",instagramObject.username];
    self.labelLikes.text = [NSString stringWithFormat:@"%ld likes",(long)instagramObject.likes];
    self.labelDescription.text = [NSString stringWithFormat:@"%@\n\n",instagramObject.text];
    
    [self.imageIconVideo setHidden:!instagramObject.video];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.imagePost addGestureRecognizer:tapGestureRecognizer];
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer{
    if(self.instagramPost.video){
        [self setupVideoPlayer];
    }
}

-(void) setupVideoPlayer{
    
    if(!self.moviePlayer){
    
        NSURL *url = [NSURL URLWithString:self.instagramPost.video];
        self.moviePlayer = [[MPMoviePlayerController alloc]
                                               initWithContentURL:url];
        self.moviePlayer.controlStyle = MPMovieControlStyleEmbedded;
        
        self.moviePlayer.view.frame = self.imagePost.bounds; //Set the size
        [self.imagePost addSubview:self.moviePlayer.view]; //Show the view
    }
    
    [self.moviePlayer play];
  

}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGSize maxSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - MARGEN*4, CGFLOAT_MAX);
    CGSize requiredSize = [self.labelUser sizeThatFits:maxSize];
    self.labelUser.frame = CGRectMake(self.labelUser.frame.origin.x, self.labelUser.frame.origin.y, requiredSize.width, requiredSize.height);
    
    requiredSize = [self.labelLikes sizeThatFits:maxSize];
    self.labelLikes.frame = CGRectMake(self.labelLikes.frame.origin.x, self.labelLikes.frame.origin.y, requiredSize.width, requiredSize.height);
    
    requiredSize = [self.labelDescription sizeThatFits:maxSize];
    self.labelDescription.frame = CGRectMake(self.labelDescription.frame.origin.x, self.labelDescription.frame.origin.y, requiredSize.width, requiredSize.height);
    
    // Reposition labels to handle content height changes
    
    CGRect subtitleFrame = self.labelLikes.frame;
    subtitleFrame.origin.y = self.labelUser.frame.origin.y + self.labelUser.frame.size.height + MARGEN;
    self.labelLikes.frame = subtitleFrame;
    
    CGRect contentTextFrame = self.labelDescription.frame;
    contentTextFrame.origin.y = self.labelLikes.frame.origin.y + self.labelLikes.frame.size.height + MARGEN;
    self.labelDescription.frame = contentTextFrame;
    
    
    // Calculate cell height
    self.requiredCellHeight = MARGEN*5 + IMAGE_SIZE;
    self.requiredCellHeight += self.labelUser.frame.size.height;
    self.requiredCellHeight += self.labelLikes.frame.size.height;
    self.requiredCellHeight += self.labelDescription.frame.size.height;
}


@end
