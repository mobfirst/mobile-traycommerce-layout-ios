//
//  YoutubeCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 19/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YouTubeObject.h"

@interface YoutubeCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *videoImage;
@property (strong, nonatomic) IBOutlet UILabel *videoName;
@property (strong, nonatomic) IBOutlet UILabel *videoInformation;
@property (strong, nonatomic) IBOutlet UILabel *videoDescription;
@property (strong, nonatomic) IBOutlet UILabel *videoDuration;

@property (strong, nonatomic) YouTubeObject* video;
@property(nonatomic,strong) void (^block) (YouTubeObject* YouTubeObject);

@property (nonatomic) float requiredCellHeight;
-(void) setupView:(YouTubeObject*) video;
@end
