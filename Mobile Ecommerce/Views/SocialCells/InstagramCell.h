//
//  InstagramCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 20/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstagramObjects.h"
#import <MediaPlayer/MediaPlayer.h>

@interface InstagramCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imagePost;
@property (strong, nonatomic) IBOutlet UIImageView *imageUser;
@property (strong, nonatomic) IBOutlet UILabel *labelUser;
@property (strong, nonatomic) IBOutlet UILabel *labelLikes;
@property (strong, nonatomic) IBOutlet UILabel *labelDescription;
@property (strong, nonatomic) IBOutlet UIImageView *imageIconVideo;

@property ( strong,nonatomic) InstagramObjects* instagramPost;
@property (nonatomic,strong) MPMoviePlayerController* moviePlayer;
@property (nonatomic) float requiredCellHeight;
-(void) setupView:(InstagramObjects*) instagramObject;

@end
