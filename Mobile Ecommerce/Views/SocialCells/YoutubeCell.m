//
//  YoutubeCell.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 19/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "YoutubeCell.h"
#import "UIColor+HexColors.h"
#import "Utils.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define MARGEN 8.0f
#define IMAGE_SIZE 250.0f

@implementation YoutubeCell

-(void) setupView:(YouTubeObject*) video{
    self.video = video;
    [self.videoImage sd_setImageWithURL:[NSURL URLWithString:video.urlImage]];
    self.videoName.text = video.title;
    self.videoInformation.text = [NSString stringWithFormat:@"%@ - %@ views",[Utils calculateTimePastYouTube:video.published],[Utils formatterTextVideoCount:video.viewCount]];
    self.videoDescription.text = [NSString stringWithFormat:@"%@\n",video.videoDescription];
    self.videoDuration.text = [Utils formatterTextVideoDuration:video.duration];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.videoImage addGestureRecognizer:tapGestureRecognizer];
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer{
    if(self.block){
        self.block(self.video);
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGSize maxSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 32, CGFLOAT_MAX);
    CGSize requiredSize = [self.videoName sizeThatFits:maxSize];
    self.videoName.frame = CGRectMake(self.videoName.frame.origin.x, self.videoName.frame.origin.y, requiredSize.width, requiredSize.height);
    
    requiredSize = [self.videoInformation sizeThatFits:maxSize];
    self.videoInformation.frame = CGRectMake(self.videoInformation.frame.origin.x, self.videoInformation.frame.origin.y, requiredSize.width, requiredSize.height);
    
    requiredSize = [self.videoDescription sizeThatFits:maxSize];
    self.videoDescription.frame = CGRectMake(self.videoDescription.frame.origin.x, self.videoDescription.frame.origin.y, requiredSize.width, requiredSize.height);
    
    // Reposition labels to handle content height changes
    
    CGRect subtitleFrame = self.videoInformation.frame;
    subtitleFrame.origin.y = self.videoName.frame.origin.y + self.videoName.frame.size.height;
    self.videoInformation.frame = subtitleFrame;
    
    CGRect contentTextFrame = self.videoDescription.frame;
    contentTextFrame.origin.y = self.videoInformation.frame.origin.y + self.videoInformation.frame.size.height + MARGEN;
    self.videoDescription.frame = contentTextFrame;
    
    
    // Calculate cell height
    self.requiredCellHeight = MARGEN*4 + IMAGE_SIZE;
    self.requiredCellHeight += self.videoName.frame.size.height;
    self.requiredCellHeight += self.videoInformation.frame.size.height;
    self.requiredCellHeight += self.videoDescription.frame.size.height;
}

@end
