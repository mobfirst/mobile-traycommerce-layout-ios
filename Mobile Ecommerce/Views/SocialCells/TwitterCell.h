//
//  TwitterCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 19/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TwitterObject.h"

@interface TwitterCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *tweetImage;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) IBOutlet UILabel *identifierAndTime;
@property (strong, nonatomic) IBOutlet UILabel *labelInformation;
@property (strong, nonatomic) NSString* urlLink;
@property(nonatomic,strong) void (^block) (NSString* url);

@property (nonatomic) float requiredCellHeight;
-(void) setupView:(TwitterObject*) tweet;

@end
