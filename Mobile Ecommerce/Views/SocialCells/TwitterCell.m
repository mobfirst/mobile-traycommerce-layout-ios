//
//  TwitterCell.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 19/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "TwitterCell.h"
#import "TwitterUrls.h"
#import "Utils.h"
#import "UIColor+HexColors.h"
#import <SDWebImage/UIImageView+WebCache.h>


#define LINK_COLOR @"1A6FCD"

@implementation TwitterCell

-(void) setupView:(TwitterObject*) tweet{
    
    [self.tweetImage sd_setImageWithURL:[NSURL URLWithString:tweet.userImage]];
    self.name.text = tweet.userName;
    self.identifierAndTime.text = [NSString stringWithFormat:@"@%@ - %@",tweet.user,[Utils calculateTimePast:tweet.timeCreate]];

    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n",tweet.text]];
    
    if(tweet.urls){
        for(TwitterUrls * url in tweet.urls){
            [attributeString addAttribute:NSForegroundColorAttributeName
                                    value:[UIColor colorWithHexString:LINK_COLOR]
                                    range:[tweet.text rangeOfString: url.url]];
            
            self.urlLink = url.url;
        }
    }
    
    self.labelInformation.attributedText = attributeString;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.labelInformation addGestureRecognizer:tapGestureRecognizer];
    
}
- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer{
    if(self.urlLink && self.block){
        self.block(self.urlLink);
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGSize maxSize = CGSizeMake([UIScreen mainScreen].bounds.size.width - 32, CGFLOAT_MAX);
    CGSize requiredSize = [self.name sizeThatFits:maxSize];
    self.name.frame = CGRectMake(self.name.frame.origin.x, self.name.frame.origin.y, requiredSize.width, requiredSize.height);
    
    requiredSize = [self.identifierAndTime sizeThatFits:maxSize];
    self.identifierAndTime.frame = CGRectMake(self.identifierAndTime.frame.origin.x, self.identifierAndTime.frame.origin.y, requiredSize.width, requiredSize.height);
    
    requiredSize = [self.labelInformation sizeThatFits:maxSize];
    self.labelInformation.frame = CGRectMake(self.labelInformation.frame.origin.x, self.labelInformation.frame.origin.y, requiredSize.width, requiredSize.height);
    
    // Reposition labels to handle content height changes
    
    CGRect subtitleFrame = self.identifierAndTime.frame;
    subtitleFrame.origin.y = self.name.frame.origin.y + self.name.frame.size.height;
    self.identifierAndTime.frame = subtitleFrame;
    
    CGRect contentTextFrame = self.labelInformation.frame;
    contentTextFrame.origin.y = self.identifierAndTime.frame.origin.y + self.identifierAndTime.frame.size.height + 13.0f;
    self.labelInformation.frame = contentTextFrame;
    
    
    // Calculate cell height
    
    self.requiredCellHeight = 10.0f + 13.0f;
    self.requiredCellHeight += self.name.frame.size.height;
    self.requiredCellHeight += self.identifierAndTime.frame.size.height;
    self.requiredCellHeight += self.labelInformation.frame.size.height;
}

@end
