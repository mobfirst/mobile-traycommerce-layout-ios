//
//  CartViewCell.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "CartViewCell.h"
#import "Variation.h"

#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import "NSString+HTML.h"

@implementation CartViewCell

- (void)awakeFromNib {

}

-(void) setupView : (float) subTotal{

    self.productQuantityValue = (float)self.productCart.quantity;
    self.productQuantity.text = [NSString stringWithFormat:@"%.0f",self.productQuantityValue];
    
    self.productTitle.text = [self.productCart.name decodeHTMLCharacterEntities];

    if(self.productCart.images && [self.productCart.images count] > 0){
        NSString* url = [self.productCart.images objectAtIndex:0];
        [self.productImage sd_setImageWithURL:[NSURL URLWithString:url]];
    }
    else{
        self.productImage.image = [UIImage imageNamed:@"no_image.jpg"];
    }
    
    NSMutableAttributedString *attributeStringOption;
    
    if(self.productCart.option){
        Variation* variation = [self.productCart.option.childrens objectAtIndex:0];
        self.productOptionValue.text = [variation.value uppercaseString];
        self.productOptionTitle.text = @"";
    }
    else{
        attributeStringOption =[[NSMutableAttributedString alloc] initWithString:@"MODELO ÚNICO"];
        [attributeStringOption addAttribute:NSForegroundColorAttributeName
                                      value:[UIColor darkGrayColor]
                                      range:NSMakeRange( 0, [attributeStringOption length])];
        self.productOptionValue.attributedText = attributeStringOption;
        self.productOptionTitle.text = @"";
        
    }
    
    self.productCart.price = [self.productCart.price stringByReplacingOccurrencesOfString:@"." withString:@","];
    
    NSString* valueProductUnit = [NSString stringWithFormat:@"VALOR DO PRODUTO R$ %@",self.productCart.price];
    attributeStringOption = [[NSMutableAttributedString alloc] initWithString:valueProductUnit];
    [attributeStringOption addAttribute:NSForegroundColorAttributeName
                                  value:[UIColor darkGrayColor]
                                  range:[valueProductUnit rangeOfString:self.productCart.price] ];
    self.productValue.attributedText = attributeStringOption;
    
    float valueTotal = [[self.productCart.price stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue] * self.productCart.quantity;
    
    NSString* valueProductTotal = [[NSString stringWithFormat:@"VALOR DO LOTE R$ %.2f",(float) valueTotal] stringByReplacingOccurrencesOfString:@"." withString:@","];
    
    attributeStringOption = [[NSMutableAttributedString alloc] initWithString: valueProductTotal];
    [attributeStringOption addAttribute:NSForegroundColorAttributeName
                                  value:[UIColor darkGrayColor]
                                  range:[valueProductTotal rangeOfString:[[NSString stringWithFormat:@"%.2f",(float) valueTotal] stringByReplacingOccurrencesOfString:@"." withString:@","]]];
    self.productTotal.attributedText = attributeStringOption;
    
    
    NSString* valueSubProductTotal = [[NSString stringWithFormat:@"SUBTOTAL R$ %.2f",(float)subTotal] stringByReplacingOccurrencesOfString:@"." withString:@","];
    
    attributeStringOption = [[NSMutableAttributedString alloc] initWithString:valueSubProductTotal];
    [attributeStringOption addAttribute:NSForegroundColorAttributeName
                                  value:[UIColor darkGrayColor]
                                  range:[valueSubProductTotal rangeOfString:[[NSString stringWithFormat:@"%.2f",(float) valueTotal] stringByReplacingOccurrencesOfString:@"." withString:@","]]];
    self.cartSubTotal.attributedText = attributeStringOption;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) updateQuantity{
    if(self.blockUpdate){
        self.blockUpdate(self.productCart,self.productQuantityValue);
    }
    self.productQuantity.text = [NSString stringWithFormat:@"%.2f",self.productQuantityValue];
}

- (IBAction)productSub:(id)sender {
     self.productQuantityValue --;
    if(self.productQuantityValue < 0){
        self.productQuantityValue = 0;
    }
    [self updateQuantity];
}

- (IBAction)productAdd:(id)sender {
    self.productQuantityValue ++;
    [self updateQuantity];
}

- (IBAction)productDiscard:(id)sender {
    if(self.blockDelete){
        self.blockDelete(self.productCart);
    }
}
@end
