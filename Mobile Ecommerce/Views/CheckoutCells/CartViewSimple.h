//
//  CartViewSimple.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 30/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartViewSimple : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *quantity;
@property (strong, nonatomic) IBOutlet UILabel *price;

@end
