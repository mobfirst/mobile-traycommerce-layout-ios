//
//  CartTotalCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartTotalCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *labelTotal;
@property (strong, nonatomic) NSString *total;

-(void) setupView;

@end
