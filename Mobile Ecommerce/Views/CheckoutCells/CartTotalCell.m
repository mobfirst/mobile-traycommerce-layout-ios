//
//  CartTotalCell.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "CartTotalCell.h"

@implementation CartTotalCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void) setupView{
    
    self.total = [[NSString stringWithFormat:@"%.2f",(float)[self.total floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@","];
    NSString* totalValue = [NSString stringWithFormat:@"TOTAL: R$ %@",self.total];
    NSMutableAttributedString *attributeStringOption = [[NSMutableAttributedString alloc] initWithString:totalValue];
    [attributeStringOption addAttribute:NSForegroundColorAttributeName
                                  value:[UIColor darkGrayColor]
                                  range:[totalValue rangeOfString:self.total]];
    self.labelTotal.attributedText = attributeStringOption;
    

}

@end
