//
//  CartViewCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductCart.h"

@interface CartViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *productTitle;
@property (strong, nonatomic) IBOutlet UILabel *productOptionTitle;
@property (strong, nonatomic) IBOutlet UILabel *productOptionValue;
@property (strong, nonatomic) IBOutlet UILabel *productValue;
@property (strong, nonatomic) IBOutlet UILabel *productTotal;
@property (strong, nonatomic) IBOutlet UILabel *productQuantity;
@property (strong, nonatomic) IBOutlet UILabel *cartSubTotal;
@property (strong, nonatomic) IBOutlet UIImageView *productImage;

@property(strong,nonatomic) ProductCart* productCart;

@property(nonatomic,strong) void (^blockUpdate) (ProductCart* productCart, int quantity);
@property(nonatomic,strong) void (^blockDelete) (ProductCart* productCart);

@property (nonatomic) float productQuantityValue;

- (IBAction)productSub:(id)sender;
- (IBAction)productAdd:(id)sender;
- (IBAction)productDiscard:(id)sender;

-(void) setupView : (float) subTotal;





@end
