//
//  ProductModelsTitleCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductModels.h"

@interface ProductModelsTitleCell : UITableViewCell

@property (strong, nonatomic) ProductModels *productModels;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *labelNumber;

- (IBAction)actionSub:(id)sender;
- (IBAction)actionAdd:(id)sender;

@end
