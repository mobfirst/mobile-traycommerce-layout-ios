//
//  ProductModelsTitleCell.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "ProductModelsTitleCell.h"

@implementation ProductModelsTitleCell

- (void)awakeFromNib {
    self.productModels.quantity = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)actionSub:(id)sender {
    self.productModels.quantity--;
    if(self.productModels.quantity < 0){
        self.productModels.quantity = 0;
    }
    [self updateView];
}

- (IBAction)actionAdd:(id)sender {
    self.productModels.quantity++;
    [self updateView];
}

-(void) updateView{
    self.labelNumber.text = [NSString stringWithFormat:@"%d",self.productModels.quantity];
}
@end
