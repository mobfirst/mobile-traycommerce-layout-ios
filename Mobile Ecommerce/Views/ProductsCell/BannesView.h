//
//  BannesView.h
//  Mia Inko
//
//  Created by Diego P Navarro on 09/02/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannesView : UICollectionReusableView<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

-(void) setupCell;

@end
