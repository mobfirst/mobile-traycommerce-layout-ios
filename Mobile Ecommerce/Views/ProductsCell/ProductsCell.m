//
//  ProductsCell.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 09/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "ProductsCell.h"
#import "StoreConfig.h"

#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

#define CORNER_SIZE 4

@implementation ProductsCell

- (void)setupView{
    
    
    self.productImage.image = nil;
    
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = CORNER_SIZE;
    
    NSString* oldPrice=@"";
    NSString* price = @"";
    
    if(self.product.specialPrice){
        price = self.product.specialPrice;
        oldPrice = self.product.price;
    }else{
        price =self.product.price;
    }
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[oldPrice stringByReplacingOccurrencesOfString:@"." withString:@","]];
    [attributeString addAttribute:NSStrikethroughStyleAttributeName
                            value:@1
                            range:NSMakeRange(0, [attributeString length])];
    
    
    self.productPriceOld.attributedText = attributeString;
    
    self.productPrice.text =[[NSString stringWithFormat:@"R$ %@",price] stringByReplacingOccurrencesOfString:@"." withString:@","];
    
    self.productPrice.textColor = [StoreConfig getStoreColor];
    
    self.productDescription.text = self.product.name;
    
    if(self.product.images && [self.product.images count] > 0){
        NSString* url = [self.product.images objectAtIndex:0];
        [self.productImage sd_setImageWithURL:[NSURL URLWithString:url]];
    }else{
        self.productImage.image = [UIImage imageNamed:@"no_image.jpg"];
    }

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.contentView addGestureRecognizer:tapGestureRecognizer];
    
}
- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer{
    if(self.responseBlock){
        self.responseBlock(self.product);
    }
}

@end
