//
//  ProductsCell.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 09/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface ProductsCell : UICollectionViewCell

@property(strong,nonatomic) Product* product;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UILabel *productDescription;
@property (strong, nonatomic) IBOutlet UILabel *productPriceOld;
@property (strong, nonatomic) IBOutlet UILabel *productPrice;
@property (nonatomic, strong) void(^responseBlock)( Product* );
- (void)setupView;

@end
