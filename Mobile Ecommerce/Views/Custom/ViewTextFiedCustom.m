//
//  ViewTextFiedCustom.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "ViewTextFiedCustom.h"

@implementation ViewTextFiedCustom


-(void) drawRect:(CGRect)rect{
 
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 3;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGRect frame = self.bounds;
    CGContextSetLineCap(context, 1);
    CGRectInset(frame, 0.5f, 0.5f);
    [[UIColor lightGrayColor] set];
    UIRectFrame(frame);
}

@end
