//
//  StoreConfig.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 24/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "StoreConfig.h"



@implementation StoreConfig

+(UIColor*) getStoreColor{
    return [UIColor colorWithHexString: [ServiceStorageStore getDefaultStore].color];
}

+(Store*) getStore{
    return [ServiceStorageStore getDefaultStore];
}

@end
