//
//  StoreConfig.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 24/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIColor+HexColors.h"
#import "ServiceStorageStore.h"
#import "Store.h"

@interface StoreConfig : NSObject

+(UIColor*) getStoreColor;
+(Store*) getStore;

@end
