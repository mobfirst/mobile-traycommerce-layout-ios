//
//  Constants.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 09/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#ifndef Mobile_Ecommerce_Constants_h
#define Mobile_Ecommerce_Constants_h


/*MENU ANIMATION */

#define MENU_ANIMATION_TIME 0.5




/*CATEGORY ALL*/
#define CATEGORY_DEFAULT @"-1000"
#define CATEGORY_SEARCH @"-1001"

/*OPTIONS IN MENU*/

typedef enum {
    MENU_TAG_RETURN,
    MENU_TAG_NOT_SELECTED,
    MENU_TAG_EXPLORE,
    MENU_TAG_SOCIAL,
    MENU_TAG_VIDEOS,
    MENU_TAG_STORES,
    MENU_TAG_ADDRESS,
    MENU_TAG_ACCOUNT,
    MENU_TAG_LOGO,
    MENU_TAG_CONTACT,
    MENU_TAG_EMAIL,
    MENU_TAG_PHONE,
    MENU_TAG_INSTAGRAM,
    MENU_TAG_FACEBOOK,
    MENU_TAG_TWITTER,
} MenuTag;


#endif
