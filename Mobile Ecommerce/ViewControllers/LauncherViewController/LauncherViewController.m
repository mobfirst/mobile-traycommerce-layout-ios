//
//  LauncherViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "LauncherViewController.h"
#import "NavigationViewController.h"
#import "LeftMenuViewController.h"
#import <TheSidebarController/TheSidebarController.h>
#import "Constants.h"
#import "ServiceApiMapper.h"
#import "Store.h"
#import "ServiceStorageStore.h"
#import "Utils.h"
#import "NavigationViewControllerBusiness.h"
#import "LeftMenuBusinessViewController.h"

@interface LauncherViewController (){
    NSArray* categories;
}

@end

@implementation LauncherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getStore];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma GET STORE INFORMATION

-(void) getStore{

    [[ServiceApiMapper Api] getStoreInformationWithBlock:^(NSObject *object, NSError * error) {
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getStore) withObject:nil afterDelay:8];
        }
        else{
            Store* store = (Store*) object;
            if(!store.active){
                [Dialog showUnVailableStore];
            }else{
                [ServiceStorageStore saveDefaultStore:store];
                [self getCategories];
            }
        }
    }];
}

-(void) getCategories{

    [[ServiceApiMapper Api] getCategoriesWithBlock:^(NSArray *categoriesList, NSError * error) {
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getStore) withObject:nil afterDelay:8];
        }
        else{
            categories = categoriesList;
            categories = [Utils insertCategoryDefaultOnCategories:categories];
            categories = [Utils insertCategoryDefault:categories];
            
            switch ([StoreConfig getStore].model) {
                case BUSINESS:
                    [self callHomeBusinessViewController];
                    break;
                case ENTERPRISE:
                    [self callHomeEnterpriseViewController];
                    break;
                default:
                    break;
            }
        }
    }];
}


-(void) callHomeBusinessViewController{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NavigationViewControllerBusiness *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationViewControllerBusiness"];
    navigationController.categories = categories;
    LeftMenuBusinessViewController* leftMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"LeftMenuBusinessViewController"];
    leftMenuViewController.categories = categories;
    
    TheSidebarController *sidebarController = [[TheSidebarController alloc] initWithContentViewController:navigationController leftSidebarViewController:leftMenuViewController rightSidebarViewController:nil];
    sidebarController.animationDuration = MENU_ANIMATION_TIME;

    sidebarController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:sidebarController animated:YES completion:nil];
}


-(void) callHomeEnterpriseViewController{
    

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    NavigationViewController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"NavigationViewController"];
    navigationController.categories = categories;
    
    LeftMenuViewController* leftMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"LeftMenuViewController"];
     
    TheSidebarController *sidebarController = [[TheSidebarController alloc] initWithContentViewController:navigationController leftSidebarViewController:leftMenuViewController rightSidebarViewController:nil];
    sidebarController.animationDuration = MENU_ANIMATION_TIME;
  
    sidebarController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:sidebarController animated:YES completion:nil];
}



@end
