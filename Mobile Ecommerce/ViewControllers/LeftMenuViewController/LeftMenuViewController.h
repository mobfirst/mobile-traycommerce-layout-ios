//
//  LeftMenuViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 08/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MapKit/MapKit.h>

@interface LeftMenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
