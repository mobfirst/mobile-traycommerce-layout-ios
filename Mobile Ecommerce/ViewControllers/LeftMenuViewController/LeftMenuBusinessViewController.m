//
//  LeftMenuBusinessViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "LeftMenuBusinessViewController.h"
#import "CategoryCell.h"
#import "ProductCategory.h"
#import "LeftMenuModel.h"
#import "StoreConfig.h"
#import "ServiceApiMapper.h"
#import <TheSidebarController/TheSidebarController.h>
#import "NavigationViewControllerBusiness.h"
#import "CategoryLogoCell.h"
#import "Utils.h"

#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

#import <EventTracker/GlobalEventTracker.h>

#define NUMBER_OF_SECTIONS 2

#define FONT_SIZE_SMALL 10
#define CELL_IDENTIFICATION_ITEM @"CategoryCell"
#define CELL_IDENTIFICATION_ITEM_WITHOUT_IMAGE @"CategoryCellTitle"
#define CELL_LOGO_IDENTIFICATION_ITEM @"CategoryLogoCell"

#define PADDING_BOTTOM_TABLE_VIEW 22
#define LIBRARY_LEFT_MENU_SIZE_SCREEN 260

#define CELL_HEIGHT_LOGO 100
#define CELL_HEIGHT_CATEGORY 70


@interface LeftMenuBusinessViewController (){
    NSMutableArray* arrayCurrentCategories;
    NSMutableArray* arrayitensOptions;
    NSInteger indexToChangeOption;
}

@end

@implementation LeftMenuBusinessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    self.tableView.backgroundColor = [StoreConfig getStoreColor];
    self.view.backgroundColor = [StoreConfig getStoreColor];
    self.seachBar.barTintColor =[StoreConfig getStoreColor];
    
    self.tableView.contentInset = UIEdgeInsetsMake(PADDING_BOTTOM_TABLE_VIEW, 0, PADDING_BOTTOM_TABLE_VIEW, 0);
    self.leftConstraint.constant = [UIScreen mainScreen].bounds.size.width - LIBRARY_LEFT_MENU_SIZE_SCREEN + self.leftConstraint.constant ;
    self.leftConstraintTable.constant = [UIScreen mainScreen].bounds.size.width - LIBRARY_LEFT_MENU_SIZE_SCREEN + self.leftConstraintTable.constant ;    
    
    arrayitensOptions = [NSMutableArray new];
    arrayCurrentCategories = [NSMutableArray new];
    
    [self insertCategoriesOnList];
    [self insertOptionsOnList];
}

-(void) insertCategoriesOnList{
    for(ProductCategory* category in self.categories){
        [arrayCurrentCategories addObject:[LeftMenuModel setItemWithTitle:nil text:[category.name lowercaseString] image:category.id tag:MENU_TAG_EXPLORE andObject:category]];
    }
}

-(void) insertOptionsOnList{
    
    [arrayitensOptions addObject:[LeftMenuModel setItemWithText:nil image:nil andTag:MENU_TAG_LOGO]];
    
    [arrayitensOptions addObject:[LeftMenuModel setItemWithText:[NSLocalizedString(@"LEFT_MENU_ITEM_ACCOUNT",nil) lowercaseString] image:@"ic_action_account" andTag:MENU_TAG_ACCOUNT]];
    
    if([StoreConfig getStore].addresses != nil && [[StoreConfig getStore].addresses count] > 0 ){
        [arrayitensOptions addObject:[LeftMenuModel setItemWithText:[NSLocalizedString(@"LEFT_MENU_ITEM_STORES",nil)lowercaseString] image:@"ic_action_place" andTag:MENU_TAG_STORES]];
    }
    
    if(([StoreConfig getStore].phones != nil && [[StoreConfig getStore].phones count] > 0 ) || ([StoreConfig getStore].emails != nil && [[StoreConfig getStore].emails count] > 0 )){
        [arrayitensOptions addObject:[LeftMenuModel setItemWithText:[NSLocalizedString(@"LEFT_MENU_ITEM_CONTACT",nil)lowercaseString] image:@"ic_action_phone" andTag:MENU_TAG_CONTACT]];
    }
    
    indexToChangeOption = [arrayitensOptions count];
}


#pragma UITableView DataSource
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_OF_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return [arrayitensOptions count];
            
        default:
            return [arrayCurrentCategories count];
    }
}


-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 0:
            return [self getCellsForDefaultSections:tableView andIndexPath:indexPath];
        default:
            return [self getCellsForCategorySections:tableView andIndexPath:indexPath];
    }
}


-(UITableViewCell*) getCellsForDefaultSections:(UITableView*) tableView andIndexPath:(NSIndexPath*) indexPath{
    if(indexPath.row == 0){
        return [self configureLogoCell:tableView andIndexPath:indexPath];
    }
    else{
        return [self getCellsForOptionsSections:tableView andIndexPath:indexPath];
    }
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return (indexPath.section == 0 && indexPath.row == 0) ? CELL_HEIGHT_LOGO: CELL_HEIGHT_CATEGORY;
}

#pragma OPTIONS CELL
-(UITableViewCell*) configureLogoCell:(UITableView*) tableView andIndexPath:(NSIndexPath*) indexPath{
    
    CategoryLogoCell* logoCell = [tableView dequeueReusableCellWithIdentifier:CELL_LOGO_IDENTIFICATION_ITEM];
    logoCell.selectionStyle = UITableViewCellSelectionStyleNone;
    logoCell.contentView.backgroundColor = [StoreConfig getStoreColor];    
    [logoCell.storeLoge sd_setImageWithURL:[NSURL URLWithString:[StoreConfig getStore].logo] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [logoCell.contentView setNeedsDisplay];
        [logoCell.storeLoge setNeedsDisplay];
    }];
    return logoCell;
}
-(UITableViewCell*) getCellsForOptionsSections:(UITableView*) tableView andIndexPath:(NSIndexPath*) indexPath{
        
    CategoryCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFICATION_ITEM];
    if(cell == nil){
        cell = [[CategoryCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CELL_IDENTIFICATION_ITEM];
    }
    LeftMenuModel * item = [arrayitensOptions objectAtIndex:indexPath.row];
    [cell.viewFooter setHidden:indexPath.row != (indexToChangeOption -1)];
    
    cell.categoryImage.image = nil;
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[Utils firstCharUpperCase:item.text]];
    [attributeString addAttribute:NSFontAttributeName
                            value:[UIFont boldSystemFontOfSize: cell.categoryName.font.pointSize]
                            range:NSMakeRange(0, [item.text length]-1)];
    
    cell.categoryName.attributedText = attributeString;
    
    cell.categoryImage.image = [UIImage imageNamed:item.image];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    return cell;
}


#pragma CATEGORIES CELL
-(UITableViewCell*) getCellsForCategorySections:(UITableView*) tableView andIndexPath:(NSIndexPath*) indexPath{

    LeftMenuModel * item = [arrayCurrentCategories objectAtIndex:indexPath.row];
    CategoryCell* cell = [tableView dequeueReusableCellWithIdentifier: item.tag == MENU_TAG_NOT_SELECTED ?  CELL_IDENTIFICATION_ITEM_WITHOUT_IMAGE : CELL_IDENTIFICATION_ITEM];
    [cell.viewFooter setHidden:YES];
    
    [self configureCategoryCell:cell item:item andIndexPath:indexPath];
    return cell;
}
-(void) configureCategoryCell:(CategoryCell*) cell item : (LeftMenuModel *)item andIndexPath:(NSIndexPath*)indexPath{
    
    cell.categoryName.text = [Utils firstCharUpperCase:item.text];
    cell.categoryImage.image = nil;
    cell.tag = indexPath.row;

    if(item.tag == MENU_TAG_EXPLORE){

        if(item.imageUrl){
            [cell.categoryImage sd_setImageWithURL:[NSURL URLWithString: item.imageUrl]];
        }else{

            [[ServiceApiMapper Api] getCategoryImage:item.image withBlock:^(NSObject * object, NSError * error) {

                CategoryCell *blockCell = (CategoryCell*)[self.tableView cellForRowAtIndexPath:indexPath];
                
                if(!blockCell || indexPath.section == 0){
                    return;
                }
                
                if(object){
                    item.imageUrl = (NSString*)object;
                    [blockCell.categoryImage sd_setImageWithURL:[NSURL URLWithString: item.imageUrl] ];
                }else{
                    blockCell.categoryImage.image = [UIImage imageNamed:@"ic_action_explore"];
                }
                [blockCell setNeedsLayout];
            }];
        }
    }else{
        if(item.tag != MENU_TAG_NOT_SELECTED){
            cell.categoryImage.image = [UIImage imageNamed:item.image];
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}




-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    switch (indexPath.section) {
        case 0:
            if(indexPath.row == 0){
                return;
            }
            return [self callItemOnContentView:[arrayitensOptions objectAtIndex: indexPath.row] andCategory:nil];
        default:{
            LeftMenuModel* lefteMenuModel = (LeftMenuModel*)[arrayCurrentCategories objectAtIndex:indexPath.row];
            if(lefteMenuModel.tag == MENU_TAG_NOT_SELECTED){
                return;
            }
            ProductCategory* category = (ProductCategory*) lefteMenuModel.object;
            return [self callItemOnContentView:[arrayCurrentCategories objectAtIndex: indexPath.row] andCategory:category];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
   
    [self.view endEditing:YES];
    [searchBar resignFirstResponder];
    NSString* search = searchBar.text;
    
    [GlobalEventTracker paramsSearch:search];
    
    TheSidebarController * controller = self.sidebarController;
    NavigationViewControllerBusiness * navigationController = (NavigationViewControllerBusiness *)controller.contentViewController;
    [navigationController onSearchProduct:search];
    [controller dismissSidebarViewController];
}

-(void) callItemOnContentView :(LeftMenuModel*) item andCategory:(ProductCategory*) category{
    
    [self.view endEditing:YES];

    if(!category || !category.childrens || category.childrens.count == 0){
        [self navigationCallContentView:item andCategory:category];
    }
    else{
        
        arrayCurrentCategories = [NSMutableArray new];
        
        if(item.tag == MENU_TAG_RETURN){
            
            NSArray * listTest = [self getCategoryList:(ProductCategory*)item.object onList:self.categories];
            if([listTest isEqual:self.categories]){
                   [self insertCategoriesOnList];
            }
            else{
                [self insertCategories:[self getParentFromCategory:(ProductCategory*)item.object onList:self.categories]];
            }
        }
        else{
            [self insertCategories:category];
        }

        [self.tableView reloadData];
    }
}
-(void) insertCategories:(ProductCategory*) category{
    [arrayCurrentCategories addObject:[LeftMenuModel setItemWithTitle: nil text:[category.name lowercaseString] image:@"" tag:MENU_TAG_NOT_SELECTED andObject:category]];
    [arrayCurrentCategories addObject:[LeftMenuModel setItemWithTitle: nil text:[NSLocalizedString(@"LEFT_MENU_ITEM_BACK",nil)lowercaseString] image:@"ic_action_arrowback" tag:MENU_TAG_RETURN andObject:category]];
    
    for(ProductCategory* categoryChild in category.childrens){
        [arrayCurrentCategories addObject:[LeftMenuModel setItemWithTitle: nil text:[categoryChild.name lowercaseString] image:categoryChild.id tag:MENU_TAG_EXPLORE andObject:categoryChild]];
    }
}

-(NSArray*) getCategoryList:(ProductCategory*) category onList:(NSArray*) items{
    for(ProductCategory* categoryChild in items){
        if([category.id isEqualToString:categoryChild.id]){
            return items;
        }else{
            if(categoryChild.childrens){
                NSArray* itemsChild = [self getCategoryList:category onList:categoryChild.childrens];
                if(itemsChild){
                    return itemsChild;
                }
            }
        }
    }
    return nil;
}

-(ProductCategory*) getParentFromCategory:(ProductCategory*) category onList:(NSArray*) list{
    
    for(ProductCategory* categoryChild in list){
        if([category.id isEqualToString:categoryChild.id]){
            return categoryChild;
        }
        else{
            if(categoryChild.childrens){
             
                ProductCategory* categoryFromChild = [self getParentFromCategory:category onList:categoryChild.childrens];
                if(categoryFromChild){
                    return categoryChild;
                    //return categoryFromChild;
                }
            }
        }
    }
    return category;
}
-(void) navigationCallContentView:(LeftMenuModel*) item andCategory:(ProductCategory*) category{
    
    TheSidebarController * controller = self.sidebarController;
    NavigationViewControllerBusiness * navigationController = (NavigationViewControllerBusiness *)controller.contentViewController;
    if(category){
        [GlobalEventTracker categoryView:category];
        navigationController.currentCategory = category;
    }
    [navigationController onLeftMenuItemSelected:item.tag];
    [controller dismissSidebarViewController];
}




@end
