//
//  LeftMenuViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 08/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "CategoryCell.h"
#import "CategoryLogoCell.h"
#import "StoreConfig.h"
#import "LeftMenuModel.h"
#import "NavigationViewController.h"
#import <TheSidebarController/TheSidebarController.h>
#import "Utils.h"
#import "Email.h"
#import "Phones.h"
#import "Dialog.h"
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>

#define NUMBER_OF_SECTIONS 1
#define CELL_IDENTIFICATION_ITEM @"CategoryCell"
#define CELL_IDENTIFICATION_LOGO @"CategoryLogoCell"

#define CELL_SIZE_LOGO 130
#define CELL_SIZE_ITEM 70
#define CELL_LINE_DIVIDER_HEIGHT 1.0f
#define FONT_SIZE_SMALL 10

#define PADDING_BOTTOM_TABLE_VIEW 30

@interface LeftMenuViewController (){
    NSMutableArray* arrayitens;
    NSArray* arrayitensChildrens;
}

@end

@implementation LeftMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    
}

-(void) setupView{
    
    NSMutableArray * childrens;
    
    self.view.backgroundColor = [StoreConfig getStoreColor];
    arrayitens = [NSMutableArray new];
    
    //Create a margin bottom
    self.tableView.contentInset = UIEdgeInsetsMake(PADDING_BOTTOM_TABLE_VIEW*2, 0, PADDING_BOTTOM_TABLE_VIEW, 0);
    
    [arrayitens addObject:[LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_EXPLORE",nil) image:@"ic_action_explore" andTag:MENU_TAG_EXPLORE]];
    
     childrens = [NSMutableArray new];
    
    if([StoreConfig getStore].facebook || [StoreConfig getStore].instagram || [StoreConfig getStore].twitter ){
        [childrens addObject: [LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_SOCIAL",nil) image:@"ic_action_arrowback" andTag:MENU_TAG_RETURN]];
        
        if([StoreConfig getStore].facebook){
            [childrens addObject: [LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_FACEBOOK",nil) image:@"ic_action_facebook" andTag:MENU_TAG_FACEBOOK]];
        }
        if([StoreConfig getStore].instagram){
            [childrens addObject: [LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_INSTAGRAM",nil) image:@"ic_action_instagram" andTag:MENU_TAG_INSTAGRAM]];
        }
        if([StoreConfig getStore].twitter){
            [childrens addObject: [LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_TWITTER",nil) image:@"ic_action_twitter" andTag:MENU_TAG_TWITTER]];
        }
        [arrayitens addObject:[LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_SOCIAL",nil) image:@"ic_action_sociais" tag:MENU_TAG_SOCIAL andChildrens:childrens]];
    }
    
    if([StoreConfig getStore].youtube){
        [arrayitens addObject:[LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_VIDEOS",nil) image:@"ic_action_videos" andTag:MENU_TAG_VIDEOS]];
    }
    
    if([StoreConfig getStore].addresses && [[StoreConfig getStore].addresses count] > 0){
    
        childrens = [NSMutableArray new];

        [childrens addObject: [LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_STORES",nil) image:@"ic_action_arrowback" andTag:MENU_TAG_RETURN]];
        
        for(Address* address in [StoreConfig getStore].addresses){
            [childrens addObject: [LeftMenuModel setItemWithTitle:address.city text:[Utils convertAddressesToString:address] image:@"ic_action_place" tag:MENU_TAG_ADDRESS andObject:address]];
        }
        [arrayitens addObject:[LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_STORES",nil) image:@"ic_action_stores" tag:MENU_TAG_EXPLORE andChildrens:childrens]];
    }
    
    //CIA SHOP
   // [arrayitens addObject:[LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_ACCOUNT",nil) image:@"ic_action_account" andTag:MENU_TAG_ACCOUNT]];
    
    
    if(([StoreConfig getStore].emails && [[StoreConfig getStore].emails count] > 0) || ([StoreConfig getStore].phones && [[StoreConfig getStore].phones count] > 0)){
        
        childrens = [NSMutableArray new];
        [childrens addObject: [LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_CONTACT",nil) image:@"ic_action_arrowback" andTag:MENU_TAG_RETURN]];

        for(Email* email in [StoreConfig getStore].emails){
             [childrens addObject: [LeftMenuModel setItemWithTitle:email.title text:email.email image:@"ic_action_email" tag:MENU_TAG_EMAIL andObject:email]];
        }
    
        for(Phones* phone in [StoreConfig getStore].phones){
            [childrens addObject: [LeftMenuModel setItemWithTitle:phone.title text: [Utils convertPhoneToString:phone] image:@"ic_action_phone" tag:MENU_TAG_PHONE andObject:phone]];
        }
        
        [arrayitens addObject:[LeftMenuModel setItemWithText:NSLocalizedString(@"LEFT_MENU_ITEM_CONTACT",nil) image:@"ic_action_email" tag:MENU_TAG_CONTACT andChildrens:childrens]];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSArray*) correctArrayToUse{
    return arrayitensChildrens != nil ? arrayitensChildrens : arrayitens ;
}


#pragma UITableView DataSource
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_OF_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[self correctArrayToUse] count] + 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        CategoryLogoCell * logoCell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFICATION_LOGO];
        logoCell.selectionStyle = UITableViewCellSelectionStyleNone;
        logoCell.contentView.backgroundColor = [StoreConfig getStoreColor];
        
        NSString* url = [StoreConfig getStore].logo;
        [logoCell.imageView sd_setImageWithURL:[NSURL URLWithString:url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }];
        
        return logoCell;
    }
    else{
        
        LeftMenuModel * item = [[self correctArrayToUse] objectAtIndex:indexPath.row - 1];
        CategoryCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFICATION_ITEM];
        [self configureCategoryCell:cell andItem:item];
        return cell;
    }
}

-(void) configureCategoryCell:(CategoryCell*) cell andItem : (LeftMenuModel *)item{
    
    if(item.title == nil){
        cell.categoryName.text = [item.text uppercaseString];
    }
    else{
        
        NSString * string = [NSString stringWithFormat:@"%@\n%@",[item.title uppercaseString], [item.text uppercaseString]];
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:string];
        [attributeString addAttribute:NSFontAttributeName
                                value:[UIFont systemFontOfSize:FONT_SIZE_SMALL]
                                range:NSMakeRange([[item.title uppercaseString] length], [[item.text uppercaseString] length] + 1)];
        cell.categoryName.attributedText = attributeString;
    }
    

    cell.categoryImage.image = [UIImage imageNamed:item.image];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath.row == 0 ? CELL_SIZE_LOGO: [self heightForCategoryCellWithItem:[[self correctArrayToUse] objectAtIndex:indexPath.row - 1]];
}

- (CGFloat)heightForCategoryCellWithItem:(LeftMenuModel *)item {
    static CategoryCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFICATION_ITEM];
    });
    
    [self configureCategoryCell: sizingCell andItem:item];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + CELL_LINE_DIVIDER_HEIGHT; 
}


#pragma UITableView Delegate
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    if(indexPath.row == 0){
        return;
    }
    
    LeftMenuModel* item = [[self correctArrayToUse] objectAtIndex:indexPath.row - 1];
    if(item.childrens){
        /*if has childrens, re-insert itens on uitableview*/
        arrayitensChildrens = item.childrens;
        [tableView reloadData];
    
    }else{
        
        switch (item.tag) {
            case MENU_TAG_RETURN:
                /*Return to main menu*/
                arrayitensChildrens = nil;
                [tableView reloadData];
                break;
                
            case MENU_TAG_EMAIL:
                //ACTION SEND EMAIL
                [self actionSendEmail:(Email*) item.object];
                break;
                
            case MENU_TAG_PHONE:
                //ACTION CALL
                [self actionCallStore:(Phones*) item.object];
                break;
                
            case MENU_TAG_ADDRESS:
                //ACTION CALL MAPS
                [self actionMap:(Address*) item.object];
                break;
                
            default:
                [self callItemOnContentView: item];
                break;
        }
        
    }
}


-(void) actionSendEmail:(Email*) email{
    
    MFMailComposeViewController *mailCont = [MFMailComposeViewController new];
    mailCont.mailComposeDelegate = self;
    [mailCont setToRecipients:@[email.email]];
    
    [self presentViewController:mailCont animated:YES completion:nil];
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) actionCallStore:(Phones*) phone{
    NSString* phoneString = [NSString stringWithFormat:@"telprompt://+%@%@%@",phone.countryCode,phone.areaCode,phone.number];
    NSURL *url = [NSURL URLWithString:phoneString];
    [[UIApplication sharedApplication] openURL:url];
}

-(void) actionMap:(Address*) address{
    
    NSString* addressString = [NSString stringWithFormat:@"%@,%@,%@,%@",address.addressLine1, address.neighborhood,address.city,address.state ];
    
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:addressString
                     completionHandler:^(NSArray *placemarks, NSError *error) {
                         
                         if(error){
                             [Dialog showToast:@"Não foi possível calcular o trajeto para esse endereço" onView:self.view];
                             return ;
                         }
                         
                         CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                         MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:geocodedPlacemark];
                         
                         MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                         [mapItem setName:geocodedPlacemark.name];
                         
                         NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
                         
                         MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
                         
                         [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem] launchOptions:launchOptions];
                         
                     }];
    }
    
}



-(void) callItemOnContentView :(LeftMenuModel*) item{
    
    TheSidebarController * controller = self.sidebarController;
    
    [controller dismissSidebarViewController];
    
    NavigationViewController * navigationController = (NavigationViewController *)controller.contentViewController;
    
    [navigationController onLeftMenuItemSelected:item.tag];
}

@end
