//
//  AccountInformationViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "AccountInformationViewController.h"
#import "ServiceApiMapper.h"
#import "User.h"
#import "HelperTextFormatter.h"
#import "Dialog.h"

@interface AccountInformationViewController(){
    User* user;
}
@end
@implementation AccountInformationViewController

-(void) viewDidLoad{
    [super viewDidLoad];
    [self setupNavigationBarWithBack: NSLocalizedString(@"ACCOUNT_INFORMATION_VIEW_CONTROLLER_TITLE",nil)];
    [self setupView];
}
-(void) setupView{
    [self.confirm setBackgroundColor:[StoreConfig getStoreColor]];
    
    self.phone.format = @"(XX)XXXXXXXXX";
    [self getUserInformation];
    
}
-(void) getUserInformation{
    
    user = [ServiceStorageUser getUserLogged];

    [[ServiceApiMapper Api] getUser:user withBlock:^(BOOL sucess, NSError * error) {
        
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getUserInformation) withObject:nil afterDelay:8];
        }
        else{
            
            [self setEnable:YES];
            user = [ServiceStorageUser getUserLogged];
            [self setInformtions];
        }
    }];
}

-(void) setInformtions{
    
    self.email.text = user.email;
    NSArray * names = [user.name componentsSeparatedByString: @" "];
    self.firstname.text = names[0];
    if([names count] > 0){
        NSString* value = @"";
        for(int count = 1; count < [names count] ; count++){
            value = [NSString stringWithFormat:@"%@%@ ",value,names[count]];
        }
        self.lastname.text = value;
    }
    
    NSMutableString *mutableString = [NSMutableString stringWithString:user.phone];
    
    if([mutableString length] > 3){
        [mutableString insertString:@"(" atIndex:0];
        [mutableString insertString:@")" atIndex:3];
    }
    
    self.phone.text = mutableString;
   
}

- (IBAction)onConfirmClick:(id)sender {
    if([self checkAllInformations]){
        [self changeUserInformation];
    }
}

-(void) changeUserInformation{
    
    [self.view endEditing:YES];
    
    user.email = self.email.text;
    user.name = [NSString stringWithFormat:@"%@ %@",self.firstname.text ,self.lastname.text];
    user.phone  = [[self.phone.text stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
    
    [self setEnable:NO];
    
    [[ServiceApiMapper Api] updateUser:user withBlock:^(BOOL success, NSError * error) {
        [self setEnable:YES];
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
        }
        else{
            [Dialog showDialog:@"Usuário atualizado com sucesso"];
        }
    }];
}

-(void) setEnable:(BOOL) enable{
    enable ? [self.activityIndicator stopAnimating]:[self.activityIndicator startAnimating];
    [self.email setUserInteractionEnabled:enable];
    [self.firstname setUserInteractionEnabled:enable];
    [self.lastname setUserInteractionEnabled:enable];
    [self.phone setUserInteractionEnabled:enable];
    [self.confirm setUserInteractionEnabled:enable];
}


-(BOOL) checkAllInformations
{
    //EMAIL
    if(![HelperTextFormatter isValidEmail: self.email.text]){
        [Dialog showToast:@"Favor informar um email válido" onView:self.view];
        return NO;
    }
    
    //FIRST NAME
    if([self.firstname.text length] == 0){
        [Dialog showToast:@"Favor inserir seu primeiro nome" onView:self.view];
        return NO;
    }
    
    //LAST NAME
    if([self.lastname.text length] == 0){
        [Dialog showToast:@"Favor inserir seu último nome" onView:self.view];
        return NO;
    }
    
    //PHONE
    if([self.phone.text length] < 7){
        [Dialog showToast:@"Favor inserir seu  telefone" onView:self.view];
        return NO;
    }
    return YES;
}

@end
