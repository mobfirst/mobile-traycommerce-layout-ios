//
//  CreateAccountViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"
#import "REFormattedNumberField.h"
#import <APIConnection/Constants.h>
#import "ViewTextFiedCustom.h"

@interface CreateAccountViewController : CenterModelViewControllers<UITextFieldDelegate>

@property (strong, nonatomic) NSArray* products;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *confirmEmail;
@property (strong, nonatomic) IBOutlet REFormattedNumberField *personalID;
@property (strong, nonatomic) IBOutlet UITextField *firstname;
@property (strong, nonatomic) IBOutlet UITextField *lastname;
@property (strong, nonatomic) IBOutlet REFormattedNumberField *phone;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *confirmPassword;
@property (strong, nonatomic) IBOutlet UIButton *confirm;
@property (strong, nonatomic) IBOutlet UITextField *personalRg;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *actionIndicator;

@property (strong, nonatomic) IBOutlet ViewTextFiedCustom *cpfContainer;
@property (strong, nonatomic) IBOutlet ViewTextFiedCustom *rgContainer;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cpfContainerTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *rgContainerTop;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *cpfContainerHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *rgContainerHeight;

@property (nonatomic) bool rgRequired;
@property (nonatomic) bool cpfRequired;

- (IBAction)onConfirmClick:(id)sender;
@end
