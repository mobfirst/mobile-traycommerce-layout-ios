//
//  AccountInformationViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"
#import "REFormattedNumberField.h"

@interface AccountInformationViewController : CenterModelViewControllers<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet REFormattedNumberField *phone;
@property (strong, nonatomic) IBOutlet UITextField *firstname;
@property (strong, nonatomic) IBOutlet UITextField *lastname;
@property (strong, nonatomic) IBOutlet UIButton *confirm;
- (IBAction)onConfirmClick:(id)sender;
@end
