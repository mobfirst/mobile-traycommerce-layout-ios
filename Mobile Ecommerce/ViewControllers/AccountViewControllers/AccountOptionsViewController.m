//
//  AccountOptionsViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "AccountOptionsViewController.h"
#import "AccountInformationViewController.h"
#import "SettingCell.h"
#import "ServiceApiMapper.h"
#import "LeftMenuModel.h"
#import "NavigationViewController.h"
#import <TheSidebarController/TheSidebarController.h>
#import "Dialog.h"

#define NUMBER_OF_SECTIONS 1
#define FONT_SIZE_SMALL 10
#define CELL_IDENTIFICATION_ITEM @"SettingCell"
#define ALERT_DIALOG_CONFIRM 0



@interface AccountOptionsViewController ()<UIAlertViewDelegate>{
    NSMutableArray* arrayitens;
}
@end

@implementation AccountOptionsViewController

-(void) viewDidLoad{
    [super viewDidLoad];
    [self setupNavigationBar: NSLocalizedString(@"ACCOUNT_OPTIONS_VIEW_CONTROLLER_TITLE",nil)];
    [self setupView];
    
}
-(void) setupView{
    arrayitens = [NSMutableArray new];
    [arrayitens addObject:[LeftMenuModel setItemWithText:@"MINHAS INFORMAÇÕES"image:@"icon_login" andTag:MENU_TAG_EXPLORE]];
    [arrayitens addObject:[LeftMenuModel setItemWithText:@"SAIR"image:@"icon_logout" andTag:MENU_TAG_EXPLORE]];
}



#pragma UITableView DataSource
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_OF_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [arrayitens count];
}


-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SettingCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFICATION_ITEM];
    LeftMenuModel * item = [arrayitens objectAtIndex:indexPath.row];
    [self configureCategoryCell:cell andItem:item];
    return cell;
}

-(void) configureCategoryCell:(SettingCell*) cell andItem : (LeftMenuModel *)item{
    
    if(item.title == nil){
        cell.categoryName.text = [item.text uppercaseString];
    }
    else{
        
        NSString * string = [NSString stringWithFormat:@"%@\n%@",[item.title uppercaseString], [item.text uppercaseString]];
        
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:string];
        [attributeString addAttribute:NSFontAttributeName
                                value:[UIFont systemFontOfSize:FONT_SIZE_SMALL]
                                range:NSMakeRange([[item.title uppercaseString] length], [[item.text uppercaseString] length] + 1)];
        cell.categoryName.attributedText = attributeString;
    }
    cell.categoryImage.image = [UIImage imageNamed:item.image];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.block = ^(SettingCell* cell){
       [self onSelectedCell: [self.tableView indexPathForCell:cell]];
    };
    [cell setupView];
}

-(void) onSelectedCell:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:
            [self callAccountInformation];
            break;
            
        case 1:
            [self logout];
            break;
    }
}

-(void) callAccountInformation{
    NavigationViewController* navigationController = (NavigationViewController* )self.navigationController;
    AccountInformationViewController * accountInformation = [navigationController returnViewControllerFromIdentifier: [navigationController className:[AccountInformationViewController class]]];
    [navigationController pushViewController:accountInformation animated:YES];
}

-(void) logout{
    
    UIAlertView* confirmation = [[UIAlertView alloc] initWithTitle:@"" message:@"Deseja realmente sair?" delegate:self cancelButtonTitle:@"Sim" otherButtonTitles:@"Não", nil];
    [confirmation show];
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == ALERT_DIALOG_CONFIRM){
        [ServiceStorageUser deleteSaveUser];
        [Dialog showDialog:@"Logout realizado com sucesso"];
        TheSidebarController * controller = self.sidebarController;
        NavigationViewController * navigationController = (NavigationViewController*)controller.contentViewController;
        [navigationController onLeftMenuItemSelected:MENU_TAG_EXPLORE];
    }
}

@end
