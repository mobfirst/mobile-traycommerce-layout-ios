//
//  CreateAccountViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "CreateAccountViewController.h"
#import "StoreConfig.h"
#import "HelperTextFormatter.h"
#import "Dialog.h"
#import "User.h"
#import "ServiceApiMapper.h"
#import <TheSidebarController/TheSidebarController.h>
#import "NavigationViewController.h"
#import "Constants.h"
#import "ProductViewController.h"

@implementation CreateAccountViewController


-(void) viewDidLoad {
    
    [super viewDidLoad];
    [self setupNavigationBarWithBack: NSLocalizedString(@"CREATE_VIEW_CONTROLLER_TITLE", nil)];
    [self setupView];
}

-(void) setupView {
    
    self.confirm.backgroundColor = [StoreConfig getStoreColor];
    self.phone.format = @"(XX)XXXXXXXXX";
    self.personalID.format = @"XXX.XXX.XXX-XX";
    
    self.confirmPassword.delegate = self;
    
    self.rgRequired = [[ServiceApiMapper Api] rgRequired] == 0 ? NO : YES;
    self.cpfRequired = [[ServiceApiMapper Api] cpfRequired]  == 0 ? NO : YES;
    
    [self setupIdFieldsWithRgRequirement:[self rgRequired] andCpfRequirement:[self cpfRequired]];
}

-(void) setupIdFieldsWithRgRequirement:(bool) rgRequired andCpfRequirement:(bool) cpfRequired {
    
    if(!rgRequired && !cpfRequired) {
        
        self.cpfContainerHeight.constant = 0;
        self.rgContainerHeight.constant = 0;
        
        self.rgContainerTop.constant = 0;
        self.cpfContainerTop.constant = 0;
        
        self.cpfContainer.hidden = YES;
        self.rgContainer.hidden = YES;
    }
    else if(!rgRequired) {
        
        self.rgContainerHeight.constant = 0;
        self.rgContainerTop.constant = 0;
        self.rgContainer.hidden = YES;
    }
    else {
        
        self.cpfContainerHeight.constant = 0;
        self.rgContainerTop.constant = 0;
        self.cpfContainer.hidden = YES;
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [self createAccount];
    return YES;
}


- (IBAction)onConfirmClick:(id)sender {
    [self createAccount];
}

-(void) createAccount{
    
    [self.view endEditing:YES];
    
    if([self checkAllInformations]){
        
        [self setEnable:NO];
    
        User * user = [User new];
        user.email  = self.email.text;
        user.name = [NSString stringWithFormat:@"%@ %@",self.firstname.text ,self.lastname.text];
        
        NSString* phone = [ self.phone.text stringByReplacingOccurrencesOfString:@"(" withString:@""];
        phone =  [ phone stringByReplacingOccurrencesOfString:@")" withString:@""];
        
        user.phone  = phone;
        user.password = self.password.text;
        
        if([self rgRequired]) {
            user.personalRg = self.personalRg.text;
        }
        else {
            user.personalRg = @"";
        }

        NSString* personalId = [ self.personalID.text stringByReplacingOccurrencesOfString:@"." withString:@""];
        personalId = [ personalId stringByReplacingOccurrencesOfString:@"-" withString:@""];
        
        if([self cpfRequired]) {
            user.personalID = personalId;
        }
        else {
            user.personalID = @"";
        }
        
        [[ServiceApiMapper Api]createUser:user withBlock:^(BOOL sucess, NSError * error) {
            [self setEnable:YES];
            if(error){
                [Dialog showDialog:@"Falha na criação do usuário"];
            }
            else{
                [self onCreateSuccess];
            }
        }];
    }
}

-(void) onCreateSuccess{
    
    if(self.products){
        [self insertProductOnCart];
    }else{
    
        [Dialog showDialog:@"Usuário criado com sucesso"];
        TheSidebarController * controller = self.sidebarController;
        NavigationViewController * navigationController = (NavigationViewController*)controller.contentViewController;
        [navigationController onLeftMenuItemSelected:MENU_TAG_EXPLORE];
    }
}


-(void) insertProductOnCart{
    
    [self setEnable:NO];
    
    [[ServiceApiMapper Api] insertProductsOnCart:self.products withBlock:^(BOOL success, NSError * error) {
        
        [self setEnable:YES];
        
        if(error){
            
            if(error.code == ERROR_CONVERTION_CODE){
                
                
                UIViewController* controller = nil;
                
                for(UIViewController* viewController  in  self.navigationController.viewControllers){
                    if([viewController isKindOfClass:[ProductViewController class]]){
                        controller = viewController;
                        break;
                    }
                }
                
                if(controller){
                     [self.navigationController  popToViewController:controller animated:YES];
                }
                else{
                    [self.navigationController  popToRootViewControllerAnimated:YES];
                }
                
                
                
                
                [Dialog showDialog:error.localizedDescription];
            }
            
            [Dialog showConnectionErrorOnView:self.view];
        }else{
            if(success){
                [self.navigationController popToRootViewControllerAnimated:YES];
                [Dialog showSuccessAddCart];
            }else{
                [Dialog showToast:@"Problemas ao adicionar ao carrinho" onView:self.view];
            }
        }
    }];
}

-(void) setEnable:(BOOL) enable{
    enable ? [self.actionIndicator stopAnimating]:[self.actionIndicator startAnimating];
    [self.email setUserInteractionEnabled:enable];
    [self.confirmEmail setUserInteractionEnabled:enable];
    [self.password setUserInteractionEnabled:enable];
    [self.confirmEmail setUserInteractionEnabled:enable];
    [self.personalID setUserInteractionEnabled:enable];
    [self.confirm setUserInteractionEnabled:enable];
    [self.firstname setUserInteractionEnabled:enable];
    [self.lastname setUserInteractionEnabled:enable];
    [self.phone setUserInteractionEnabled:enable];
}


-(BOOL) checkAllInformations
{
    //EMAIL
    if(![HelperTextFormatter isValidEmail: self.email.text]){
        [Dialog showToast:@"Favor informar um email válido" onView:self.view];
        return NO;
    }
    
    if([self.confirmEmail.text length] == 0){
        [Dialog showToast:@"Favor confirmar seu email" onView:self.view];
        return NO;
    }
    if(![self.confirmEmail.text isEqualToString:self.email.text]){
        [Dialog showToast:@"Os emails diferem" onView:self.view];
        return NO;
    }
    
    //Personal ID
    if([self.personalID.text length] != 14 && [self cpfRequired]){
        [Dialog showToast:@"Favor inserir um CPF válido" onView:self.view];
        return NO;
    }
   
    //Personal RG
    if([self.personalRg.text length] == 0 && [self rgRequired]){
        [Dialog showToast:@"Favor inserir um RG válido" onView:self.view];
        return NO;
    }

    //FIRST NAME
    if([self.firstname.text length] == 0){
        [Dialog showToast:@"Favor inserir seu primeiro nome" onView:self.view];
        return NO;
    }
    
    //LAST NAME
    if([self.lastname.text length] == 0){
        [Dialog showToast:@"Favor inserir seu último nome" onView:self.view];
        return NO;
    }
    
    //PHONE
    if([self.phone.text length] < 7){
        [Dialog showToast:@"Favor inserir seu  telefone" onView:self.view];
        return NO;
    }
    
    //PASSWORD
    if(![self.password.text isEqualToString:self.confirmPassword.text]){
        [Dialog showToast:@"As senhas diferem" onView:self.view];
        return NO;
    }
    
    if([self.password.text length] == 0||![HelperTextFormatter isValidPassword:self.password.text]){
        [Dialog showToast:@"Favor inserir sua senha, ela precisa ter de 4 a 20 caracteres" onView:self.view];
        return NO;
    }
    return YES;
}
@end
