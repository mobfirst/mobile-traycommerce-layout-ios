//
//  LoginViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"
#import <APIConnection/Constants.h>

@interface LoginViewController : CenterModelViewControllers<UITextFieldDelegate>

@property (strong, nonatomic) NSArray* products;
@property (strong, nonatomic) IBOutlet UITextField *email;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UIButton *outletBotton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *actionIndicator;
@property (strong, nonatomic) IBOutlet UILabel *createAccount;
@property (strong, nonatomic) IBOutlet UILabel *titleAccount;

- (IBAction)onConfirmClick:(id)sender;
@end
