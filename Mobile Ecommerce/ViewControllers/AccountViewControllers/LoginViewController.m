//
//  LoginViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "LoginViewController.h"
#import "StoreConfig.h"
#import "HelperTextFormatter.h"
#import "Dialog.h"
#import "User.h"
#import "ServiceApiMapper.h"
#import <TheSidebarController/TheSidebarController.h>
#import "NavigationViewController.h"
#import "Constants.h"
#import "CreateAccountViewController.h"
#import "ProductViewController.h"
#import <EventTracker/GlobalEventTracker.h>


@implementation LoginViewController


-(void) viewDidLoad{
    [super viewDidLoad];
    [self setupNavigationBar: NSLocalizedString(@"LOGIN_VIEW_CONTROLLER_TITLE",nil)];
    [self setupView];
}
-(void) setupView{
    
    [self.actionIndicator stopAnimating];
    [self.outletBotton setBackgroundColor:[StoreConfig getStoreColor]];
    self.titleAccount.text = [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"LOGIN_VIEW_CONTROLLER_ACCOUNT_TITLE",nil),[StoreConfig getStore].name];
    
    NSMutableAttributedString *createAccountString = [[NSMutableAttributedString alloc] initWithString:self.createAccount.text];
    [createAccountString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:[self.createAccount.text rangeOfString:@"Clique aqui"]];
    self.createAccount.attributedText = createAccountString;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.createAccount addGestureRecognizer:tapGestureRecognizer];
    
    self.password.delegate = self;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self loginAction];
    return YES;
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer{
    NavigationViewController* navigationController = (NavigationViewController* )self.navigationController;
    CreateAccountViewController * createAccountViewController = [navigationController returnViewControllerFromIdentifier: [navigationController className:[CreateAccountViewController class]]];
    createAccountViewController.products = self.products;
    [navigationController pushViewController:createAccountViewController animated:YES];
}

- (IBAction)onConfirmClick:(id)sender {
    [self loginAction];
}

-(void) loginAction{
    
    [self.view endEditing:YES];
    
    if([self isValidCredentials]){
        [self setEnable:NO];
        
        User* user = [User new];
        user.email = self.email.text;
        user.password = self.password.text;
        
        [[ServiceApiMapper Api] loginUser:user withBlock:^(BOOL success, NSError * error) {
            [self setEnable:YES];
            if(error){
                [Dialog showDialog:@"Não foi possível realizar o login, verifique suas credenciais"];
            }
            else{
                [self onLoginSuccess];
            }
        }];
    }
}

-(void) onLoginSuccess{
    
     if(self.products){
         [self insertProductOnCart];
     }
     else{
         [Dialog showDialog:@"Login realizado com sucesso"];
         TheSidebarController * controller = self.sidebarController;
         NavigationViewController * navigationController = (NavigationViewController*)controller.contentViewController;
         [navigationController onLeftMenuItemSelected:MENU_TAG_EXPLORE];
     }
}


-(void) insertProductOnCart{
    
    [self setEnable:NO];
    
    [[ServiceApiMapper Api] insertProductsOnCart:self.products withBlock:^(BOOL success, NSError * error) {
        
        [self setEnable:YES];
        
        if(error){
            
            if(error.code == ERROR_CONVERTION_CODE){
                UIViewController* controller = nil;
                
                for(UIViewController* viewController  in  self.navigationController.viewControllers){
                    if([viewController isKindOfClass:[ProductViewController class]]){
                        controller = viewController;
                        break;
                    }
                }
                
                if(controller){
                    [self.navigationController  popToViewController:controller animated:YES];
                }
                else{
                    [self.navigationController  popToRootViewControllerAnimated:YES];
                }
                
                [Dialog showDialog:error.localizedDescription];
            }
            else{
                [Dialog showConnectionErrorOnView:self.view];
            }
            
            
        }else{
            if(success){
                [self.navigationController popToRootViewControllerAnimated:YES];
                [Dialog showSuccessAddCart];
            }else{
                [Dialog showToast:@"Problemas ao adicionar ao carrinho" onView:self.view];
            }
        }
    }];
}

-(void) setEnable:(BOOL) enable{
    enable ? [self.actionIndicator stopAnimating]:[self.actionIndicator startAnimating];
    [self.email setUserInteractionEnabled:enable];
    [self.password setUserInteractionEnabled:enable];
    [self.outletBotton setUserInteractionEnabled:enable];
    [self.createAccount setUserInteractionEnabled:enable];
}

-(BOOL) isValidCredentials{
    if(![HelperTextFormatter isValidEmail:self.email.text]){
        [Dialog showToast:@"Favor inserir um email válido" onView:self.view];
        return NO;
    }
    if([self.password.text length] == 0){
        [Dialog showToast:@"Favor inserir a senha" onView:self.view];
        return NO;
    }
    return  YES;
}
@end
