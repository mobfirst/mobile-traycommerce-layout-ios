//
//  YouTubeViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 25/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"
#import "VideoViewController.h"

@interface YouTubeViewController : CenterModelViewControllers<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
