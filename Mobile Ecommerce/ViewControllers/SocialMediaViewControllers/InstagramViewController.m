//
//  InstagramViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 25/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "InstagramViewController.h"
#import "InstagramConnection.h"
#import "InstagramCell.h"
#import "StoreConfig.h"
#import "InstagramObjects.h"
#import "Dialog.h"

#define NUMBER_OF_SECTIONS 1
#define IDENTIFIER_CELL_CHOICER @"InstagramCell"

@interface InstagramViewController (){
    NSArray* itemsFromUser;
    NSArray* itemsFromTag;
    NSArray* itemsToShow;
}
@end

@implementation InstagramViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setupNavigationBar: NSLocalizedString(@"INSTAGRAM_VIEW_CONTROLLER_TITLE",nil)];
    self.tableView.separatorColor = [UIColor whiteColor];
    
    itemsToShow = [NSArray new];
    
    [self setupSegmentedControl];
    [self getInstagramPostByTag];
    [self getInstagramPostByUser];
}

-(void) setupSegmentedControl{
    self.segmentedControl.tintColor = [UIColor lightGrayColor];
    [self.segmentedControl setTitle:[NSString stringWithFormat:@"@%@",[StoreConfig getStore].instagram] forSegmentAtIndex:0];
    [self.segmentedControl setTitle:[NSString stringWithFormat:@"#%@",[StoreConfig getStore].instagramTag] forSegmentAtIndex:1];
}

-(void) getInstagramPostByTag{
    [InstagramConnection getAllMediasFromTag:[StoreConfig getStore].instagramTag andBlock:^(NSArray * items) {
        if(items){
            itemsFromTag = items;
            if(itemsFromUser){
                [self setupTableView];
            }
        }
        else{
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getInstagramPostByTag) withObject:nil afterDelay:8];
        }
    }];
}

-(void) getInstagramPostByUser{
    [InstagramConnection getAllMediaFromUser:[StoreConfig getStore].instagram andBlock:^(NSArray * items) {
        if(items){
            itemsFromUser = items;
            if(itemsFromTag){
                [self setupTableView];
            }
        }
        else{
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getInstagramPostByUser) withObject:nil afterDelay:8];
        }
    }];
}

-(void) setupTableView{
    itemsToShow = self.segmentedControl.selectedSegmentIndex == 0 ? itemsFromUser : itemsFromTag;
    self.tableView.separatorColor = [UIColor lightGrayColor];
    [self.tableView reloadData];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [self.activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma UITableView Datasource
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_OF_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [itemsToShow count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self getInstagramPost:[itemsToShow objectAtIndex:indexPath.row]];
}

-(InstagramCell*) getInstagramPost:(InstagramObjects*) object{
    InstagramCell* cell = [self.tableView dequeueReusableCellWithIdentifier:IDENTIFIER_CELL_CHOICER];
    [cell setupView:object];
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self heightForCategoryCellWithItem: [itemsToShow objectAtIndex:indexPath.row]];
}

- (CGFloat)heightForCategoryCellWithItem:(InstagramObjects*)item {
    static InstagramCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:IDENTIFIER_CELL_CHOICER];
    });
    [self setupCell: sizingCell andItem:item];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(InstagramCell *)sizingCell {
    [sizingCell layoutSubviews];
    return sizingCell.requiredCellHeight;
}

-(void) setupCell:(InstagramCell*) cell andItem : (InstagramObjects *)video{
    [cell setupView:video];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}


- (IBAction)onSegmentedChange:(id)sender {
    [self setupTableView];
}
@end
