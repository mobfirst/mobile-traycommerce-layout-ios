//
//  YouTubeViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 25/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "YouTubeViewController.h"
#import "YouTubeConnection.h"
#import "Dialog.h"
#import "YoutubeCell.h"
#import "YouTubeObject.h"
#import "NavigationViewController.h"


#define NUMBER_OF_SECTION 1
#define CELL_IDENTIFICATION @"YoutubeCell"

@interface YouTubeViewController (){
    NSArray* videosItems;
}

@end

@implementation YouTubeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar: NSLocalizedString(@"VIDEOS_VIEW_CONTROLLER_TITLE",nil)];
    videosItems = [NSArray new];
    self.tableView.separatorColor = [UIColor whiteColor];
    [self getVideos];
}

-(void) getVideos{
    
    [YouTubeConnection getAllVideosFromStoreChannel:^(NSArray * videos) {

        if(videos){
            videosItems = videos;
            [self.tableView reloadData];
            self.tableView.separatorColor = [UIColor lightGrayColor];
            [self.activityIndicator stopAnimating];
        }
        else{
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getVideos) withObject:nil afterDelay:8];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma UITableView

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_OF_SECTION;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [videosItems count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    YoutubeCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFICATION forIndexPath:indexPath];
    [cell setupView:[videosItems objectAtIndex:indexPath.row]];
    cell.block = ^(YouTubeObject* video){
        [self callVideoViewController:video];
    };
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self heightForCategoryCellWithItem: [videosItems objectAtIndex:indexPath.row]];
}

- (CGFloat)heightForCategoryCellWithItem:(YouTubeObject *)item {
    static YoutubeCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFICATION];
    });
    [self setupCell: sizingCell andItem:item];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(YoutubeCell *)sizingCell {
    [sizingCell layoutSubviews];
    return sizingCell.requiredCellHeight;
}

-(void) setupCell:(YoutubeCell*) cell andItem : (YouTubeObject *)video{
    [cell setupView:video];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void) callVideoViewController:(YouTubeObject*) video{
    NavigationViewController* navigationController = (NavigationViewController* )self.navigationController;
    VideoViewController * videoViewController = [navigationController returnViewControllerFromIdentifier: [navigationController className:[VideoViewController class]]];
    videoViewController.video = video;
    [navigationController pushViewController:videoViewController animated:YES];
}

@end
