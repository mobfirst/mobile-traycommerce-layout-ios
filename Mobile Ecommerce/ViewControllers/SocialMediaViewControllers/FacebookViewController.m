//
//  FacebookViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 25/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "FacebookViewController.h"
#import "StoreConfig.h"

@interface FacebookViewController ()

@end

@implementation FacebookViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar: NSLocalizedString(@"FACEBOOK_VIEW_CONTROLLER_TITLE",nil)];
    
    NSURL* nsUrl = [NSURL URLWithString:[StoreConfig getStore].facebook];
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl];
    [self.webview loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

@end
