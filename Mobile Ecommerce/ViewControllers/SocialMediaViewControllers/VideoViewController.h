//
//  VideoViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 19/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"
#import "YouTubeObject.h"

@interface VideoViewController : CenterModelViewControllers
@property (strong,nonatomic) YouTubeObject* video;

@end
