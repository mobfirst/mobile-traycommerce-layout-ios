//
//  InstagramViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 25/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"

@interface InstagramViewController : CenterModelViewControllers<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)onSegmentedChange:(id)sender;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
