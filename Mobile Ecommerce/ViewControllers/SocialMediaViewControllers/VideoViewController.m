//
//  VideoViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 19/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "VideoViewController.h"
#import "XCDYouTubeVideoPlayerViewController.h"

@implementation VideoViewController


- (void)viewDidLoad{
    [super viewDidLoad];
    [self setupNavigationBarWithBack: [self.video.title uppercaseString]];
    [self setupVideo];
}

-(void) setupVideo{
    XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:self.video.urlVideo];
    [videoPlayerViewController presentInView:self.view];
    [videoPlayerViewController.moviePlayer play];
}
@end
