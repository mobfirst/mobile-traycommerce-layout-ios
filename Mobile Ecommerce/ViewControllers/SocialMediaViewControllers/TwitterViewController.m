//
//  TwitterViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 25/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "TwitterViewController.h"
#import "TwitterConnection.h"
#import "StoreConfig.h"
#import "TwitterUrls.h"
#import "Dialog.h"

#define NUMBER_OF_SECTION 1
#define CELL_IDENTIFICATION @"TwitterCell"

@interface TwitterViewController (){
    NSArray* tweets;
}

@end

void (^block) (NSArray *);

@implementation TwitterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar: NSLocalizedString(@"TWITTER_VIEW_CONTROLLER_TITLE",nil)];
    self.tableView.separatorColor = [UIColor whiteColor];
    tweets = [NSArray new];
    block = ^(NSArray *tweetLis) {
        if(tweetLis){
            tweets = tweetLis;
            [self.tableView reloadData];
            [self.activityIndicator stopAnimating];
            self.tableView.separatorColor = [UIColor lightGrayColor];
        }
        else{
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getTweets) withObject:nil afterDelay:8];
        }
    };
    [self getTweets];
   
}

-(void) getTweets{
     [TwitterConnection getAllTweetsFromUserTimeline:[StoreConfig getStore].twitter andResponseBlock:block];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Ui TableView
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_OF_SECTION;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tweets.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TwitterCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFICATION forIndexPath:indexPath];
    [cell setupView:[tweets objectAtIndex:indexPath.row]];
    
    cell.block = ^(NSString* url){
        [self callNavigationToUrl:url];
    };
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self heightForCategoryCellWithItem: [tweets objectAtIndex:indexPath.row]];
}

- (CGFloat)heightForCategoryCellWithItem:(TwitterObject *)item {
    static TwitterCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFICATION];
    });
    [self setupCell: sizingCell andItem:item];
    return [self calculateHeightForConfiguredSizingCell:sizingCell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(TwitterCell *)sizingCell {
    [sizingCell layoutSubviews];
    return sizingCell.requiredCellHeight;
}

-(void) setupCell:(TwitterCell*) cell andItem : (TwitterObject *)tweet{
    [cell setupView:tweet];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

-(void) callNavigationToUrl:(NSString*) url{
    NSURL *urlToOpen = [NSURL URLWithString:url];
    [[UIApplication sharedApplication] openURL:urlToOpen];
}


@end
