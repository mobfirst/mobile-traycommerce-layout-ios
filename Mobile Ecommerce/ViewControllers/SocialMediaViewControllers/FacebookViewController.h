//
//  FacebookViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 25/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"

@interface FacebookViewController : CenterModelViewControllers
@property (strong, nonatomic) IBOutlet UIWebView *webview;

@end
