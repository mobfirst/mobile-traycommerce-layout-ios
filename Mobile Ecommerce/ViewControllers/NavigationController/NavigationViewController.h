//
//  MENavigationViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 08/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"

@interface NavigationViewController : UINavigationController

@property(nonatomic,strong) NSArray* categories;

-(void) onLeftMenuItemSelected:(MenuTag)tag;


-(id) returnViewControllerFromIdentifier:(NSString*) identifier;
-(NSString *) className: (Class) class;

@end
