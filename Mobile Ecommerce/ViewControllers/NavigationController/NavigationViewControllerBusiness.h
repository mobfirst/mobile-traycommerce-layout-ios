//
//  NavigationViewControllerBusiness.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 20/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationViewController.h"
#import "ProductGridViewController.h"
#import "ProductCategory.h"

@interface NavigationViewControllerBusiness : NavigationViewController
@property(strong,nonatomic) ProductCategory* currentCategory;

-(void) onSearchProduct:(NSString*)search;

@end
