//
//  NavigationViewControllerBusiness.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 20/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "NavigationViewControllerBusiness.h"
#import "LoginViewController.h"
#import "ServiceStorageUser.h"
#import "AccountOptionsViewController.h"
#import "StoreContactViewController.h"
#import "StorePlacesViewController.h"

@interface NavigationViewControllerBusiness (){
    MenuTag currentOption;
}
@end

@implementation NavigationViewControllerBusiness

-(void) onLeftMenuItemSelected:(MenuTag)tag{
    switch (tag) {
        case MENU_TAG_EXPLORE:
            [self openProductsViewController];
            break;
            
        case MENU_TAG_ACCOUNT:
            [self openAccountViewController];
            break;
            
         case MENU_TAG_STORES:
            [self openStorePlaces];
            break;
            
        case MENU_TAG_CONTACT:
            [self openStoreContacts];
            break;
            
        default:
            break;
    }
}

-(void) openStorePlaces{
    [self setViewControllers:@[[self returnViewControllerFromIdentifier: [self className:[StorePlacesViewController class]]]]] ;
}

-(void) openStoreContacts{
    [self setViewControllers:@[[self returnViewControllerFromIdentifier: [self className:[StoreContactViewController class]]]]] ;
}

-(void) onSearchProduct:(NSString*)search{
    
    ProductCategory* category = [ProductCategory new];
    category.id = CATEGORY_SEARCH;
    category.name = search;
    
    ProductGridViewController* productViewController = [self returnViewControllerFromIdentifier: [self className:[ProductGridViewController class]]];
     productViewController.category = category;
    [self setViewControllers:@[productViewController]] ;
}

-(void) openProductsViewController{
    
    if(!self.currentCategory){
        self.currentCategory = [self.categories objectAtIndex:0];
    }
    ProductGridViewController* productViewController = [self returnViewControllerFromIdentifier: [self className:[ProductGridViewController class]]];
    productViewController.category = self.currentCategory;
    [self setViewControllers:@[productViewController]] ;
}


-(void) openAccountViewController{
    if([ServiceStorageUser userIsLogged]){
        [self setViewControllers:@[[self returnViewControllerFromIdentifier: [self className:[AccountOptionsViewController class]]]]] ;
    }else{
        [self setViewControllers:@[[self returnViewControllerFromIdentifier: [self className:[LoginViewController class]]]]] ;
    }
}

@end
