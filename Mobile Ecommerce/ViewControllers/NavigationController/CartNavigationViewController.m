//
//  CartNavigationViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "CartNavigationViewController.h"
#import "CartViewController.h"
#import "NavigationViewController.h"

@interface CartNavigationViewController ()

@end

@implementation CartNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    NavigationViewController* navigationViewController = [NavigationViewController new];
    [self setViewControllers:@[[navigationViewController returnViewControllerFromIdentifier:
                            [navigationViewController className:[CartViewController class]]]]] ;
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
