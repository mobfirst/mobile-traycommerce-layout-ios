//
//  MENavigationViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 08/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "NavigationViewController.h"

#import "ExploreViewController.h"
#import "YouTubeViewController.h"
#import "InstagramViewController.h"
#import "FacebookViewController.h"
#import "TwitterViewController.h"
#import "LoginViewController.h"

#define STORYBOARD_NAME @"Main"

@interface NavigationViewController (){
    MenuTag currentOption;
}

@end

@implementation NavigationViewController

- (void)viewDidLoad {
   
    [super viewDidLoad];
    
    [self setupViewsControllers];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setupViewsControllers{
    
    currentOption = MENU_TAG_NOT_SELECTED;
    
    [self onLeftMenuItemSelected: MENU_TAG_EXPLORE];
}

-(void) onLeftMenuItemSelected:(MenuTag)tag{
    
    if(currentOption == tag){
        return;
    }
    
    currentOption = tag;
    
    switch (currentOption) {
            
        case MENU_TAG_EXPLORE:
            [self openExploreViewController];
            break;
            
        case MENU_TAG_INSTAGRAM:
            [self openInstagramViewController];
            break;
            
        case MENU_TAG_FACEBOOK:
            [self openFacebookViewController];
            break;
            
        case MENU_TAG_TWITTER:
            [self openTwitterViewController];
            break;
            
        case MENU_TAG_ACCOUNT:
            [self openAccountViewController];
            break;
            
        case MENU_TAG_VIDEOS:
            [self openVideosViewController];
            break;
            
        default:
            break;
    }
   
}

-(id) returnViewControllerFromIdentifier:(NSString*) identifier{
    UIStoryboard *storyboard  = [UIStoryboard storyboardWithName:STORYBOARD_NAME bundle:nil];
    return [storyboard instantiateViewControllerWithIdentifier:identifier];
}

-(NSString *) className: (Class) class{
    return [NSString stringWithFormat:@"%@",class];
}

-(void) openExploreViewController{
    [self setViewControllers:@[[self returnViewControllerFromIdentifier: [self className:[ExploreViewController class]]]]] ;
}

-(void) openVideosViewController{
    [self setViewControllers:@[[self returnViewControllerFromIdentifier: [self className:[YouTubeViewController class]]]]] ;
}

-(void) openInstagramViewController{
    [self setViewControllers:@[[self returnViewControllerFromIdentifier: [self className:[InstagramViewController class]]]]] ;
}

-(void) openFacebookViewController{
    [self setViewControllers:@[[self returnViewControllerFromIdentifier: [self className:[FacebookViewController class]]]]] ;
}

-(void) openTwitterViewController{
    [self setViewControllers:@[[self returnViewControllerFromIdentifier: [self className:[TwitterViewController class]]]]] ;
}

-(void) openAccountViewController{
    [self setViewControllers:@[[self returnViewControllerFromIdentifier: [self className:[LoginViewController class]]]]] ;
}

@end
