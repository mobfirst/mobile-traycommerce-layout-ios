//
//  StorePlacesViewController.m
//  Mia Inko
//
//  Created by Diego P Navarro on 28/02/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "StorePlacesViewController.h"
#import "StoreConfig.h"
#import "StorePlacesViewCell.h"
#import "Address.h"
#import "Dialog.h"
#import "Utils.h"
#import <MapKit/MapKit.h>

#define NUMBER_OF_SECTIONS 1
#define CELL_IDENTIFIER @"StorePlacesViewCell"

#define TITLE @"ENDEREÇOS"


@interface StorePlacesViewController ()

@end

@implementation StorePlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    [self setupNavigationBar:TITLE];
}

#pragma UITableView DataSource
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_OF_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[StoreConfig getStore].addresses count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Address * address = [[StoreConfig getStore].addresses objectAtIndex:indexPath.row];

    StorePlacesViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    cell.place.text = [Utils convertAddressesToString:address];
    cell.indexPath = indexPath;
    cell.block = ^(NSIndexPath* index){
          [self callMaps:[[StoreConfig getStore].addresses objectAtIndex:index.row]];
    };
    [cell setupCell];
    return cell;
}

-(void) callMaps:(Address*) address{
    
     NSString* addressString = [NSString stringWithFormat:@"%@,%@,%@,%@",address.addressLine1, address.neighborhood,address.city,address.state ];
     
     Class mapItemClass = [MKMapItem class];
     if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)]){
         CLGeocoder *geocoder = [[CLGeocoder alloc] init];
         [geocoder geocodeAddressString:addressString
                      completionHandler:^(NSArray *placemarks, NSError *error) {
     
                          if(error){
                              [Dialog showToast:@"Não foi possível calcular o trajeto para esse endereço" onView:self.view];
                              return ;
                          }
     
                          CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                          MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:geocodedPlacemark];
     
                          MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                          [mapItem setName:geocodedPlacemark.name];
     
                          NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
     
                          MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
     
                          [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem] launchOptions:launchOptions];
     
                      }];
     }
}


@end
