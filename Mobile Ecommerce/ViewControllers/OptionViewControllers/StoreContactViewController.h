//
//  StoreContactViewController.h
//  Mia Inko
//
//  Created by Diego P Navarro on 28/02/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"

@interface StoreContactViewController : CenterModelViewControllers<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
