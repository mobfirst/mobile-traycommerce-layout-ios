//
//  StoreContactViewController.m
//  Mia Inko
//
//  Created by Diego P Navarro on 28/02/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "StoreContactViewController.h"
#import "StoreConfig.h"
#import "StoreContactViewCell.h"
#import "Email.h"
#import "Phones.h"
#import "Utils.h"
#import <MessageUI/MessageUI.h>

#define TITLE @"CONTATO"
#define NUMBER_OF_SECTIONS 1

#define IMAGE_EMAIL @"ic_action_email_gray"
#define IMAGE_PHONE @"ic_action_phone_gray"

#define CELL_IDENTIFIER @"StoreContactViewCell"


@interface StoreContactViewController ()<MFMailComposeViewControllerDelegate>{
    NSMutableArray* options;
}
@end

@implementation StoreContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    [self setupNavigationBar:TITLE];
    [self insertOptions];
}

-(void) insertOptions{
    
    options = [NSMutableArray new];
    
    if([StoreConfig getStore].emails){
        [options addObjectsFromArray:[StoreConfig getStore].emails];
    }
    
    if([StoreConfig getStore].phones){
        [options addObjectsFromArray:[StoreConfig getStore].phones];
    }
}


#pragma UITableView DataSource
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_OF_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [options count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSObject* object = [options objectAtIndex:indexPath.row];
    
    StoreContactViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    
    NSString* image = nil;
    NSString* text = nil;
    
    if([object isKindOfClass:[Email class]]){
        image = IMAGE_EMAIL;
        Email * email = (Email *)object;
        text = [NSString stringWithFormat:@"%@\n%@",email.title,email.email];
        
    }else{
        image = IMAGE_PHONE;
        text = [Utils convertPhoneToStringWithTitle:object];
    }
    
    cell.icon.image = [UIImage imageNamed:image];
    cell.title.text = text;
    
    cell.indexPath = indexPath;
    cell.block = ^(NSIndexPath* index){
        [self onOptionSelected:[options objectAtIndex:index.row]];
    };
    [cell setupCell];
    
    return cell;
}


-(void) onOptionSelected:(NSObject*) object{
    if([object isKindOfClass:[Email class]]){
        [self sendEmail:(Email*) object];
    }else{
        [self callToStore:object];
    }
}

-(void) sendEmail:(Email*) email{
    MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
    mailCont.mailComposeDelegate = self;
    [mailCont setToRecipients:@[email.email]];
    [self.navigationController presentViewController:mailCont animated:YES completion:nil];
}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

-(void) callToStore:(NSObject*) object{
    Phones* phoneNumber = (Phones*) object;
    NSString* phone = nil;
    if(phoneNumber.areaCode){
        phone = [NSString stringWithFormat:@"telprompt://+%@%@%@",phoneNumber.countryCode,phoneNumber.areaCode,phoneNumber.number];
    }
    else{
         phone = [NSString stringWithFormat:@"telprompt://%@",phoneNumber.number];
    }
    NSURL *url = [NSURL URLWithString:phone];
    [[UIApplication sharedApplication] openURL:url];
}

@end
