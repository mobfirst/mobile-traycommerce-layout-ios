//
//  CartViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckoutModelViewController.h"

@interface CartViewController : CheckoutModelViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UILabel *noProducts;
@property (strong, nonatomic) IBOutlet UIButton *buttonConfirm;
- (IBAction)onButtonConfirmClick:(id)sender;

@end
