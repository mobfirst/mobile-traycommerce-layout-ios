//
//  CategoryViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 26/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"
#import "ProductCategory.h"
#import "NavigationViewController.h"

@interface CategoryViewController : CenterModelViewControllers <UITableViewDataSource,UITableViewDelegate>

@property(strong,nonatomic) NavigationViewController* navigationViewController;
@property(strong,nonatomic) ProductCategory* category;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
