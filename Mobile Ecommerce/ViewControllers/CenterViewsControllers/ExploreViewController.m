//
//  ExploreViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 24/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "ExploreViewController.h"
#import "ProductCategoryBannerCell.h"
#import "ProductCategoryCell.h"
#import "CategoryViewController.h"
#import "NavigationViewController.h"
#import "ProductGridViewController.h"

#define NUMBER_SECTIONS 1
#define CELL_BANNER_IDENTIFIER @"ProductCategoryBannerCell"
#define CELL_CATEGORY_IDENTIFIER @"ProductCategoryCell"


@interface ExploreViewController ()

@end

@implementation ExploreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBar: NSLocalizedString(@"EXPLORE_VIEW_CONTROLLER_TITLE",nil)];
    [self setupView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void) setupView{
    [self.tableView setHidden:YES];
    self.tableView.backgroundColor = [StoreConfig getStoreColor];
    [self performSelector:@selector(startShowTableView) withObject:nil afterDelay:3];
}

-(void) startShowTableView{
    [self.activityIndicator stopAnimating];
    [self.activityIndicator setUserInteractionEnabled:NO];
    [self.tableView setHidden:NO];
}

-(BOOL) storeHasBanner{
    return [StoreConfig getStore].banners && [[StoreConfig getStore].banners count] > 0;
}

-(NSArray*) getCategories{
    NavigationViewController * navigationController = (NavigationViewController*)self .navigationController;
    return navigationController.categories;
}


#pragma UITableView Data Source
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self storeHasBanner] ? [[self getCategories] count] + 1 : [[self getCategories] count];
}


-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return indexPath.row == 0 && [self storeHasBanner] ? [self getBannerCell] :   [self storeHasBanner] ? [self getCategoryCellOnRow:indexPath.row - 1 ] : [self getCategoryCellOnRow:indexPath.row];
}

-(ProductCategoryCell*) getCategoryCellOnRow:(NSInteger) row{
  
    ProductCategoryCell * cell = [self.tableView dequeueReusableCellWithIdentifier:CELL_CATEGORY_IDENTIFIER];
    cell.category = [[self getCategories] objectAtIndex:row];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    [cell setupCellWithSelectedBlock: ^(ProductCategoryCell* cell){
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        ProductCategory* category = [[self getCategories] objectAtIndex: [self storeHasBanner] ?  indexPath.row - 1 : indexPath.row ];
        category.childrens == nil ? [self callProductGrid:category] : [self callCategoryView:category];
    }];
    
    return cell;
}


-(ProductCategoryBannerCell *) getBannerCell{
    ProductCategoryBannerCell * cell = [self.tableView dequeueReusableCellWithIdentifier:CELL_BANNER_IDENTIFIER];
    
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    [cell setupCell];
    
    return cell;
}


-(void) callProductGrid:(ProductCategory*) category{
    
    NavigationViewController* navigationController = (NavigationViewController* )self.navigationController;
    ProductGridViewController * productGridViewController = [navigationController returnViewControllerFromIdentifier: [navigationController className:[ProductGridViewController class]]];
    productGridViewController.category = category;
    [navigationController pushViewController:productGridViewController animated:YES];

}

-(void) callCategoryView:(ProductCategory*) category{
    
    NavigationViewController* navigationController = (NavigationViewController* )self.navigationController;
    CategoryViewController * categoryViewController = [navigationController returnViewControllerFromIdentifier: [navigationController className:[CategoryViewController class]]];
    categoryViewController.navigationViewController = navigationController;
    categoryViewController.category = category;
    categoryViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [navigationController presentViewController:categoryViewController animated:YES completion:nil];
}

@end
