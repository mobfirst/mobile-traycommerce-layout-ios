//
//  CartViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "CartViewController.h"
#import "CartViewCell.h"
#import "CartTotalCell.h"
#import "ServiceApiMapper.h"
#import "Cart.h"
#import "Dialog.h"
#import "ProductCart.h"
#import "ProductCartToUpdate.h"
#import "AddressChoiceViewController.h"
#import "NavigationViewController.h"
#import <EventTracker/GlobalEventTracker.h>

#define NUMBER_SECTIONS 1
#define CELL_CART_IDENTIFIER @"CartViewCell"
#define CELL_CART_TOTAL_IDENTIFIER @"CartTotalCell"

#define CELL_CART_HEIGHT 330
#define CELL_CART_TOTAL_HEIGHT 50

#define ALERT_DIALOG_CONFIRM 0


@interface CartViewController ()<UIAlertViewDelegate>{
    Cart * cart;
    ProductCart* productToDelete;
    NSMutableArray* itemsToChange;
}

@end

@implementation CartViewController

void (^blockDelete) (ProductCart *);
void (^blockUpdate) (ProductCart *, int quantity);

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    [self setupNavigationBar:@"MEU CARRINHO"];
    self.buttonConfirm.backgroundColor = [StoreConfig getStoreColor];
    
    [self setEnable:NO];
    [self setBlocks];
    [self getCart];
}

-(void) setBlocks{
    blockDelete = ^(ProductCart *productCart) {
        [self deleteProduct:productCart];
    };

    blockUpdate = ^ (ProductCart * productCart, int quantity){
        [self updateProduct:productCart withQuantity:quantity];
    };
}

-(void) setEnable:(BOOL) enable{
    [self.buttonConfirm setHidden:!enable];
    enable ? [self.activityIndicator stopAnimating] : [self.activityIndicator startAnimating];
    [self.tableView setHidden:!enable];
}

-(void) getCart{
    [[ServiceApiMapper Api] getCartWithBlock:^(NSObject * object, NSError * error) {
        
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getCart) withObject:nil afterDelay:8];
        }
        else{
            cart = (Cart*) object;
            [self showCart];
        }
    }];
}

-(void) showCart{

    [self.tableView reloadData];
    [self.buttonConfirm setTitle:@"FAZER CHECKOUT" forState:UIControlStateNormal];
    [self setEnable:YES];
    
    [self.buttonConfirm setHidden:(!cart.products || [cart.products count] ==0)];
    [self.noProducts setHidden:!(!cart.products || [cart.products count] ==0)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma UITableView datasource 

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return cart && cart.products.count > 0 ? [cart.products count] + 1 : 0;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath.row == [cart.products count] ? [self getCartTotal]:[self getCartCellOnRow:indexPath];
}

-(CartViewCell*) getCartCellOnRow:(NSIndexPath*) indexPath{
    float subTotal = 0;
    for(int count  = 0 ; count <= indexPath.row ; count ++ ){
        ProductCart* productCart = [cart.products objectAtIndex:count];
        subTotal = [[productCart.price  stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue] * productCart.quantity + subTotal;
    }
    ProductCart* productCart = [cart.products objectAtIndex:indexPath.row];
    CartViewCell * cell = [self.tableView dequeueReusableCellWithIdentifier: CELL_CART_IDENTIFIER forIndexPath:indexPath];
    
    
    cell.blockUpdate = blockUpdate;
    cell.blockDelete = blockDelete;
    cell.productCart = productCart;
    [cell setupView:subTotal];
    return cell;
}

-(CartTotalCell*) getCartTotal{
    CartTotalCell * cell = [self.tableView dequeueReusableCellWithIdentifier: CELL_CART_TOTAL_IDENTIFIER];
    cell.total = cart.value;
    [cell setupView];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath.row == [cart.products count] ? CELL_CART_TOTAL_HEIGHT :CELL_CART_HEIGHT;
}


-(void) deleteProduct:(ProductCart*) product{
    
    NSString* messageDelete = [NSString stringWithFormat:@"Deseja realmente excluir o produto %@",product.name];
    UIAlertView* confirmation = [[UIAlertView alloc] initWithTitle:@"" message:messageDelete delegate:self cancelButtonTitle:@"Sim" otherButtonTitles:@"Não", nil];
    [confirmation show];
    productToDelete = product;
}

-(void) updateProduct:(ProductCart*) product withQuantity:(int) quantity{

    if(!itemsToChange){
        itemsToChange = [NSMutableArray new];
    }
    
    ProductCartToUpdate * productToUpdate = [ProductCartToUpdate new];
    
    if(product.option){
        Variation* variation = [product.option.childrens objectAtIndex:0];
        productToUpdate.id = variation.id;
    }
    else{
        productToUpdate.id = product.id;
    }
    
    productToUpdate.product = product;
    productToUpdate.newQuantity = quantity;
    
    if(product.quantity == quantity){
        int index = 0;
        for(int count = 0 ; count < [itemsToChange count] ; count ++){
            
            ProductCartToUpdate* productUpdated = [itemsToChange objectAtIndex:count];
            
            if([productUpdated.id isEqualToString:productToUpdate.id]){
                index = count;
                break;
            }
        }
        [itemsToChange removeObjectAtIndex:index];
        
    }else{
        
        BOOL found = NO;
        
        for(ProductCartToUpdate * productUpdated in itemsToChange){
            if([productUpdated.id isEqualToString:productToUpdate.id]){
                productUpdated.newQuantity = quantity;
                found = YES;
                break;
            }
        }
        if(!found){
            [itemsToChange addObject:productToUpdate];
        }
    }
    
    if([itemsToChange count] == 0){
        [self.buttonConfirm setTitle:@"FAZER CHECKOUT" forState:UIControlStateNormal];
    }
    else{
        [self.buttonConfirm setTitle:@"ATUALIZAR CARRINHO" forState:UIControlStateNormal];
    }
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex == ALERT_DIALOG_CONFIRM){
        [self setEnable:NO];
        [[ServiceApiMapper Api] deleteProductOnCart:productToDelete withBlock:^(BOOL success, NSError * error) {
            if(error){
                [Dialog showConnectionErrorOnView:self.view];
                [self setEnable:NO];
            }
            else{
                cart = nil;
                [self getCart];
            }
        }];
    }
    else{
        productToDelete = nil;
    }
}


- (IBAction)onButtonConfirmClick:(id)sender {
    
    if(!itemsToChange || [itemsToChange count] == 0){
        [self confirmCheckOut];
    }else{
        [self updateProducts];
    }
}
-(void) updateProducts{
    
    [self setEnable:NO];
    [[ServiceApiMapper Api] updateProductsOnCart:itemsToChange withBlock:^(BOOL success, NSError *error) {
        
        if(error){
            if(success){
                [Dialog showDialog:error.localizedDescription];
            }else{
                [Dialog showConnectionErrorOnView:self.view];
            }
            [self setEnable:YES];
        }
        else{
            itemsToChange = nil;
            cart = nil;
            [self getCart];
        }
    }];
}

-(void) confirmCheckOut{
    [self setEnable:NO];
    
    [[ServiceApiMapper Api] confirmCheckoutWithCart:cart withBlock:^(NSObject * object, NSError * error) {
        
        [self setEnable:YES];
        
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            
        }
        else{
            NavigationViewController* navigationViewController = [NavigationViewController new];
            [self.navigationController pushViewController:
            [navigationViewController returnViewControllerFromIdentifier:
            [navigationViewController className:[AddressChoiceViewController class]]] animated:YES];
            [GlobalEventTracker initiatedCheckout];  
        }
    }];
}

@end
