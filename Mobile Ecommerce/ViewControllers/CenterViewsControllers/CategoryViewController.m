//
//  CategoryViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 26/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "CategoryViewController.h"
#import "ProductCategoryCell.h"
#import "ProductCategorySubCell.h"
#import "NavigationViewController.h"
#import "ProductGridViewController.h"

#define NUMBER_SECTIONS 1
#define CELL_CATEGORY_IDENTIFIER @"ProductCategoryCell"
#define CELL_SUB_CATEGORY_IDENTIFIER @"ProductCategorySubCell"


#define CELL_CATEGORY_HEIGHT 180
#define CELL_SUB_CATEGORY_HEIGHT 90

#define ANIMATION_SLEEP 0.5f


@interface CategoryViewController ()
{
    NSInteger indexToInsert;
    BOOL isToRemove;
    NSMutableArray * categoriesCell;
    CategoryClickControl* categoryControlToUpdate;
    
}

@end

@implementation CategoryViewController


- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    categoriesCell = [NSMutableArray new];

    [self setupCategoriesList];
    
    [self.tableView setHidden:YES];

    [self performSelector:@selector(startShowTableView) withObject:nil afterDelay:3];
}

-(void) setupCategoriesList{
    for(int count = 0 ; count < [self.category.childrens count]; count++){
        CategoryClickControl * categoryControl = [CategoryClickControl new];
        categoryControl.isParent = NO;
        categoryControl.category = [self.category.childrens objectAtIndex:count];
        [categoriesCell addObject:categoryControl];
    }
}

-(void) startShowTableView{
    [self.activityIndicator stopAnimating];
    [self.activityIndicator setUserInteractionEnabled:NO];
    [self.tableView setHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma UITableView Data Source
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [categoriesCell count] + 1;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  indexPath.row == 0 ? [self getCategoryCellOnRow] : [self getCategorySubCell:indexPath.row - 1];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath.row == 0 ? CELL_CATEGORY_HEIGHT : CELL_SUB_CATEGORY_HEIGHT;
}

-(ProductCategoryCell*) getCategoryCellOnRow{
    
    ProductCategoryCell * cell = [self.tableView dequeueReusableCellWithIdentifier:CELL_CATEGORY_IDENTIFIER];
    cell.category = self.category;
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    
    [cell setupCellWithSelectedBlock: ^(ProductCategoryCell* cell){
        [self dismissView];
    }];
    
    return cell;
}

-(ProductCategorySubCell*) getCategorySubCell:(NSInteger) row{
    
    ProductCategorySubCell * cell = [self.tableView dequeueReusableCellWithIdentifier:CELL_SUB_CATEGORY_IDENTIFIER];
    CategoryClickControl* categoryControl = categoriesCell[row];
    cell.category = categoryControl.category;

    if(categoryControl.isParent){
        cell.label.textColor = [StoreConfig getStoreColor];
    }
    else{
        cell.label.textColor = [UIColor lightGrayColor];
    }
    
    [cell setupView];
    
    return cell;
    
}

#pragma UITableView Delegate
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CategoryClickControl * categoryControl = [categoriesCell objectAtIndex:indexPath.row -1];
    
    if(categoryControl.category.childrens &&  [categoryControl.category.childrens count] > 0){
        [self removeCategoryChildren:indexPath];
    }
    else{
        [self callProductGrid:categoryControl.category];
    }
}


-(int) getFirstIndext{
    
    int listSize = (int)([categoriesCell count] - 1);
    
    int position = listSize;
    
    for(int count = listSize ; count >= 0 ; count --){
        CategoryClickControl* categoryControl = categoriesCell[count];
        if(categoryControl.isParent){
            return position;
        }
        position = count;
    }
    return position;
}



-(void) removeCategoryChildren:(NSIndexPath*) indexPath{
    
    int correctIndex = (int)(indexPath.row - 1);
    
    indexToInsert = [self getFirstIndext];
    isToRemove = YES;
    
    if(correctIndex < indexToInsert){
        indexToInsert = correctIndex;
        isToRemove = NO;
    }
    else{
        CategoryClickControl* categoryControl = categoriesCell[correctIndex];
        categoryControl.isParent = YES;
    }
    
    categoryControlToUpdate = categoriesCell[correctIndex];
    
    if(isToRemove){
        
        [categoriesCell removeObjectAtIndex:correctIndex];
        [categoriesCell insertObject:categoryControlToUpdate atIndex:indexToInsert];
      
        
        
        NSArray *deleteIndexPaths = [NSArray arrayWithObjects:
                                    [NSIndexPath indexPathForRow:correctIndex + 1 inSection:0],
                                     nil];
        
        NSArray *insertIndexPaths = [NSArray arrayWithObjects:
                                     [NSIndexPath indexPathForRow:indexToInsert + 1 inSection:0],
                                     nil];
        
        
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:insertIndexPaths withRowAnimation:UITableViewRowAnimationRight];
        [self.tableView deleteRowsAtIndexPaths:deleteIndexPaths withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
      
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, ANIMATION_SLEEP * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self removeCellsFromTableFromIndex:indexToInsert];
        });
    }
    else{
        [self removeCellsFromTableFromIndex:indexToInsert];
    }
}

-(void) removeCellsFromTableFromIndex:(NSInteger) index{
    
    int listSize = (int)([categoriesCell count] - 1);
    
    NSMutableArray * itensToRemove = [NSMutableArray new];
    
    for(int count = listSize ; count > index ; count --){
        [categoriesCell removeObjectAtIndex:count];
        [itensToRemove addObject:[NSIndexPath indexPathForRow:count + 1 inSection:0]];
    }
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:itensToRemove withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, ANIMATION_SLEEP * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
         [self insertCellFromTable];
    });
}

-(void) insertCellFromTable{
    
    for(int count = 0 ; count < [categoryControlToUpdate.category.childrens count]; count++){
        CategoryClickControl * categoryControl = [CategoryClickControl new];
        categoryControl.isParent = NO;
        categoryControl.category = [categoryControlToUpdate.category.childrens objectAtIndex:count];
        [categoriesCell addObject:categoryControl];
    }
    
    [self.tableView reloadData];
}

-(void) callProductGrid:(ProductCategory*) category{
    
    NavigationViewController* navigationController = self.navigationViewController;
    ProductGridViewController * productGridViewController = [navigationController returnViewControllerFromIdentifier: [navigationController className:[ProductGridViewController class]]];
    productGridViewController.category = category;
    [navigationController pushViewController:productGridViewController animated:YES];
    [self dismissView];
}


#pragma Dismiss modal
-(void) dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];

}

@end
