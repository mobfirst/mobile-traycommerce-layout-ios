//
//  ProductDescriptionViewController.h
//  Multivisi
//
//  Created by Diego P Navarro on 06/04/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"
#import <PlataformModels/Product.h>


@interface ProductDescriptionViewController : CenterModelViewControllers

@property (weak, nonatomic) Product *product;
@property (weak, nonatomic) IBOutlet UIWebView *vwebView;

@end
