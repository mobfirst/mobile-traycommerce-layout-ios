//
//  ProductChoiceModelViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "ProductChoiceModelViewController.h"
#import "ProductModels.h"
#import "ProductModelsTitleCell.h"
#import "ProductCart.h"
#import "Dialog.h"
#import "LoginViewController.h"
#import "NavigationViewController.h"
#import <EventTracker/GlobalEventTracker.h>
#import <APIConnection/Constants.h>

#define MARGEN_LEFT 10

#define CELL_TITLE_IDENTIFIER @"ProductModelsTitleCell"

@interface ProductChoiceModelViewController (){
    NSMutableArray* listItens;
}

@end

@implementation ProductChoiceModelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBarWithBack:@"MODELOS DISPONÍVEIS"];
    self.outletButton.backgroundColor = [StoreConfig getStoreColor];
    [self loadListItem];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void) loadListItem{
    
    listItens = [NSMutableArray new];

    ProductModels * productModels = [ProductModels new];
    productModels.title = self.product.option.name;
    productModels.itens = [NSMutableArray new];
    
    for(Variation* variantion in self.product.option.childrens){
        ProductModels * productModelsChild = [ProductModels new];

        if(variantion.price || variantion.specialPrice){
            NSString* message;
            
            if(variantion.specialPrice){
                message = [NSString stringWithFormat:@"%@ R$ %@",variantion.value,variantion.specialPrice];
            }else{
                message = [NSString stringWithFormat:@"%@ R$ %@",variantion.value,variantion.price];
            }
            
            productModelsChild.title = message;
        }
        else{
            productModelsChild.title = variantion.value;
        }
        productModelsChild.variation = variantion;
        [productModels.itens addObject:productModelsChild];
    }
    [listItens addObject:productModels];
    
    [self setEnable:YES];

}


#pragma UITableView datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    ProductModels * productModels = [listItens objectAtIndex:section];
    return [productModels.itens count ];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [listItens count];;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    ProductModels * productModels = [listItens objectAtIndex:section];

    CGRect frame;
    frame.origin.x = 0;
    frame.origin.y = MARGEN_LEFT;

    UILabel *label = [[UILabel alloc] initWithFrame:frame];

    label.textColor = [UIColor lightGrayColor];
    label.backgroundColor = [UIColor clearColor];
    label.text = productModels.title;
    return label;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ProductModelsTitleCell * cell = [tableView dequeueReusableCellWithIdentifier:CELL_TITLE_IDENTIFIER forIndexPath:indexPath];
    
    ProductModels * productModels = [listItens objectAtIndex:indexPath.section];
    
    ProductModels * productModelsChild = [productModels.itens objectAtIndex:indexPath.row];
    cell.productModels = productModelsChild;
    cell.title.text = productModelsChild.title;
    return cell;
    
}


- (IBAction)onConfirmClick:(id)sender {
    
    [self setEnable:NO];
    
    NSMutableArray * products = [NSMutableArray new];
    
    ProductModels * productModel = [listItens objectAtIndex:0];
    
    for(ProductModels * item in productModel.itens){
        
        if(item.quantity > 0){
            
            ProductCart* productCart = [ProductCart new];
            
            productCart.id = self.product.id;
            productCart.name = self.product.name;

            productCart.images = self.product.images;
            productCart.quantity = item.quantity;
            
            Option* option = [Option new];
            option.id = self.product.option.id;
            option.name = self.product.option.name;
            
            NSMutableArray* variants =  [NSMutableArray new];
            [variants addObject:item.variation];
            option.childrens = variants;
    
            if(item.variation.price || item.variation.specialPrice){
                productCart.price = item.variation.specialPrice ? item.variation.specialPrice : item.variation.price;
            }else{
                productCart.price = self.product.specialPrice ? self.product.specialPrice : self.product.price;
            }
            
            productCart.option = option;
            
            [products addObject:productCart];
        }
    }
    
    if([products count] == 0 ){
        [Dialog showToast:@"Favor inserir uma quantidade" onView:self.view];
        return;
    }

    [GlobalEventTracker productAddToCart:self.product];
    
    if(![ServiceStorageUser userIsLogged]){
        NavigationViewController* navigationController = (NavigationViewController* )self.navigationController;
        LoginViewController * loginViewController = [navigationController returnViewControllerFromIdentifier: [navigationController className:[LoginViewController class]]];
        loginViewController.products = products;
        [navigationController pushViewController:loginViewController animated:YES];
        
    }else{
    
        [[ServiceApiMapper Api] insertProductsOnCart:products withBlock:^(BOOL success, NSError * error) {
        
            [self setEnable:YES];
            
            if(error){
                if(error.code == ERROR_CONVERTION_CODE){
                    [Dialog showDialog:error.localizedDescription];
                }
                else{
                    [Dialog showConnectionErrorOnView:self.view];
                }
            }else{
                if(success){
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    [Dialog showSuccessAddCart];
                }else{
                    [Dialog showToast:@"Problemas ao adicionar ao carrinho" onView:self.view];
                }
            }
        }];
    }
}

-(void) setEnable:(BOOL) enable{
    if(!enable){
        [self.activityIndicatorButton startAnimating];
    }
    else{
        [self.activityIndicatorButton stopAnimating];
    }
    self.outletButton.enabled = enable;
}

@end
