//
//  ProductDescriptionViewController.m
//  Multivisi
//
//  Created by Diego P Navarro on 06/04/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "ProductDescriptionViewController.h"

@interface ProductDescriptionViewController ()

@end

@implementation ProductDescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigationBarWithBack:self.product.name];
    [self setupWebView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void) setupWebView{
    
    NSString* header = @"<!DOCTYPE html><html><head><meta name = \"viewport\" content = \"width = device-width\"></head>";
    
    if([self.product.productDescription rangeOfString:@"<html>" options:NSCaseInsensitiveSearch].location != NSNotFound){
        self.product.productDescription = [self.product.productDescription stringByReplacingOccurrencesOfString:@"<html>" withString:@""];
    }
    
    self.product.productDescription = [NSString stringWithFormat:@"%@%@",header,self.product.productDescription];
    
    if([self.product.productDescription rangeOfString:@"</html>" options:NSCaseInsensitiveSearch].location == NSNotFound){
        self.product.productDescription = [NSString stringWithFormat:@"%@%@",self.product.productDescription,@"</html>"];
    }
    
    [self.vwebView loadHTMLString:[self.product.productDescription stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"] baseURL:nil];
}

@end
