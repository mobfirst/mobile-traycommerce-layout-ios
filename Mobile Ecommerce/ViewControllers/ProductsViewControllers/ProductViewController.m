//
//  ProductViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "ProductViewController.h"
#import "NavigationViewController.h"
#import "ProductChoiceModelViewController.h"
#import "ServiceApiMapper.h"
#import "Dialog.h"
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import "LoginViewController.h"
#import "NSString+HTML.h"
#import <APIConnection/Constants.h>
#import <EventTracker/GlobalEventTracker.h>
#import "HelperTextFormatter.h"
#import "ProductDescriptionViewController.h"


#define IMAGE_SIZE_HEIGHT 1.5f

@implementation ProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setupView];
    
    [self setHidden:YES];
}


-(void) setupView{
    
    [self setupNavigationBarWithBack: [self.product.name uppercaseString]];
   
    [self adjustScreenWidth];
    
    self.viewChoiceModels.backgroundColor = [StoreConfig getStoreColor];
    
    [self getProductInformation];
}


-(void) getProductInformation{
    
    [[ServiceApiMapper Api] getProductById:self.product.id withBlock:^(NSObject *productFull, NSError *error) {
        
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getProductInformation) withObject:nil afterDelay:8];
            
        }
        else{
            self.product = (Product*)productFull;
            [self.activityIndicator stopAnimating];
            [self insertProductsImage];
            [self setProductInformation];
            [self setHidden:NO];
            
        }
    }];
}


-(void) adjustScreenWidth{
    self.ConstraintViewWidth.constant = [UIScreen mainScreen].bounds.size.width;
}

-(void) insertProductsImage{

    int numberOfImages = 0;
    
    if(self.product.images && [self.product.images count] > 0){
        
        numberOfImages = (int) [self.product.images count];
        
        for(int count = 0;count < numberOfImages ;count ++)
        {
            CGRect frame;
            frame.origin.x = 0;
            frame.origin.y = [UIScreen mainScreen].bounds.size.width * IMAGE_SIZE_HEIGHT * count;
            frame.size.height = [UIScreen mainScreen].bounds.size.width * IMAGE_SIZE_HEIGHT;
            frame.size.width = [UIScreen mainScreen].bounds.size.width;
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
            
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            
            [imageView sd_setImageWithURL:[NSURL URLWithString:[self.product.images objectAtIndex:count]]];
            [self.viewImages addSubview:imageView];
        }
    }
    else{
    
        numberOfImages = 1;
        
        CGRect frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        frame.size.height = [UIScreen mainScreen].bounds.size.width * IMAGE_SIZE_HEIGHT;
        frame.size.width = [UIScreen mainScreen].bounds.size.width;
            
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
        
        imageView.contentMode = UIViewContentModeScaleAspectFit;

        UIImage *image = [UIImage imageNamed:@"no_image.jpg"];
            
        [imageView setImage:image];
        [self.viewImages addSubview:imageView];
        
    }
    self.ConstraintViewHeight.constant = [UIScreen mainScreen].bounds.size.width * IMAGE_SIZE_HEIGHT * numberOfImages;
}


-(void) setProductInformation{
    
    [GlobalEventTracker productView:self.product];
    
    self.productTitle.text =self.product.name;
    
    NSString* oldPrice = @"";
    NSString* price = @"";
    
    
    if(self.product.specialPrice){
        oldPrice = [self.product.price stringByReplacingOccurrencesOfString:@"." withString:@","];
        price = [self.product.specialPrice stringByReplacingOccurrencesOfString:@"." withString:@","];
    }else{
        price = [self.product.price stringByReplacingOccurrencesOfString:@"." withString:@","];
    }
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:oldPrice];
    [attributeString addAttribute:NSStrikethroughStyleAttributeName
                            value:@1
                            range:NSMakeRange(0, [attributeString length])];
    self.productOldPrice.attributedText = attributeString;
    self.productPrice.text = [NSString stringWithFormat:@"R$ %@",price];
    self.productPrice.textColor=[StoreConfig getStoreColor];
    
    if(!self.product.option){
        self.viewChoiceModelTitle.text = @"ADICIONAR AO CARRINHO";
    }
}


-(void) setHidden:(BOOL) hidden{

    [self.productTitle setHidden:hidden];
    [self.productOldPrice setHidden:hidden];
    [self.productPrice setHidden:hidden];
    [self.viewProductDescription setHidden:hidden];
    [self.viewChoiceModels setHidden:hidden];
    [self.viewChoiceModelTitle setHidden:hidden];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)onAddOrChoiceModelTap:(UITapGestureRecognizer *)recognizer {
    
    if(!self.product.option){
        [self addProdductToCart];
    }else{
        NavigationViewController* navigationController = (NavigationViewController* )self.navigationController;
        ProductChoiceModelViewController * productChoiceModelViewController = [navigationController returnViewControllerFromIdentifier: [navigationController className:[ProductChoiceModelViewController class]]];
        productChoiceModelViewController.product = self.product;
        [navigationController pushViewController:productChoiceModelViewController animated:YES];
    }
}

- (void)onDescriptionTap:(UITapGestureRecognizer *)recognizer {

    NavigationViewController* navigationController = (NavigationViewController* )self.navigationController;
    ProductDescriptionViewController * productDescriptionViewController = [navigationController returnViewControllerFromIdentifier: [navigationController className:[ProductDescriptionViewController class]]];
    productDescriptionViewController.product = self.product;
    [navigationController pushViewController:productDescriptionViewController animated:YES];
    
}

-(void) addProdductToCart{
    
    [GlobalEventTracker productAddToCart:self.product];
    
    NSMutableArray * products = [NSMutableArray new];
    
    ProductCart* productCart = [ProductCart new];
    
    productCart.id = self.product.id;
    productCart.name = self.product.name;
    productCart.price = self.product.specialPrice ? self.product.specialPrice : self.product.price;
    productCart.images = self.product.images;
    productCart.quantity = 1;
    
    [products addObject:productCart];

    
     if(![ServiceStorageUser userIsLogged]){
         NavigationViewController* navigationController = (NavigationViewController* )self.navigationController;
         LoginViewController * loginViewController = [navigationController returnViewControllerFromIdentifier: [navigationController className:[LoginViewController class]]];
         loginViewController.products = products;
         [navigationController pushViewController:loginViewController animated:YES];

     }else{
         [[ServiceApiMapper Api] insertProductsOnCart:products withBlock:^(BOOL success, NSError * error) {
        
            if(error){
                if(error.code == ERROR_CONVERTION_CODE){
                   [Dialog showDialog:error.localizedDescription];
                }
                else{
                    [Dialog showConnectionErrorOnView:self.view];
                }
            }else{
                if(success){
                    [self.navigationController popToRootViewControllerAnimated:YES];
                    [Dialog showSuccessAddCart];
                }else{
                    [Dialog showToast:@"Problemas ao adicionar ao carrinho" onView:self.view];
                }
            }
        }];
     }
}

@end
