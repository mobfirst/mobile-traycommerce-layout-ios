//
//  ProductGridViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"
#import "ProductCategory.h"


@interface ProductGridViewController : CenterModelViewControllers
<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property(strong,nonatomic) ProductCategory* category;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UILabel *noProducts;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
