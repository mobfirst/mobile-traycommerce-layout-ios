//
//  ProductViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"
#import "Product.h"

@interface ProductViewController : CenterModelViewControllers

@property (strong, nonatomic) Product* product;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint * ConstraintViewHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *ConstraintViewWidth;
@property (strong, nonatomic) IBOutlet UIView *viewImages;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) IBOutlet UILabel *productTitle;
@property (strong, nonatomic) IBOutlet UILabel *productOldPrice;
@property (strong, nonatomic) IBOutlet UILabel *productPrice;
@property (strong, nonatomic) IBOutlet UIView *viewProductDescription;
@property (strong, nonatomic) IBOutlet UIView *viewChoiceModels;
@property (strong, nonatomic) IBOutlet UILabel *viewChoiceModelTitle;
- (IBAction)onDescriptionTap:(id)sender;
- (IBAction)onAddOrChoiceModelTap:(id)sender;
@end
