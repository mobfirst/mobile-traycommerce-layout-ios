//
//  ProductGridViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "ProductGridViewController.h"
#import "NavigationViewController.h"
#import "ProductViewController.h"
#import <TheSidebarController/TheSidebarController.h>
#import "ProductsCell.h"
#import "ServiceApiMapper.h"
#import "Product.h"
#import "Dialog.h"
#import "BannesView.h"

#define CELL_IDENTIFIER @"ProductsCell"
#define CELL_WIDTH 145
#define CELL_HEIGHT 230
#define PRODUCTS_QUANTITY_REQUEST 10
#define NUMBER_SECTIONS 1

#define HEADER_SIZE 180.0f
#define HEADER_IDENTIFIER @"BannesView"

@interface ProductGridViewController (){
    CGFloat marginLeft;
    NSMutableArray * products;
    int currentPage;
    BOOL canLoadScroll;
}
@end

@implementation ProductGridViewController

void (^block) (NSArray *, NSError *);

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    
    currentPage = 1;
    
    switch ([StoreConfig getStore].model) {
        case BUSINESS:
            [self setupNavigationBar:[self.category.name uppercaseString]];
            break;
            
        case ENTERPRISE:
            [self setupNavigationBarWithBack: [self.category.name uppercaseString]];
        default:
            break;
    }

    [self getPaddingToCell];
    
    if(!products){
        products = [NSMutableArray new];
    }
    
    block = ^(NSArray *prodList, NSError * error) {
        
        canLoadScroll = YES;
        
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getCategoryProducts) withObject:nil afterDelay:8];
        }
        else{
            [self startShowGrid];
            [products addObjectsFromArray:prodList];
            if([products count] == 0){
                [self.noProducts setHidden:NO];
                
            }else{
                [self.collectionView reloadData];
            }
        }
    };
    
    [self getCategoryProducts];

}

-(void) getCategoryProducts{

    if([self.category.id isEqualToString:CATEGORY_DEFAULT]){
        [[ServiceApiMapper Api] getAllProductsOnPage:currentPage andQuantity:PRODUCTS_QUANTITY_REQUEST withBlock:block];
    }else if([self.category.id isEqualToString:CATEGORY_SEARCH]){
        [[ServiceApiMapper Api] getProductsBySearch:self.category.name onPage:currentPage andQuantity:PRODUCTS_QUANTITY_REQUEST withBlock:block];
    }
    else{
        [[ServiceApiMapper Api] getProductsByCategoryId:self.category.id onPage:currentPage andQuantity:PRODUCTS_QUANTITY_REQUEST withBlock:block];
    }
}


-(void) getPaddingToCell{
   /* NSInteger screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    CGFloat margin = screenWidth % CELL_WIDTH;
    NSInteger numberOfColuns = screenWidth / CELL_WIDTH;
    marginLeft = margin/ (numberOfColuns + 2);*/
    marginLeft = 10;
}

-(void) startShowGrid{
    [self.activityIndicator stopAnimating];
    [self.activityIndicator setUserInteractionEnabled:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma UICollectionViews Data Source
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ProductsCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    cell.product = [products objectAtIndex:indexPath.row];
    
    cell.responseBlock = ^(Product* product){
        [self callProductViewController:product];
    };
    
    [cell setupView];
    return cell;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return NUMBER_SECTIONS;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.activityIndicator isAnimating] ? 0 : [products count];
}

-(UIEdgeInsets) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(marginLeft, marginLeft, marginLeft, marginLeft);
}
-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
     return CGSizeMake(self.collectionView.bounds.size.width/2 - 2*marginLeft, CELL_HEIGHT);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return marginLeft;
}

-(UICollectionReusableView*) collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    if (kind == UICollectionElementKindSectionHeader && [self showBannersOnGrid]){
        BannesView* bannerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:HEADER_IDENTIFIER forIndexPath:indexPath];
        [bannerView setupCell];
        return bannerView;
    }
    return nil;
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if ([self showBannersOnGrid]){
        return CGSizeMake(self.collectionView.bounds.size.width,self.collectionView.bounds.size.width/1.666666667f);
    }
    return CGSizeMake(0,0);
}



-(void) scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.bounds.size.height)) {
        [self scrollToBottom];
    }
}

-(void) scrollToBottom{
    if(canLoadScroll){
        canLoadScroll = NO;
        currentPage++;
        [self getCategoryProducts];
    }
}

-(BOOL) showBannersOnGrid{
    return
    [self.category.id isEqualToString:CATEGORY_DEFAULT] &&
    [StoreConfig getStore].model == BUSINESS &&
    [StoreConfig getStore].banners != nil &&
    [[StoreConfig getStore].banners count] > 0 &&
    ![self.activityIndicator isAnimating];
}


#pragma UICollectionViews Delegate
-(void) callProductViewController:(Product*) product{
    
    if(![self closedLeftMenu]){
        return;
    }
    
    NavigationViewController* navigationController = (NavigationViewController* )self.navigationController;
    ProductViewController * productViewController = [navigationController returnViewControllerFromIdentifier: [navigationController className:[ProductViewController class]]];
    productViewController.product = product;
    [navigationController pushViewController:productViewController animated:YES];
}

-(BOOL) closedLeftMenu{
    TheSidebarController * controller = self.sidebarController;
    if(controller.sidebarIsPresenting){
        [controller.leftSidebarViewController.view endEditing:YES];
        [controller dismissSidebarViewController];
        return NO;
    }
    return YES;
}

@end
