//
//  ProductChoiceModelViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterModelViewControllers.h"
#import "Product.h"
#import "ServiceApiMapper.h"
#import "Variation.h"

@interface ProductChoiceModelViewController : CenterModelViewControllers<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *outletButton;
@property (strong, nonatomic) Product* product;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorButton;
- (IBAction)onConfirmClick:(id)sender;

@end
