//
//  WebViewCheckout.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 16/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewCheckout : UIViewController<UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UINavigationBar *navigationBar;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UINavigationItem *navigationItemCustom;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndication;
- (IBAction)onCancelClick:(id)sender;
@end
