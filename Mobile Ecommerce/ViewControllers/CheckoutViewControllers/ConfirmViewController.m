//
//  ConfirmViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 29/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "ConfirmViewController.h"
#import "ServiceApiMapper.h"
#import "Dialog.h"
#import "Cart.h"
#import "ShippingMethod.h"
#import "CartViewSimple.h"
#import "CartTotalSimpleCell.h"
#import "NSString+HTML.h"

//TrayCommerce
#import "TrayStorage.h"
#import "TrayPaymentMethod.h"
#import "NavigationViewController.h"
#import "CreditCardViewController.h"

#import <EventTracker/GlobalEventTracker.h>

#define NUMBER_SECTIONS 1
#define CELL_CART_IDENTIFIER @"CartViewSimple"
#define CELL_CART_TOTAL_IDENTIFIER @"CartTotalSimpleCell"


#define CELL_CART_HEIGHT 200
#define CELL_CART_TOTAL_HEIGHT 50

@interface ConfirmViewController()<UIAlertViewDelegate>{
    Cart * cart;
    ShippingMethod* shippingMethod;
    NSString* urlRedirect;
}

@end

@implementation ConfirmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    [self setupNavigationBar:@"CONFIRMAR COMPRA"];
    self.confirmButton.backgroundColor = [StoreConfig getStoreColor];
    [self setShow:NO];
    [self getCart];
}

-(void) getCart{
    [[ServiceApiMapper Api] getCartWithBlock:^(NSObject * object, NSError * error) {
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getCart) withObject:nil afterDelay:8];
        }
        else{
            cart = (Cart*) object;
            [self getShippingMethods];
        }
    }];
}

-(void) getShippingMethods{
    //Tray Commerce
    shippingMethod = [TrayStorage getShippingMethods];
    [self showTable];
}

-(void) showTable{
    [self.tableView reloadData];
    [self setShow:YES];
    [self setEnable:YES];
}


#pragma UITableView datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return cart ?  shippingMethod ?   [cart.products count] + 3 :[cart.products count] + 2 : 0;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath.row < [cart.products count] ?[self getProdutCartCellWithProductCart:cart.products[indexPath.row]]:[self getTotalCellsAtIndex:indexPath.row];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath.row < [cart.products count] ? CELL_CART_HEIGHT : CELL_CART_TOTAL_HEIGHT;
}

-(CartTotalSimpleCell*) getTotalCellsAtIndex:(NSInteger) index{
    
    CartTotalSimpleCell* cell = [self.tableView dequeueReusableCellWithIdentifier:CELL_CART_TOTAL_IDENTIFIER];
    
    switch (index - [cart.products count]) {
        case 0:
            cell.labelValue.text = [self getStringValueWithTitle:@"Subtotal :" andValue:[[cart.value stringByReplacingOccurrencesOfString:@"," withString:@"."]  floatValue]];
            break;
            
        case 1:
            if(shippingMethod){
                cell.labelValue.text = [self getStringValueWithTitle:@"Valor de entrega :" andValue:[[shippingMethod.value stringByReplacingOccurrencesOfString:@"," withString:@"."]  floatValue]];
                break;
            }

            
        default:
            if(shippingMethod){
                cell.labelValue.text = [self getStringValueWithTitle:@"Total :" andValue: [[shippingMethod.value stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue] + [[cart.value  stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue]];
            }else{
                cell.labelValue.text = [self getStringValueWithTitle:@"Total :" andValue: [[cart.value  stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue]];
            }
            break;
    }
    return cell;
}

-(NSString*) getStringValueWithTitle:(NSString*) title andValue:(float) value{
    return [[NSString stringWithFormat:@"%@ R$ %.2f",title, value ] stringByReplacingOccurrencesOfString:@"." withString:@","];
}

-(CartViewSimple*) getProdutCartCellWithProductCart:(ProductCart*) product{

    CartViewSimple* cell = [self.tableView dequeueReusableCellWithIdentifier:CELL_CART_IDENTIFIER];
    cell.productName.text = [product.name decodeHTMLCharacterEntities];
    cell.quantity.text = [NSString stringWithFormat:@"%lu",(long) product.quantity];
    cell.price.text = [self getStringValueWithTitle:@"" andValue:[[product.price stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue] * product.quantity];
    
    return cell;
}


-(void) setShow:(BOOL) enable{
    [self.confirmButton setHidden:!enable];
    enable ? [self.activityIndicator stopAnimating] : [self.activityIndicator startAnimating];
    [self.tableView setHidden:!enable];
}

-(void) setEnable:(BOOL) enable{
    [self.confirmButton setUserInteractionEnabled:enable];
    enable ? [self.activityIndicator stopAnimating] : [self.activityIndicator startAnimating];
    [self.tableView setUserInteractionEnabled:enable];
}

- (IBAction)onConfirmClick:(id)sender {
    
    //TrayCommerce
    TrayPaymentMethod* paymentMethod = [TrayStorage getPaymentsMethods];
    
    if(paymentMethod && paymentMethod.isCredit){
        [self callCreditViewController];
    }
    else{
        [self confirmCheckOut];
    }
}

-(void) callCreditViewController{
    NavigationViewController* navigationViewController = [NavigationViewController new];
    [self.navigationController pushViewController:
    [navigationViewController returnViewControllerFromIdentifier:
    [navigationViewController className:[CreditCardViewController class]]] animated:YES];
}

-(void) confirmCheckOut{

    [self setEnable:NO];
    
    [[ServiceApiMapper Api] finnishOrderWithBlock:^(BOOL success, NSString * url , NSError * error) {
        
        [self setEnable:YES];
        if(error){
            [Dialog showDialog:@"A solicitação não pôde ser concluída. Verifique seu endereço de entrega e seus dados cadastrais."];
        }else{
            urlRedirect = url;
            [self confirmSucess];
        }
    }];
}


-(void) confirmSucess{
    
    UIAlertView* continueAlert;
    
    if(![shippingMethod paymentRequired]){
        
       continueAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"A solicicitação da compra já está em andamento! Em breve você receberá um motoboy no endereço cadastrado" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }else{        
        continueAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"A solicicitação da compra já está em andamento, você será redirecionado para continuar o pagamento." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    }
    [GlobalEventTracker purchasedCheckout];
    [continueAlert show];
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if([shippingMethod paymentRequired]){
        [self callBrowser];
    }
}

-(void) callBrowser{
    NSURL *url = [NSURL URLWithString:urlRedirect];
    [[UIApplication sharedApplication] openURL:url];
    [self performSelector:@selector(dismissView) withObject:nil afterDelay:5];
}

-(void) dismissView{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
