//
//  AddressCreateViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "AddressCreateViewController.h"
#import "ServiceApiMapper.h"
#import "Dialog.h"
#import "Address.h"

#define NUMBER_OF_COMPONEMTS 1


@interface AddressCreateViewController(){
    NSArray* states;
}
@end

@implementation AddressCreateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    [self setupNavigationBar:@"CADASTRAR ENDEREÇO"];
    self.postalCode.format = @"XXXXX-XXX";
    self.buttonSave.backgroundColor = [StoreConfig getStoreColor];
    [self getStates];
}

-(void) getStates{
    
    [[ServiceApiMapper Api] getStatesToCreateAddressWithBlock:^(NSArray * items, NSError * error) {
        
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getStates) withObject:nil afterDelay:8];
        }else{
            states = items;
            [self showView];
        }
    }];
}
-(void) showView{
    [self.pickerStates reloadAllComponents];
    [self setEnable:YES];
}

#pragma PickerData source and delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return NUMBER_OF_COMPONEMTS;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return states ? states.count : 0;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return states[row];
}

- (IBAction)onSaveClick:(id)sender {
    if([self isAllInformationValid]){
        [self createAddress];
    }
}

-(void) setEnable:(BOOL) enable{
    
    [self.city setEnabled:enable];
    [self.postalCode setEnabled:enable];
    [self.neighborhood setEnabled:enable];
    [self.number setEnabled:enable];
    [self.pickerStates setUserInteractionEnabled:enable];
    [self.addressLine1 setEnabled:enable];
    [self.addressLine2 setEnabled:enable];
    [self.buttonSave setEnabled:enable];
    enable ? [self.activityIndicator stopAnimating] : [self.activityIndicator startAnimating];
}

-(BOOL) isAllInformationValid{
    
    if([self.postalCode.text length] != 9 ){
        [Dialog showDialog:@"Por favor insira um CEP válido"];
        return NO;
    }
    
    if([self.city.text length] == 0){
        [Dialog showDialog:@"Por favor insira a cidade"];
        return NO;
    }
    
    if([self.neighborhood.text length] == 0){
        [Dialog showDialog:@"Por favor insira o bairro"];
        return NO;
    }
    
    if([self.number.text length] == 0){
        [Dialog showDialog:@"Por favor insira o número"];
        return NO;
    }
    
    if([self.addressLine1.text length] == 0){
        [Dialog showDialog:@"Por favor insira a rua"];
        return NO;
    }
    
    return YES;
}

-(void) createAddress{
    
    [self setEnable:NO];

    Address * address = [Address new];
    address.addressLine1 = self.addressLine1.text;
    address.addressLine2 = self.addressLine2.text;
    address.number = self.number.text;
    address.postalCode = self.postalCode.text;
    address.neighborhood = self.neighborhood.text;
    address.city = self.city.text;
    address.state = states[[self.pickerStates selectedRowInComponent:0]];
    
    [[ServiceApiMapper Api] createAddress:address withBlock:^(BOOL success, NSError * error) {
        [self setEnable:YES];
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
        }else{
            [self successCreate];
        }
    }];
}

-(void) successCreate{
    [Dialog showDialog:@"Endereço criado com sucesso!"];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
