//
//  CheckoutModelViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "CheckoutModelViewController.h"

@interface CheckoutModelViewController ()

@end

@implementation CheckoutModelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupNavigationBar:(NSString*) title{
    
    self.navigationController.navigationBar.barTintColor = [StoreConfig getStoreColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    if([StoreConfig getStore].model == BUSINESS){
        self.title = [title lowercaseString];
    }else{
        self.title = [title uppercaseString];
    }
    
    [self addButtons];
}

-(void) addButtons{
    
    if([[self.navigationController viewControllers] count] > 1){
        UIImage *buttonImage = [UIImage imageNamed:@"ic_action_arrowback"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage
                forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self
                   action:@selector(popViewController)
         forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.leftBarButtonItem = barButtonItem;
    }
    else{
        UIImage *buttonImage = [UIImage imageNamed:@"ic_action_remove"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage
                forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self
               action:@selector(dismissViewController)
         forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.leftBarButtonItem = barButtonItem;
    }
    

}

-(void) popViewController{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) dismissViewController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
