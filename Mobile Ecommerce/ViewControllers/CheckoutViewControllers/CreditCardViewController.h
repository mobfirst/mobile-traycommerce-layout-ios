//
//  CreditCardViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 30/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckoutModelViewController.h"
#import "REFormattedNumberField.h"

@interface CreditCardViewController :CheckoutModelViewController<UIPickerViewDataSource, UIPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *viewRoot;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) IBOutlet UIPickerView *picketSplitNumber;
@property (strong, nonatomic) IBOutlet UILabel *totalValue;
@property (strong, nonatomic) IBOutlet UITextField *cardName;
@property (strong, nonatomic) IBOutlet UITextField *cardNumber;
@property (strong, nonatomic) IBOutlet REFormattedNumberField *cartMonth;
@property (strong, nonatomic) IBOutlet REFormattedNumberField *cardYear;
@property (strong, nonatomic) IBOutlet UITextField *cardCvv;
@property (strong, nonatomic) IBOutlet UIButton *buttonConfirm;

- (IBAction)onButtonClick:(id)sender;

@end
