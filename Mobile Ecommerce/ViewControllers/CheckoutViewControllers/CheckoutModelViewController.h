//
//  CheckoutModelViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreConfig.h"
#import "ServiceApiMapper.h"
#import "Dialog.h"

@interface CheckoutModelViewController : UIViewController

-(void) setupNavigationBar:(NSString*) title;

@end
