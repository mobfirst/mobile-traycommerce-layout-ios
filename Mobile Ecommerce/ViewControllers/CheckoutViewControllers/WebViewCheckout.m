//
//  WebViewCheckout.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 16/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "WebViewCheckout.h"
#import "StoreConfig.h"
#import "ServiceApiMapper.h"
#import "Cart.h"
#import "Dialog.h"

@implementation WebViewCheckout

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}


-(void) setupView{
    
    [[UINavigationBar appearance] setTintColor:[StoreConfig getStoreColor]];
    [[UINavigationBar appearance] setBarTintColor:[StoreConfig getStoreColor]];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    self.navigationItemCustom.titleView.backgroundColor = [StoreConfig getStoreColor];
    self.view.backgroundColor = [StoreConfig getStoreColor];
    self.navigationItemCustom.title = @"MEU CARRINHO";
    
    [self getUrlRedirect];
}


-(void) getUrlRedirect{
    
    [[ServiceApiMapper Api] getCartWithBlock:^(NSObject * cart, NSError * error) {
        
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getUrlRedirect) withObject:nil afterDelay:8];
            return;
        }
        
        [[ServiceApiMapper Api] confirmCheckoutWithCart: (Cart*)cart withBlock:^(NSObject * url, NSError * error) {
           
            if(error){
                [Dialog showConnectionErrorOnView:self.view];
                [self performSelector:@selector(getUrlRedirect) withObject:nil afterDelay:8];
                return;
            }
            
            [self setupWebView:(NSString*) url];
        }];
    }];
}

-(void) setupWebView:(NSString*) url{    
    NSURL* nsUrl = [NSURL URLWithString:url];
    NSURLRequest* request = [NSURLRequest requestWithURL:nsUrl];
    [self.webView loadRequest:request];
}


#pragma UIWebViewDelegate
-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.activityIndication stopAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.activityIndication stopAnimating];
}


- (IBAction)onCancelClick:(id)sender {
      [self dismissViewControllerAnimated:YES completion:nil];
}
@end
