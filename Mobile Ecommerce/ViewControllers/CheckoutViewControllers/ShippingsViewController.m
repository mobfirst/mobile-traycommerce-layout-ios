//
//  ShippingsViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "ShippingsViewController.h"
#import "ShippingCell.h"
#import "ShippingMethod.h"
#import "ServiceApiMapper.h"
#import "Dialog.h"
#import "PaymentsViewController.h"
#import "NavigationViewController.h"
#import "ConfirmViewController.h"

#define NUMBER_SECTIONS 1
#define CELL_IDENTIFIER @"ShippingCell"

@interface ShippingsViewController(){
    NSArray* shippingMethods;
}

@end

@implementation ShippingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    [self setupNavigationBar:@"MÉTODOS DE ENTREGA"];
    [self getCart];
}

-(void) getCart{
    
    
    [[ServiceApiMapper Api] getCartWithBlock:^(NSObject * object, NSError * error) {

        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getCart) withObject:nil afterDelay:8];
        }else{
         
            if([self hasPhysicalProduct:(Cart*) object]){
                [self getShippingMethods];
            }else{
                [self callPaymentsMethods];
            }
        }
    }];
    
}


-(BOOL) hasPhysicalProduct:(Cart*) cart{
    
    for(ProductCart* productCart in cart.products){
        
        if(!productCart.virtualProduct){
            return YES;
        }
    }
    return NO;
}


-(void) getShippingMethods{
    
    [[ServiceApiMapper Api] getShippingMethodsWithBlock:^(NSArray * items, NSError * error) {
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getShippingMethods) withObject:nil afterDelay:8];
        }
        else{
            shippingMethods = items;
            [self showItems];
        }
    }];
}

-(void) showItems{
    [self.tableView reloadData];
    [self setEnable:YES];
}

-(void) setEnable:(BOOL) enable{
    [self.tableView setUserInteractionEnabled:enable];
    enable ? [self.activityIndicator stopAnimating] : [self.activityIndicator startAnimating];
}

#pragma UITableView datasource 

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return shippingMethods ? [shippingMethods count] : 0;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self getShippingCellWithShippingMethod:shippingMethods[indexPath.row]];
}

-(ShippingCell*) getShippingCellWithShippingMethod:(ShippingMethod*) method{
    
    ShippingCell* cell = [self.tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    cell.title.text = method.title;
    cell.value.text = [[NSString stringWithFormat:@"R$ %@",method.value] stringByReplacingOccurrencesOfString:@"." withString:@","];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self onSelectedMethod:shippingMethods[indexPath.row]];
}

-(void) onSelectedMethod:(ShippingMethod*) method{
    
    [self setEnable:NO];

    if(![method paymentRequired]){
    
        [[ServiceApiMapper Api] insertPaymentMethod:nil WithBlock:^(BOOL success, NSError *error ) {
            
            if(error){
                [Dialog showConnectionErrorOnView:self.view];
            }else{
                [self callConfirmView];
            }
        }];
        
    }
    else{
    
    
        [[ServiceApiMapper Api] insertShippingMethod:method WithBlock:^(BOOL success, NSError * error) {
            [self setEnable:YES];
            if(error){
                 [Dialog showConnectionErrorOnView:self.view];
            }else{
                [self callPaymentsMethods];
            }
        }];
    }
}

-(void) callPaymentsMethods{
    NavigationViewController* navigationViewController = [NavigationViewController new];
    [self.navigationController pushViewController:
    [navigationViewController returnViewControllerFromIdentifier:
    [navigationViewController className:[PaymentsViewController class]]] animated:YES];
}

-(void) callConfirmView{
    NavigationViewController* navigationViewController = [NavigationViewController new];
    [self.navigationController pushViewController:
     [navigationViewController returnViewControllerFromIdentifier:
      [navigationViewController className:[ConfirmViewController class]]] animated:YES];
}

@end
