//
//  PaymentsViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "PaymentsViewController.h"
#import "ServiceApiMapper.h"
#import "PaymentsCell.h"
#import "Dialog.h"
#import "PaymentMethod.h"
#import "ConfirmViewController.h"
#import "NavigationViewController.h"

#define NUMBER_SECTIONS 1
#define CELL_IDENTIFIER @"PaymentsCell"

@interface PaymentsViewController(){
    NSArray* paymentsMethods;
}

@end

@implementation PaymentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    [self setupNavigationBar:@"PAGAMENTOS"];
    [self getPaymentsMethods];
}

-(void) getPaymentsMethods{
    
    [[ServiceApiMapper Api] getPaymentMethodsWithBlock:^(NSArray * items, NSError * error) {
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getPaymentsMethods) withObject:nil afterDelay:8];
        }
        else{
            paymentsMethods = items;
            [self showItems];
        }
    }];
}

-(void) showItems{
    [self.tableView reloadData];
    [self setEnable:YES];
}

-(void) setEnable:(BOOL) enable{
    [self.tableView setUserInteractionEnabled:enable];
    enable ? [self.activityIndicator stopAnimating] : [self.activityIndicator startAnimating];
}

#pragma UITableView datasource 

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return paymentsMethods ? [paymentsMethods count] : 0;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self getPaymentCellWithPaymentMethod:paymentsMethods[indexPath.row]];
}

-(PaymentsCell*) getPaymentCellWithPaymentMethod:(PaymentMethod*) method{
    PaymentsCell* cell = [self.tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    cell.title.text = method.name;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self onSelectedMethod:paymentsMethods[indexPath.row]];
}

-(void) onSelectedMethod:(PaymentMethod*) method{
    
    [self setEnable:NO];
    
    [[ServiceApiMapper Api] insertPaymentMethod:method WithBlock:^(BOOL success, NSError * error) {
        [self setEnable:YES];
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
        }else{
            [self callPaymentsMethods];
        }
    }];
}

-(void) callPaymentsMethods{
    NavigationViewController* navigationViewController = [NavigationViewController new];
    [self.navigationController pushViewController:
    [navigationViewController returnViewControllerFromIdentifier:
    [navigationViewController className:[ConfirmViewController class]]] animated:YES];
}


@end
