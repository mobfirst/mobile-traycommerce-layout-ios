//
//  PaymentsViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckoutModelViewController.h"

@interface PaymentsViewController :  CheckoutModelViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
