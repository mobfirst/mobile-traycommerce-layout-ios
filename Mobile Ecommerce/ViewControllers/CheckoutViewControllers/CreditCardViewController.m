//
//  CreditCardViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 30/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "CreditCardViewController.h"
//TrayCommerce
#import "TrayStorage.h"
#import "TrayPaymentsValue.h"
#import "CreditCard.h"

#import <EventTracker/GlobalEventTracker.h>

#define NUMBER_OF_COMPONEMTS 1

@interface CreditCardViewController()<UIAlertViewDelegate>{
    NSArray* payments;
}
@end
@implementation CreditCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    [self setupNavigationBar:@"CARTÃO DE CRÉDITO"];
    self.buttonConfirm.backgroundColor = [StoreConfig getStoreColor];
    self.cartMonth.format = @"XX";
    self.cardYear.format = @"XXXX";
    [self getPaymentsValues];
}

-(void) getPaymentsValues{
    
    [[ServiceApiMapper Api] getTrayCheckoutTransationValuesWithBlock:^(NSArray * items, NSError * error) {
        
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getPaymentsValues) withObject:nil afterDelay:8];
        }
        else{
            payments = items;
            [self showView];
        }
    }];
}

-(void) showView{
    [self.picketSplitNumber reloadAllComponents];
    [self setTotalValueAtIndex:0];
    [self.activityIndicator stopAnimating];
    [self.viewRoot setHidden:NO];
}

#pragma PickerData source and delegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return NUMBER_OF_COMPONEMTS;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return payments ? payments.count : 0;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    TrayPaymentsValue * payment = payments[row];
    return [[NSString stringWithFormat:@"%@ X R$ %.2f",payment.split,[payment.valueSplit floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@","];
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [self setTotalValueAtIndex:row];
}

-(void) setTotalValueAtIndex:(NSInteger) index{
    TrayPaymentsValue * payment = payments[index];
    self.totalValue.text = [[NSString stringWithFormat:@"%@ X R$ %.2f - Total : R$ %.2f",payment.split,[payment.valueSplit floatValue],[payment.valueTotal floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@","];
}

- (IBAction)onButtonClick:(id)sender {

    if([self isAllInformationValid]){
        [self confirmCheckout];
    }
}

-(void) setEnable:(BOOL) enable{
    [self.viewRoot setUserInteractionEnabled:enable];
    enable ? [self.activityIndicator stopAnimating] : [self.activityIndicator startAnimating];
}

-(BOOL) isAllInformationValid{
    
    if([self.cardName.text length] == 0 ){
        [Dialog showDialog:@"Favor inserir o nome do titular do cartão"];
        return NO;
    }
    
    if([self.cardNumber.text length] == 0){
        [Dialog showDialog:@"Favor inserir o número do cartão"];
        return NO;
    }
    
    if([self.cartMonth.text length] == 0){
        [Dialog showDialog:@"Favor inserir o mês de validade do cartão"];
        return NO;
    }
    
    int month = (int)[self.cartMonth.text integerValue];
    
    if(month == 0  || month > 12){
        [Dialog showDialog:@"Favor inserir um mês de válido"];
        return NO;
    }
    
    if([self.cardYear.text length] != 4){
        [Dialog showDialog:@"Favor inserir o ano de validade do cartão"];
        return NO;
    }
    
    if([self.cardCvv.text length] == 0){
        [Dialog showDialog:@"Favor inserir o código cvv do cartão"];
        return NO;
    }
    
    return YES;
}

-(void) confirmCheckout{
    
    [self setEnable:NO];

    [self saveCard];

    [[ServiceApiMapper Api] finnishOrderWithBlock:^(BOOL success, NSString * url , NSError * error) {
            
        [self setEnable:YES];
            
        if(error){
            [Dialog showDialog:@"A solicitação não pôde ser concluída. Verifique seu endereço de entrega e seus dados cadastrais."];
        }else if(success){
            [self confirmSucess];
        }else{
            [Dialog showDialog:@"Não foi possível a realização da compra. \nVerifique seus dados e tente novamente."];
        }
    }];
}
    
-(void) confirmSucess{
    UIAlertView* continueAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"A solicitação da compra já está em andamento, logo você receberá um email, informando o status" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [GlobalEventTracker purchasedCheckout];
    [continueAlert show];
}
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void) saveCard{
    
    //TrayCommerce
    
    CreditCard * creditCart = [CreditCard new];
    creditCart.ownerName = self.cardName.text;
    creditCart.cardNumber = self.cardNumber.text;
    
    int month = (int)[self.cartMonth.text integerValue];
    creditCart.monthExpirateDate = month < 10 ? [NSString stringWithFormat:@"0%@",self.cartMonth.text] : self.cartMonth.text;
    creditCart.yearExpirateDate = self.cardYear.text;
    creditCart.cvv = self.cardCvv.text;
    creditCart.split = [NSString stringWithFormat:@"%ld", ((long)[self.picketSplitNumber selectedRowInComponent:0] + 1)];
    [TrayStorage saveUserCreditCard:creditCart];
}


@end
