//
//  AddressChoiceViewController.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "AddressChoiceViewController.h"
#import "AddressCell.h"
#import "Address.h"
#import "Utils.h"
#import "ServiceApiMapper.h"
#import "Dialog.h"
#import "NavigationViewController.h"
#import "AddressCreateViewController.h"
#import "ShippingsViewController.h"


#define NUMBER_SECTIONS 1
#define CELL_IDENTIFIER @"AddressCell"

@interface AddressChoiceViewController (){
    NSMutableArray* addresses;
}
@end
@implementation AddressChoiceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void) setupView{
    [self setupNavigationBar:@"MEUS ENDEREÇOS"];
    [self setAddButton];
    
}

-(void) setAddButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeContactAdd];
    button.tintColor = [UIColor whiteColor];
    [button addTarget:self
               action:@selector(createAddresses)
     forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getAddress];
    
}
-(void) getAddress{
    
    [[ServiceApiMapper Api] getUserAddressesWithBlock:^(NSArray * items, NSError * error) {
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
            [self performSelector:@selector(getAddress) withObject:nil afterDelay:8];
        }
        else{
            addresses = [NSMutableArray new];
            [addresses addObjectsFromArray:items];
            [self showAddress];
        }
    }];
}

-(void) showAddress{
    if([addresses count] > 0){
        self.tableView.separatorColor = [UIColor lightGrayColor];
    }
    [self.noAddresses setHidden:[addresses count] > 0];
    [self.tableView reloadData];
    [self setEnable:YES];
}

-(void) setEnable:(BOOL) enable{
    [self.tableView setUserInteractionEnabled:enable];
    enable ? [self.activityIndicator stopAnimating] : [self.activityIndicator startAnimating];
}

#pragma UITableView datasource 

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return NUMBER_SECTIONS;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return addresses ? [addresses count] : 0;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self getAddressCellWithAddress:addresses[indexPath.row]];
}

-(AddressCell*) getAddressCellWithAddress:(Address*) address{
    AddressCell * cell = [self.tableView dequeueReusableCellWithIdentifier:CELL_IDENTIFIER];
    cell.addressText.text = [Utils convertAddressesToString:address];
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self selectedAddress:addresses[indexPath.row]];
}

-(void) selectedAddress:(Address*) address{
    
    [self setEnable:NO];
    
    [[ServiceApiMapper Api] choiceShippingAddress:address withBlock:^(BOOL success, NSError * error) {
   
        [self setEnable:YES];
        if(error){
            [Dialog showConnectionErrorOnView:self.view];
        }else{
            [self callShippingMethods];
        }
    }];
}

-(void) createAddresses{
    NavigationViewController* navigationViewController = [NavigationViewController new];
    [self.navigationController pushViewController:
    [navigationViewController returnViewControllerFromIdentifier:
    [navigationViewController className:[AddressCreateViewController class]]] animated:YES];
}
-(void) callShippingMethods{
    NavigationViewController* navigationViewController = [NavigationViewController new];
    [self.navigationController pushViewController:
    [navigationViewController returnViewControllerFromIdentifier:
    [navigationViewController className:[ShippingsViewController class]]] animated:YES];
}

@end
