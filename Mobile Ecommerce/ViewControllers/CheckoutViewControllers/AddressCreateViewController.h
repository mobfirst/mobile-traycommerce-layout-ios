//
//  AddressCreateViewController.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckoutModelViewController.h"
#import "REFormattedNumberField.h"

@interface AddressCreateViewController : CheckoutModelViewController<UIPickerViewDataSource, UIPickerViewDelegate>
@property (strong, nonatomic) IBOutlet UIPickerView *pickerStates;
@property (strong, nonatomic) IBOutlet REFormattedNumberField *postalCode;
@property (strong, nonatomic) IBOutlet UITextField *city;
@property (strong, nonatomic) IBOutlet UITextField *neighborhood;
@property (strong, nonatomic) IBOutlet UITextField *addressLine1;
@property (strong, nonatomic) IBOutlet UITextField *number;
@property (strong, nonatomic) IBOutlet UITextField *addressLine2;
@property (strong, nonatomic) IBOutlet UIButton *buttonSave;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)onSaveClick:(id)sender;
@end
