//
//  CenterModelViewControllers.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 24/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "CenterModelViewControllers.h"
#import <TheSidebarController/TheSidebarController.h>
#import "CartNavigationViewController.h"
#import "NavigationViewController.h"
#import "WebViewCheckout.h"
#import "ServiceStorageUser.h"
#import "Dialog.h"


#define BADGE_SIZE  12
#define BADGE_PADDING 2
#define BADGE_TEXT_ADJUST 0.2f


@interface CenterModelViewControllers (){
    UILabel* badgeText;
    UITapGestureRecognizer * recognizerGestureTap;
}
@end


@implementation CenterModelViewControllers

-(void)setupNavigationBar:(NSString*) title{
    
    self.navigationController.navigationBar.barTintColor = [StoreConfig getStoreColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    if([StoreConfig getStore].model == BUSINESS){
        self.title = [title lowercaseString];
    }else{
         self.title = [title uppercaseString];
    }
    
   
    [self addCallLeftMenuButton];
    
    [self addCardMenuButton];
    
    recognizerGestureTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:recognizerGestureTap];
}

-(void) setupNavigationBarWithBack:(NSString*) title{
    
    self.navigationController.navigationBar.barTintColor = [StoreConfig getStoreColor];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
    if([StoreConfig getStore].model == BUSINESS){
        self.title = [title lowercaseString];
    }else{
        self.title = [title uppercaseString];
    }
    
    [self addBackButton];
    
    [self addCardMenuButton];
    
    recognizerGestureTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self.view addGestureRecognizer:recognizerGestureTap];
    
}

-(void) addBackButton{
    
    if(!self.navigationController.navigationItem.leftBarButtonItem){
        
        UIImage *buttonImage = [UIImage imageNamed:@"ic_action_arrowback"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage
                forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self
                   action:@selector(dismissViewController)
         forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.leftBarButtonItem = barButtonItem;
    }
    
}

-(void)addCallLeftMenuButton{
    
    if(!self.navigationController.navigationItem.leftBarButtonItem){
        
        UIImage *buttonImage = [UIImage imageNamed:@"ic_action_menuleft"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage
                    forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self
                       action:@selector(callLeftMenuController)
             forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.leftBarButtonItem = barButtonItem;
    }
}

-(void) addCardMenuButton{
    
    if(!self.navigationController.navigationItem.rightBarButtonItem){
        
        UIImage *buttonImage = [UIImage imageNamed:@"ic_action_shopping_cart"];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:buttonImage
                forState:UIControlStateNormal];
        button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
        [button addTarget:self
                     action:@selector(callCartViewController)
          forControlEvents:UIControlEventTouchUpInside];
        
        //Cia Shop
        
        /*UIView * viewBase = [[UIView alloc] initWithFrame:CGRectMake(buttonImage.size.width - BADGE_SIZE + BADGE_PADDING/2, 0, BADGE_SIZE, BADGE_SIZE)];
        viewBase.backgroundColor = [StoreConfig getStoreColor];
        viewBase.layer.cornerRadius = BADGE_SIZE/2;
        
        UIView * viewContent = [[UIView alloc] initWithFrame: CGRectMake(BADGE_PADDING/2, BADGE_PADDING/2, BADGE_SIZE - BADGE_PADDING, BADGE_SIZE - BADGE_PADDING)];
        viewContent.backgroundColor = [UIColor whiteColor];
        viewContent.layer.cornerRadius = (BADGE_SIZE - BADGE_PADDING)/2;
        
        badgeText = [[UILabel alloc] initWithFrame: CGRectMake(BADGE_TEXT_ADJUST, 0, BADGE_SIZE - BADGE_PADDING, BADGE_SIZE - BADGE_PADDING)];
        
        badgeText.textColor = [StoreConfig getStoreColor];
        badgeText.textAlignment = NSTextAlignmentCenter;
        badgeText.font = [UIFont systemFontOfSize:8];
        
    
        [viewContent addSubview:badgeText];
        [viewBase addSubview:viewContent];
        [button addSubview:viewBase];*/
        
        
        UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
        
        self.navigationItem.rightBarButtonItem = barButtonItem;
    
        [self updateBadge];
    }
}

#pragma ACTIONS

-(void) updateBadge{
    if(badgeText){
        badgeText.text = @"8";
    }
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    
    if(self.navigationController.sidebarController.sidebarIsPresenting){
        [self.navigationController.sidebarController dismissSidebarViewController];
    }
}

-(void) dismissViewController{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) callLeftMenuController{
    
    if(self.navigationController.sidebarController.sidebarIsPresenting){
        [self.navigationController.sidebarController dismissSidebarViewController];
    }
    else{
        switch ([StoreConfig getStore].model) {
            case BUSINESS:
                [self.navigationController.sidebarController presentLeftSidebarViewControllerWithStyle:SidebarTransitionStyleFeedly];
                break;
                
            case ENTERPRISE:
               [self.navigationController.sidebarController presentLeftSidebarViewControllerWithStyle:SidebarTransitionStyleAirbnb];
                break;
        }
    }
}

-(void) callCartViewController{
    if([ServiceStorageUser userIsLogged]){
        NavigationViewController* navigationController = (NavigationViewController* )self.navigationController;
        CartNavigationViewController * cartViewController = [navigationController returnViewControllerFromIdentifier: [navigationController className:[CartNavigationViewController class]]];
        cartViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [navigationController presentViewController:cartViewController animated:YES completion:nil];
    }else{
        [Dialog showToast:@"Você deve estar logado" onView:self.view];
    }
}

@end
