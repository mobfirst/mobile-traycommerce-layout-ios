//
//  CenterModelViewControllers.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 24/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreConfig.h"

@interface CenterModelViewControllers : UIViewController

-(void) setupNavigationBar:(NSString*) title;
-(void) setupNavigationBarWithBack:(NSString*) title;
-(void) updateBadge;

-(void)addCallLeftMenuButton;
-(void)addCardMenuButton;

@end
