//
//  SocialMapper.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 17/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "SocialMapper.h"
#import "YouTubeObject.h"
#import "NSString+HTML.h"
#import "TwitterObject.h"
#import "TwitterUrls.h"
#import "InstagramObjects.h"

@implementation SocialMapper


#pragma Utils
+(NSDictionary*) convertNSStringToDictionary:(NSString*) string{
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    return json;
}
+(id) checkIfInformationIsNil:(id) information{
    return [information isKindOfClass:[NSNull class]] ? nil : information;
}
+(id) getValue:(NSString*) value fromDictionary:(id) dictionary{
    id data = [dictionary objectForKey:value];
    return [self checkIfInformationIsNil:data];
}

#pragma YOUTUBE
+(NSArray*) getYouTubeVideosFromApiResponse:(NSString*) apiResponse{

    NSString* FEED = @"feed";
    NSString* ENTRY = @"entry";
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    
    if(!json){
        return nil;
    }
    
    id feed = [self getValue:FEED fromDictionary:json];
    if(!feed){
        return nil;
    }
    
    id entries = [self getValue:ENTRY fromDictionary:feed];
    if(!entries){
        return nil;
    }
    
    
    NSMutableArray* items = [NSMutableArray new];

    for(id entry in entries){
        
        YouTubeObject* object = [self getVideoObjectFromDictionary:entry];
        if(object){
            [items addObject:object];
        }
        
    }
    return items;
    
}
+(YouTubeObject*) getVideoObjectFromDictionary:(id) dictionary{

    if(!dictionary){
        return nil;
    }
    
    NSString* MEDIA = @"media$group";
    NSString* MEDIA_CONTENT = @"media$content";
    NSString* MEDIA_CONTENT_FORMAT = @"video/3gpp";
    
   
    NSString* MEDIA_DESCRIPTION = @"media$description";
    NSString* MEDIA_IMAGE = @"media$thumbnail";
    NSString *MEDIA_TITLE = @"media$title";
    
    NSString* PUBLISHED = @"published";
    NSString* STATISTICS = @"yt$statistics";
    NSString* VIDEO_ID = @"yt$videoid";
    
    

    id media = [self getValue:MEDIA fromDictionary:dictionary];
    
    if(!media){
        return nil;
    }
    
    YouTubeObject* object = [YouTubeObject new];
    
    
    id mediaVideoId = [self getValue:VIDEO_ID fromDictionary:media];
    
    if(!mediaVideoId){
        return nil;
    }
    
    if(mediaVideoId){
        NSString* TYPE = @"$t";
        object.urlVideo = [self getValue:TYPE fromDictionary:mediaVideoId];
        if(!object.urlVideo){
            return nil;
        }
    }
    
    
    
    id mediaContent = [self getValue:MEDIA_CONTENT fromDictionary:media];
    for(id itemMedia in mediaContent){
        
        NSString* TYPE = @"type";
        NSString* DURATION =@"duration";
        
        NSString* typeMedia = [self getValue:TYPE fromDictionary:itemMedia];
        if(typeMedia && [typeMedia isEqualToString:MEDIA_CONTENT_FORMAT]){
            
            object.duration = (int)[[self getValue:DURATION fromDictionary:itemMedia] integerValue];
            break;
        }
    }

    id mediaDescription = [self getValue:MEDIA_DESCRIPTION fromDictionary:media];
    if(mediaDescription){
        NSString* TYPE = @"$t";
        object.videoDescription = [self getValue:TYPE fromDictionary:mediaDescription];
        
        if(object.videoDescription){
            object.videoDescription = [object.videoDescription decodeHTMLCharacterEntities];
        }
    }
    
    
    id mediaTitle = [self getValue:MEDIA_TITLE fromDictionary:media];
    if(mediaTitle){
        NSString* TYPE = @"$t";
        object.title = [self getValue:TYPE fromDictionary:mediaTitle];
    }
    
    
    id mediaThumbnail = [self getValue:MEDIA_IMAGE fromDictionary:media];
    
    if(mediaThumbnail){
        
        id maxImage = nil;
        int withMax = 0;
        
        for(id image in mediaThumbnail){
            NSString * WIDTH = @"width";
            int width = (int) [[self getValue:WIDTH fromDictionary:image] integerValue];
            if(width > withMax){
                withMax = width;
                maxImage = image;
            }
        }
        
        if(maxImage){
            NSString* URL = @"url";
            object.urlImage = [self getValue:URL fromDictionary:maxImage];
        }
    }
    
    id publishedObject = [self getValue:PUBLISHED fromDictionary:dictionary];
    
    if(publishedObject) {
        NSString* TYPE = @"$t";
        object.published = [self getValue:TYPE fromDictionary:publishedObject];
    }
    
    id statisticsObject = [self getValue:STATISTICS fromDictionary:dictionary];
    if(statisticsObject){
        NSString* VIEWCOUNT = @"viewCount";
        object.viewCount = [[self getValue:VIEWCOUNT fromDictionary:statisticsObject] integerValue];
    }
    
    return object;
}

#pragma Twitter
+(NSString*) getTwitterApiToken:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    if(!json){
        return nil;
    }
    NSString* ACCESS_TOKEN = @"access_token";
    return [self getValue:ACCESS_TOKEN fromDictionary:json];
}
+(NSArray*) getAllTweetsFromAPIResponse:(NSString*) apiResponse{
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    if(!json){
        return nil;
    }
    
    NSMutableArray* items = [NSMutableArray new];
    for(id item in json){
        TwitterObject * tweet = [self getTweetFromDictionary:item];
        if(tweet){
            [items addObject:tweet];
        }
    }
    return items;
}
+(TwitterObject*) getTweetFromDictionary:(id) json{
    
    if(!json){
        return nil;
    }
    
    NSString* CREATED_AT = @"created_at";
    NSString* TEXT =@"text";
    NSString* USER =@"user";
    NSString* NAME =@"name";
    NSString* SCREEN_NAME =@"screen_name";
    NSString* USER_IMAGE = @"profile_image_url";
    
    NSString* ENTITIES = @"entities";
    NSString* URLS = @"urls";
    NSString* URL = @"url";
    NSString* INDICES = @"indices";
    
    TwitterObject * tweet = [TwitterObject new];
    tweet.timeCreate = [self getValue:CREATED_AT fromDictionary:json];
    tweet.text = [self getValue:TEXT fromDictionary:json];
    
    id userObject = [self getValue:USER fromDictionary:json];
    if(userObject){
        tweet.userName = [self getValue:NAME fromDictionary:userObject];
        tweet.user = [self getValue:SCREEN_NAME fromDictionary:userObject];
        tweet.userImage = [self getValue:USER_IMAGE fromDictionary:userObject];
    }
    
    id entittesObject = [self getValue:ENTITIES fromDictionary:json];
    
    if(entittesObject){
        id urlsObject = [self getValue:URLS fromDictionary:entittesObject];
        if(urlsObject){
         
            NSMutableArray * urlsItems = [NSMutableArray new];
            
            for(id item in urlsObject){
                
                TwitterUrls * tweeturl = [TwitterUrls new];
                
                tweeturl.url = [self getValue:URL fromDictionary:item];
                
                NSArray* index = [self getValue:INDICES fromDictionary:item];
                
                if(index && [index count] > 1){
                    tweeturl.indiceStart = [[index objectAtIndex:0] integerValue];
                    tweeturl.indiceEnd = [[index objectAtIndex:1] integerValue];
                }
                
                if(tweeturl.url && tweeturl.indiceStart  && tweeturl.indiceEnd){
                    [urlsItems addObject:tweeturl];
                }
            }
            tweet.urls = urlsItems;
        }
    }
    
    return tweet;
}

#pragma INSTAGRAM
+(NSString*) getInstagramUserId:(NSString*) apiResponse{
    
    NSString *DATA = @"data";
    NSString *ID = @"id";
    
    NSDictionary * json = [self convertNSStringToDictionary:apiResponse];
    if(!json){
        return nil;
    }
    
    id data = [self getValue:DATA fromDictionary:json];
    
    if(!data){
        return nil;
    }
    
    for(id user in data){
        NSString* userId = [self getValue:ID fromDictionary:user];
        if(userId){
            return userId;
        }
    }
    return nil;
}

-(NSArray*) getInstagramPost:(NSString*) apiResponse{
    
    NSDictionary * json = [SocialMapper convertNSStringToDictionary:apiResponse];
    if(!json){
        return nil;
    }
    
     NSString *DATA = @"data";
    id data = [SocialMapper getValue:DATA fromDictionary:json];
    
    if(!data){
        return nil;
    }
    
    NSMutableArray* items = [NSMutableArray new];
    
    for(id item in data){
        
        InstagramObjects* object = [self getInstagramObjectFromDictionary:item];
        
        if(object){
            [items addObject:object];
        }
    }
    return items;
}


-(InstagramObjects*) getInstagramObjectFromDictionary:(id) dictionary{
    
    if(!dictionary){
        return nil;
    }
    
    
    NSString* USER = @"user";
    NSString* USERNAME= @"username";
    NSString* USERIMAGE = @"profile_picture";
    
    NSString* LIKES =@"likes";
    NSString* LIKESCOUNT = @"count";
    
    NSString* TYPE = @"type";
    
    NSString* CAPTION = @"caption";
    NSString* TEXT = @"text";
    
    
    NSString* STANDART_RESOLUTION = @"standard_resolution";
    NSString* URL = @"url";
    NSString* IMAGES = @"images";
    NSString* VIDEOS = @"videos";
    
    
    InstagramObjects* object = [InstagramObjects new];
    
    id userObject = [SocialMapper getValue:USER fromDictionary:dictionary];
    if(userObject){
        object.username = [SocialMapper getValue:USERNAME fromDictionary:userObject];
        object.userimage = [SocialMapper getValue:USERIMAGE fromDictionary:userObject];
    }
    
    id userLikes = [SocialMapper getValue:LIKES fromDictionary:dictionary];
    if(userLikes){
        object.likes = [[SocialMapper getValue:LIKESCOUNT fromDictionary:userLikes] integerValue];
    }
    
    object.type = [SocialMapper getValue:TYPE fromDictionary:dictionary];
    
    id captionObject = [SocialMapper getValue:CAPTION fromDictionary:dictionary];
    if(captionObject){
        object.text = [SocialMapper getValue:TEXT fromDictionary:captionObject];
    }
    
    id imageObject = [SocialMapper getValue:IMAGES fromDictionary:dictionary];
    if(imageObject){
        id standartObject = [SocialMapper getValue:STANDART_RESOLUTION fromDictionary:imageObject];
        if(standartObject){
            object.image = [SocialMapper getValue:URL fromDictionary:standartObject];
        }
    }
    
    id videoObject = [SocialMapper getValue:VIDEOS fromDictionary:dictionary];
    if(videoObject){
        id standartObject = [SocialMapper getValue:STANDART_RESOLUTION fromDictionary:videoObject];
        if(standartObject){
            object.video = [SocialMapper getValue:URL fromDictionary:standartObject];
        }
    }
    
    return object;
}



@end
