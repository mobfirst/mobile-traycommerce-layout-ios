//
//  YouTubeConnection.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 16/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "YouTubeConnection.h"
#import "StoreConfig.h"
#import <PlataformCommunication/PlataformCommunication.h>
#import "SocialMapper.h"

#define YOUTUBE_DEVELOPER_KEY @"AIzaSyDj8N8AXdtlMWQ0rlFR4SNPmtL2QSqfmYQ"

@implementation YouTubeConnection


+(void) getAllVideosFromStoreChannel:(void (^)(NSArray*))block{
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:[self getApiUrl] path:nil headers:nil andResponseBlock:^(NSString * apiResponse,NSDictionary* headers, NSError * error) {
    
        if(error){
            block(nil);
        }
        else{
            block([SocialMapper getYouTubeVideosFromApiResponse:apiResponse]);
        }
    }] connectionStart];
}


+(NSString*) getApiUrl{
    return [NSString stringWithFormat:@"https://gdata.youtube.com/feeds/api/users/%@/uploads?v=2&alt=json",[StoreConfig getStore].youtube];
}


@end
