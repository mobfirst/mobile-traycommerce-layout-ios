//
//  InstagramConnection.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 19/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstagramConnection : NSObject

+(void) getAllMediasFromTag:(NSString*) tag andBlock:(void (^)(NSArray*))block;
+(void) getAllMediaFromUser:(NSString*) user andBlock:(void (^)(NSArray*))block;

@end
