//
//  InstagramConnection.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 19/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "InstagramConnection.h"
#import "StoreConfig.h"
#import <PlataformCommunication/PlataformCommunication.h>
#import "SocialMapper.h"

#define INSTAGRAM_SECRET_KEY @"8eb3221f8bde4c36b895aa30b1a6f468"

@implementation InstagramConnection


+(void) getAllMediasFromTag:(NSString*) tag andBlock:(void (^)(NSArray*))block{
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:[self getInstagramMediasUrlFromTag:tag] path:nil headers:nil andResponseBlock:^(NSString * apiResponse, NSDictionary* headers,NSError * error) {
        
        if(error){
            block(nil);
        }else{
            block([[SocialMapper new] getInstagramPost:apiResponse]);
        }
       
    }] connectionStart];
    
}
+(void) getAllMediaFromUser:(NSString*) user andBlock:(void (^)(NSArray*))block{
    
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:[self getInstagramUserByName:user] path:nil headers:nil andResponseBlock:^(NSString * apiResponse,NSDictionary* headers, NSError * error) {
        
        if(error){
             block(nil);
        }else{

            NSString* userID = [SocialMapper getInstagramUserId:apiResponse];
            
            if(!userID){
                block(nil);
            }
            else{
                [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:[self getInstagramUserMediaByUserId:userID] path:nil headers:nil andResponseBlock:^(NSString * apiResponse, NSDictionary* headers,NSError * error) {
                    
                    if(error){
                        block(nil);
                    }else{
                        block([[SocialMapper new] getInstagramPost:apiResponse]);
                    }
                }] connectionStart];
            }
        }
    }] connectionStart];
}

+(NSString*)getInstagramMediasUrlFromTag:(NSString*) TAG{
    return [NSString stringWithFormat:@"https://api.instagram.com/v1/tags/%@/media/recent?client_id=%@",TAG, INSTAGRAM_SECRET_KEY];
}

+(NSString*)getInstagramUserByName:(NSString*) Name{
    return [NSString stringWithFormat:@"https://api.instagram.com/v1/users/search?q=%@&client_id=%@", Name,INSTAGRAM_SECRET_KEY];
}

+(NSString*)getInstagramUserMediaByUserId:(NSString*) userId{
    return [NSString stringWithFormat:@"https://api.instagram.com/v1/users/%@/media/recent/?client_id=%@",userId, INSTAGRAM_SECRET_KEY];
}


@end
