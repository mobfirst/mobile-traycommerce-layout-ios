//
//  TwitterConnection.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 16/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "TwitterConnection.h"
#import "NSString+HTML.h"
#import <PlataformCommunication/PlataformCommunication.h>
#import "SocialMapper.h"

#define TWITTER_KEY @"MVQwSXRjUHVaRDNNN0N0bmdHUmpiOVZ4MzpTSmVmMTNLZmVSTVhVcGZXWm5GT2pIdVdtelpYOGc4akRkR3lYZldNaU9LaTAxYVdHRQ=="


#define AUTHORIZATION @"Authorization"

@implementation TwitterConnection


+(void) getAllTweetsFromUserTimeline:(NSString*) user andResponseBlock:(void (^)(NSArray*))block{

    
    NSDictionary* headers =@{AUTHORIZATION: [NSString stringWithFormat:@"Basic %@",TWITTER_KEY]};
    NSDictionary* params =@{@"grant_type": @"client_credentials"};
    
    [[PlataformCommunication initWithConnectionType:ConnectionTypePost urlString:[self getAuthUrl] path:nil headers:headers parameters:params andResponseBlock:^(NSString * apiResponse, NSDictionary* headers, NSError * error) {
        
        if(error){
            block(nil);
            return;
        }
        
        NSString* token = [SocialMapper getTwitterApiToken:apiResponse];
        
        if(!token){
            block(nil);
            return;
        }
        
        
        NSDictionary* headersRequest =@{
                                        AUTHORIZATION: [NSString stringWithFormat:@"Bearer %@",token],
                                        @"Content-Type":@"application/json"
                                        };
        
        [[PlataformCommunication initWithConnectionType:ConnectionTypeGet urlString:[self getTimeLineUrlFromUser:user] path:nil headers:headersRequest andResponseBlock:^(NSString * apiResponse, NSDictionary* headers,NSError * error) {
            if(error){
                block(nil);
                return;
            }else{
                block([SocialMapper getAllTweetsFromAPIResponse:apiResponse]);
                return;
            }
        }] connectionStart];
    }] connectionStart];
}



+(NSString*) getAuthUrl{
    return @"https://api.twitter.com/oauth2/token";
}

+(NSString*) getTimeLineUrlFromUser:(NSString*) user{
    return [NSString stringWithFormat:@"https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=%@",user];
}


@end
