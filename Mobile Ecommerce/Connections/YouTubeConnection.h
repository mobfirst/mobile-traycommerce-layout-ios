//
//  YouTubeConnection.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 16/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YouTubeConnection : NSObject

+(void) getAllVideosFromStoreChannel:(void (^)(NSArray*))block;

@end
