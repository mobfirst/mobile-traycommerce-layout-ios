//
//  SocialMapper.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 17/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YouTubeObject.h"
#import "TwitterUrls.h"
#import "TwitterObject.h"

@interface SocialMapper : NSObject

#pragma YOUTUBE
+(NSArray*) getYouTubeVideosFromApiResponse:(NSString*) apiResponse;

#pragma TWITTER
+(NSString*) getTwitterApiToken:(NSString*) apiResponse;
+(NSArray*) getAllTweetsFromAPIResponse:(NSString*) apiResponse;

#pragma INSTAGRAM
+(NSString*) getInstagramUserId:(NSString*) apiResponse;
-(NSArray*) getInstagramPost:(NSString*) apiResponse;

@end
