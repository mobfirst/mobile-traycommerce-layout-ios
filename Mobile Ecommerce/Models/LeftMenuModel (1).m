//
//  LeftMenuModel.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 24/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "LeftMenuModel.h"

@implementation LeftMenuModel

+(LeftMenuModel*) setItemWithText:(NSString*) text image:(NSString*) image andTag:(MenuTag) tag{
    return [LeftMenuModel setItemWithText:text image:image tag:tag andChildrens:nil];
}

+(LeftMenuModel *) setItemWithText:(NSString*) text image:(NSString*) image tag:(MenuTag) tag andChildrens:(NSArray*) array{
    
    LeftMenuModel * item = [LeftMenuModel new];
    item.text = text;
    item.image = image;
    item.tag = tag;
    item.childrens = array;
    item.title = nil;
    return item;
}

+(LeftMenuModel *) setItemWithTitle:(NSString*) title text:(NSString*) text image:(NSString*) image tag:(MenuTag) tag andObject:(NSObject*) object{
    
    LeftMenuModel * item = [LeftMenuModel new];
    item.text = text;
    item.image = image;
    item.tag = tag;
    item.title = title;
    item.childrens = nil;
    item.object = object;
    return item;
    
}

+(LeftMenuModel *) setItemWithTitle:(NSString*) title text:(NSString*) text image:(NSString*) image andTag:(MenuTag) tag{
    return  [LeftMenuModel setItemWithTitle:title text:text image:image tag:tag andObject:nil];
}
+(LeftMenuModel *) setItemWithText:(NSString*) text image:(NSString*) image tag:(MenuTag) tag andObject:(NSObject*) object{
    
    LeftMenuModel * item = [LeftMenuModel new];
    item.text = text;
    item.image = image;
    item.tag = tag;
    item.object = object;
    item.title = nil;
    return item;
    
}

@end
