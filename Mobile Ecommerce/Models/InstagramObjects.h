//
//  InstagramObjects.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 19/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstagramObjects : NSObject

@property(strong,nonatomic) NSString* text;
@property(strong,nonatomic) NSString* username;
@property(strong,nonatomic) NSString* userimage;
@property(strong,nonatomic) NSString* image;
@property(strong,nonatomic) NSString* type;
@property(strong,nonatomic) NSString* video;
@property(nonatomic) NSInteger likes;
@end
