//
//  ProductModels.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 06/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Variation.h"

@interface ProductModels : NSObject

@property (nonatomic) BOOL isValue;
@property (strong,nonatomic) NSString* title;
@property (strong,nonatomic) NSMutableArray* itens;
@property (strong,nonatomic) Variation* variation;
@property (nonatomic) int quantity;

@end
