//
//  LeftMenuModel.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 24/11/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface LeftMenuModel : NSObject

@property(strong, nonatomic) NSString* imageUrl;
@property(strong, nonatomic) NSString* image;
@property(strong, nonatomic) NSString* text;
@property(strong, nonatomic) NSString* title;
@property(nonatomic) MenuTag tag;
@property(strong,nonatomic) NSArray * childrens;
@property(strong,nonatomic) NSObject* object;


+(LeftMenuModel *) setItemWithTitle:(NSString*) title text:(NSString*) text image:(NSString*) image andTag:(MenuTag) tag;
+(LeftMenuModel *) setItemWithTitle:(NSString*) title text:(NSString*) text image:(NSString*) image tag:(MenuTag) tag andObject:(NSObject*) object;
+(LeftMenuModel *) setItemWithText:(NSString*) text image:(NSString*) image andTag:(MenuTag) tag;
+(LeftMenuModel *) setItemWithText:(NSString*) text image:(NSString*) image tag:(MenuTag) tag andChildrens:(NSArray*) array;

@end
