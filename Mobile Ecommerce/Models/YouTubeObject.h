//
//  YouTubeObject.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 17/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YouTubeObject : NSObject

@property(strong,nonatomic) NSString* urlImage;
@property(strong,nonatomic) NSString* urlVideo;
@property(strong,nonatomic) NSString* title;
@property(strong,nonatomic) NSString* videoDescription;
@property(strong,nonatomic) NSString* published;
@property(nonatomic) NSInteger viewCount;
@property(nonatomic) int duration;

@end
