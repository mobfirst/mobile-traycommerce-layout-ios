//
//  TwitterUrls.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 19/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TwitterUrls : NSObject

@property(strong,nonatomic) NSString* url;
@property(nonatomic) NSInteger indiceStart;
@property(nonatomic) NSInteger indiceEnd;

@end
