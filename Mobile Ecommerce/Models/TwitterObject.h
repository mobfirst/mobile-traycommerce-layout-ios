//
//  TwitterObject.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 19/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TwitterObject : NSObject

@property(strong,nonatomic) NSString* userName;
@property(strong,nonatomic) NSString* user;
@property(strong,nonatomic) NSString* userImage;

@property(strong,nonatomic) NSString* text;
@property(strong,nonatomic) NSString* timeCreate;
@property(strong,nonatomic) NSArray* urls;

@end
