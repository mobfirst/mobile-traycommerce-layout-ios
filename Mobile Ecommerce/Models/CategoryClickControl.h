//
//  CategoryClickControl.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 05/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductCategory.h"

@interface CategoryClickControl : NSObject

@property(strong,nonatomic) ProductCategory* category;

@property(nonatomic) NSInteger index;
@property(nonatomic) BOOL isParent;

@end
