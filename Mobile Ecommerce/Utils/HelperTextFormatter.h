//
//  HelperTextFormatter.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HelperTextFormatter : NSObject

+(BOOL)isValidUsername:(NSString *)username;
+(BOOL)isValidUsernameChar:(NSString *)character;
+(BOOL)isValidPassword:(NSString *)password;
+(BOOL)isValidFullName:(NSString *)fullName;
+(BOOL)isValidEmail:(NSString *)email;
+(BOOL)isValidDOB:(NSString *)dobString;
+(BOOL)isValidPostalCode:(NSString*) postalCode;

+(BOOL)isValidCPF:(NSString*) cpf;
+(BOOL)isValidCreditCardNumber:(NSString *)ccNumber;
+(BOOL)isValidCreditCardValidationNumber:(NSString *)validationNumber;
+(BOOL)isValidExpirationDateYear:(NSString*)dateString;
+(BOOL)isValidHolderName:(NSString *)name;

+(BOOL)isValidPhoneDDD:(NSString *)ddd;
+(BOOL)isValidPhoneNumber:(NSString *)phone;


+(int) getMonthFromYearExpiration:(NSString*)dateString;
+(int) getYearFromYearExpiration:(NSString*)dateString;


+(BOOL) hasHTMlContent:(NSString*) string;


@end
