//
//  HelperTextFormatter.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 21/01/15.
//  Copyright (c) 2015 Diego P Navarro. All rights reserved.
//

#import "HelperTextFormatter.h"

@implementation HelperTextFormatter


+(BOOL)isValidUsername:(NSString *)username {
    /* Rules:
     - 3 to 18 characters
     - Only numbers, lower case letters and underscore
     */
    // Validate
    NSRange range = [username rangeOfString:@"[a-z0-9][_a-z0-9]{2,17}" options:NSRegularExpressionSearch|NSCaseInsensitiveSearch];
    return range.location != NSNotFound;
}
+(BOOL)isValidUsernameChar:(NSString *)character {
    NSRange range = [character rangeOfString:@"[_a-z0-9]" options:NSRegularExpressionSearch|NSCaseInsensitiveSearch];
    return range.location != NSNotFound;
}
+(BOOL)isValidPassword:(NSString *)password {
    /* Rules:
     - 3 to 32 chars
     */
    NSRange range = [password rangeOfString:@"^(.{3,32})$" options:NSRegularExpressionSearch];
    return range.location != NSNotFound;
}
+(BOOL)isValidFullName:(NSString *)fullName {
    // 2 or more letters
    NSRange range = [fullName rangeOfString:@"[^-!$%^&*()_+|~=`{}\\[\\]:\";'<>?,.\\/\\d]{2,32}" options:NSRegularExpressionSearch|NSCaseInsensitiveSearch];
    return range.location != NSNotFound;
}
+(BOOL)isValidEmail:(NSString *)email {
    // Valid email
    NSRange range = [email rangeOfString:@"[a-z0-9._%-]+@[a-z0-9.-]+\\.[a-z]{2,4}" options:NSRegularExpressionSearch|NSCaseInsensitiveSearch];
    return range.location != NSNotFound;
}

+(NSDate *)convertStringToDate:(NSString*) string
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    return [dateFormatter dateFromString:string];
}
+(BOOL)isValidDOB:(NSString *)dobString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd/MM/yyyy";
    
    NSDate *date = [dateFormatter dateFromString:dobString];
    if (!date || [date compare:[NSDate date]] == NSOrderedDescending) {
        return NO;
    }
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    
    BOOL isValidDate = YES;
    isValidDate &= (components.year > 1900);
    isValidDate &= (components.month >= 1 && components.month <= 12);
    isValidDate &= (components.day >= 1 && components.day <= 31);
    
    if (isValidDate) {
        return YES;
    }
    return NO;
}

+(BOOL)isValidPostalCode:(NSString*) postalCode
{
    if([postalCode length] == 8)
        return YES;
    else
        return NO;
}

+(BOOL)isValidCreditCardNumber:(NSString *)ccNumber {
    NSRange range = [ccNumber rangeOfString:@"[0-9]{12,20}" options:NSRegularExpressionSearch|NSCaseInsensitiveSearch];
    return range.location != NSNotFound;
}

+(BOOL)isValidCreditCardValidationNumber:(NSString *)validationNumber {
    NSRange range = [validationNumber rangeOfString:@"[0-9]{2,5}" options:NSRegularExpressionSearch|NSCaseInsensitiveSearch];
    return range.location != NSNotFound;
}



+(BOOL)isValidExpirationDateYear:(NSString*)dateString  {
    
    if([dateString length] != 7 )
        return NO;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM/yyyy";
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    
    NSCalendar* auxCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents* auxComponents = [auxCalendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    BOOL isValidDate = YES;
    isValidDate &= (components.year >= auxComponents.year);
    isValidDate &= (components.month >= 1 && components.month <= 12);
    isValidDate &= (components.month >= auxComponents.month);
    
    if (isValidDate) {
        return YES;
    }
    return NO;
}

+(BOOL)isValidHolderName:(NSString *)name {
    return [HelperTextFormatter isValidFullName:name];
}
+(BOOL)isValidCPF:(NSString*) cpf
{
    return ([cpf length]==11);
}

+(BOOL)isValidPhoneDDD:(NSString *)ddd
{
    return ([ddd length] == 2);
}
+(BOOL)isValidPhoneNumber:(NSString *)phone
{
    NSArray* completePhone = [phone componentsSeparatedByString:@"-"];
    
    if([completePhone count] != 2)
        return NO;
    
    NSString* dddPhone = [completePhone objectAtIndex:0];
    
    if([dddPhone length] != 2)
        return NO;
    
    NSString* numberPhone = [completePhone objectAtIndex:1];
    
    return ([numberPhone length] > 7);
}


+(int) getMonthFromYearExpiration:(NSString*)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM/yyyy";
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    
    return components.month;
}
+(int) getYearFromYearExpiration:(NSString*)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM/yyyy";
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    
    return components.year;
}


+(BOOL) hasHTMlContent:(NSString*) string{
    return ([string rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound;
}
@end
