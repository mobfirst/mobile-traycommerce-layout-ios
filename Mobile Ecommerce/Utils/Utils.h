//
//  Utils.h
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 10/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Utils : NSObject

+(NSArray*) insertCategoryDefault:(NSArray*) categories;
+(NSArray*) insertCategoryDefaultOnCategories:(NSArray*) categories;
+(NSArray*) setPlainToCategories:(NSArray*) categories;

+(NSString*) convertAddressesToString:(NSObject*) address;
+(NSString*) convertPhoneToString:(NSObject*) phone;
+(NSString*) convertPhoneToStringWithTitle:(NSObject*) phone;

+(float) getCellAspectWithWidth:(float) cellWidth andHeight:(float) cellHeight;
+(float) getCellWidth:(float) screenSize numberOfColums:(int) colums andPadding:(int) padding;

+(NSString*) calculateTimePast:(NSString*) time;

+(NSString*) formatterTextVideoDuration:(int) duration;
+(NSString*) calculateTimePastYouTube:(NSString *)time;
+(NSString*) formatterTextVideoCount:(NSInteger) viewCount;

+(NSString*) firstCharUpperCase:(NSString*) text;

@end
