//
//  Utils.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 10/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import "Utils.h"
#import "ProductCategory.h"
#import "Constants.h"
#import "Address.h"
#import "Phones.h"



@implementation Utils

+(NSArray*) insertCategoryDefault:(NSArray*) categories{
    
    NSMutableArray * cat = [NSMutableArray new];
    
    ProductCategory* categoryAll = [ProductCategory new];
    
    categoryAll.id = CATEGORY_DEFAULT;
    categoryAll.name = @"TODOS OS PRODUTOS";
    
    
    [cat addObject:categoryAll];
    [cat addObjectsFromArray:categories];

    return cat;
    
}

+(NSArray*) insertCategoryDefaultOnCategories:(NSArray*) categories{
    
    
    for(ProductCategory * category in categories){
        
        if(category.childrens && [category.childrens count] > 0){
            
            NSMutableArray * listCategories = [NSMutableArray new];
            
            ProductCategory * catDefault = [ProductCategory new];
            catDefault.id = category.id;
            catDefault.name = [NSString stringWithFormat:@"TODOS"];
            
            [self insertCategoryDefaultOnCategories:category.childrens];
            
            [listCategories addObject:catDefault];
            
            [listCategories addObjectsFromArray:category.childrens];
            
            category.childrens = listCategories;
        }
    }
    return categories;
}

+(void) checkCategoryInside:(NSMutableArray*) newCategories andCategory : (ProductCategory*) category{
    [newCategories addObject:category];
    if(category.childrens && [category.childrens count] > 0){
        for(ProductCategory* category2 in category.childrens){
            [self checkCategoryInside:newCategories andCategory:category2];
        }
    }
}
+(NSArray*) setPlainToCategories:(NSArray*) categories{
    
    NSMutableArray* newCategories = [NSMutableArray new];
    
    for(ProductCategory* category in categories){
        [self checkCategoryInside:newCategories andCategory:category];
    }
    return newCategories;
}

+(NSString*) convertAddressesToString:(NSObject*) object{
    
    Address* address = (Address*) object;
    
    NSString* addressString;
    
    if(address.addressLine2){
        addressString = [NSString stringWithFormat:@"%@ %@\n%@\n%@\n%@-%@", address.addressLine1,address.number,address.addressLine2,address.neighborhood,address.city,address.state];
    }
    else{
        addressString = [NSString stringWithFormat:@"%@ %@\n%@\n%@-%@", address.addressLine1,address.number,address.neighborhood,address.city,address.state];
    }
    return addressString;
}

+(NSString*) convertPhoneToString:(NSString*) object{
    Phones* phone = (Phones*) object;
    return [NSString stringWithFormat:@"(%@) %@",phone.areaCode,phone.number];
}

+(NSString*) convertPhoneToStringWithTitle:(NSObject*) object{
    Phones* phone = (Phones*) object;
    return [NSString stringWithFormat:@"%@\n(%@) %@",phone.title,phone.areaCode,phone.number];
}
+(float) getCellAspectWithWidth:(float) cellWidth andHeight:(float) cellHeight
{
    return cellHeight/cellWidth;
}

+(float) getCellWidth:(float) screenSize numberOfColums:(int) colums andPadding:(int) padding
{
    return  screenSize/colums - (padding*(colums-1));
}

+(NSString*) calculateTimePast:(NSString*) time{
    
    time = [time substringFromIndex:4];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd HH:mm:ss +0000 yyyy"];
    dateFormatter.timeZone =  [NSTimeZone timeZoneForSecondsFromGMT:0];
    
    NSDate* postCreate = [dateFormatter dateFromString:time];
    NSDate* today = [NSDate date];

    return [self compareDates:today old:postCreate];
}

+(NSString*) compareDates:(NSDate*) recent old :(NSDate* ) old{
    NSTimeInterval interval = [recent timeIntervalSinceDate:old];
    
    int minutes = interval/60;
    
    if(minutes < 60){
        return [NSString stringWithFormat:@"%d min",minutes];
    }else{
        
        int hours = minutes / 60;
        if(hours < 24){
            return [NSString stringWithFormat:@"%d h",hours];
        }
        else{
            
            int days = hours/24;
            if(days < 7){
                return [NSString stringWithFormat:@"%d d",days];
            }else{
                NSDateFormatter *formater = [[NSDateFormatter alloc] init] ;
                [formater setDateFormat:@"dd MMM"];
                return [formater stringFromDate:old];
            }
        }
    }
    
}

+(NSString*) formatterTextVideoDuration:(int) duration{
    
    int seconds = duration;
    int minutes = 0;
    int hours = 0;
    if(seconds > 60){
        seconds = seconds%60;
        minutes = seconds/60;
        if(minutes > 60){
            hours = minutes/60;
            minutes = minutes%60;
        }
    }
    
    if(hours == 0){
        return [NSString stringWithFormat:@" %02d:%02d ", minutes,seconds];
    }
    else{
        return [NSString stringWithFormat:@" %02d:%02d:%02d ", hours,minutes,seconds];
    }
}

+(NSString*) calculateTimePastYouTube:(NSString *)time{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"];
    dateFormatter.timeZone =  [NSTimeZone timeZoneForSecondsFromGMT:0];
    
    NSDate* postCreate = [dateFormatter dateFromString:time];
    NSDate* today = [NSDate date];
    
    return [self compareDates:today old:postCreate];
}
+(NSString*) formatterTextVideoCount:(NSInteger) viewCount{
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    return [formatter stringFromNumber:[NSNumber numberWithInteger:viewCount]];
}


+(NSString*) firstCharUpperCase:(NSString*) text{
    
    if([text length] == 0){
        return text;
    }
    
    NSString *firstChar = [text substringToIndex:1];
    NSString *folded = [firstChar stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale: [NSLocale currentLocale]];
    return [[folded uppercaseString] stringByAppendingString:[text substringFromIndex:1]];
}

@end
