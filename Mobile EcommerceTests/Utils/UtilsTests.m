//
//  UtilsTests.m
//  Mobile Ecommerce
//
//  Created by Diego P Navarro on 10/10/14.
//  Copyright (c) 2014 Diego P Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Utils.h"

@interface UtilsTests : XCTestCase


@end

@implementation UtilsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testUtils {
    
    float aspect = [Utils getCellAspectWithWidth:1 andHeight:2];
    XCTAssert(aspect == 2, @"Aspect isn't correct");
    
    float width = [Utils getCellWidth:12 numberOfColums:3 andPadding:1];
    XCTAssert(width == 2, @"Width isn't  correct");
    
    
}

/*- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}*/

@end
